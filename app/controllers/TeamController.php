<?php

class TeamController extends BaseController{
public function getUpdateV5 ($v,$teamname) {

    return View::make('teamv5')
        ->with('v',$v)
        ->with('teamname',$teamname);
}

public function postUpdateV5($v,$teamname) {

    $validator = Validator::make(Input::all(), array(
        'password'      => 'required|min:6',
        'email'         => 'required|email',
    ));

    if($validator->fails()) {

        return Redirect::route('team-update-v5',array('v5',$teamname))
            ->withErrors($validator);
    } else {
        $team_v5 = DB::table('teams_v5')->where('teamname',$teamname)->first();
        $captain_v5 = Sentry::findUserById($team_v5->captain_id);

        if(Hash::check(Input::get('password'), $team_v5->password)) {
            return View::make('team.updatev5')
                ->with('team_v5', $team_v5)
                ->with('captain_v5',$captain_v5);
                     }
        else {
            return Redirect::route('profile-team',array($v,$teamname))
                ->with('global','Incorrect password!');
        }
    }
}
}