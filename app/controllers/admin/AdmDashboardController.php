<?php

class AdmDashboardController extends BaseController {

    private  $admin_data;

    public function initialize($page='home')
    {
        $data['page'] = $page;
        if($page === 'profile'){
            $admin = DB::table('admin')->where('users_id','=',Auth::id())->first();
            $this->admin_data = array(
                'name' => $admin->name,
                'telephone' => $admin->telephone,
                'contact_email' => $admin->contact_email
            );
            return View::make('users.admin.dashboard',$data)->with($this->admin_data);

        }else{
            $admin = DB::table('user')->select('profile_pic', 'username')->where('id','=',Auth::id())->first();
            Auth::user()->setAttribute('profile_pic',$admin->profile_pic);
            Auth::user()->setAttribute('name',$admin->username);
            $und = DB::table('user')->where('role','std')->count();
            $alm = DB::table('user')->where('role','alm')->count();
            $ind = DB::table('user')->where('role','ind')->count();
            $role_count = array(
                'und'=> $und,
                'alm' => $alm,
                'ind' => $ind
            );
            $it = DB::table('undergraduate')->where('faculty','ITfac')->count();
            $eng = DB::table('undergraduate')->where('faculty','Efac')->count();
            $archi = DB::table('undergraduate')->where('faculty','Afac')->count();
            $role_count1 = array(
                'und1'=> $it,
                'alm1' => $eng,
                'ind1' => $archi
            );
            $emp = DB::table('employee')->count();
            $all = DB::table('alumni')->count();
            $role_count2 = array(
                'emp'=> $emp,
                'unemp' => $all - $emp,
            );
        }
        return View::make('users.admin.dashboard',$data)->with(array('role_count' => $role_count,'role_count1' => $role_count1 , 'role_count2' => $role_count2));

    }
}
