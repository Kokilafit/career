<?php

class AdmAcceptIndController extends BaseController
{


    public function load(){
        $list = DB::table('user')->where('role','ind')->where('confirmed',2)->get();
        return View::make('users.admin.dashboard')->with(array('page'=>"ind_request_list",'posts'=>$list));
    }
    public function showDetail(){
        $list1 = DB::table('industry')->join('user','user.id','=','industry.user_id')->select('user.username','user.p_email','industry.comp_name','industry.website','industry.phone')->where('industry.user_id',Input::get('uid'))->first();

        return Response::json(View::make('users.admin.popup.ind_request_data')->with(array('accept' => $list1))->render());
       // return Response::json($list1) ;
    }
    public function accept(){
        DB::table('user')->where('id',Input::get('uid'))->update(array('confirmed' => 1));
    }
    public function reject(){
        DB::table('user')->where('id',Input::get('uid'))->delete();
    }
}
