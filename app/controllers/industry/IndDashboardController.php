<?php

class IndDashboardController extends BaseController {


    public function initialize($page='profile')
    {
        $data['page'] = $page;
        $ind = DB::table('industry')->where('user_id','=',Auth::user()->id)->first();
        $ind_data = array(
            'comp_name' => $ind->comp_name,
            'website' => $ind->website,
            'linkedin'=> $ind->linkedin,
            'twitter' => $ind->twitter,
            'about' => $ind->about
        );

        //var_dump($ind_data);
        return View::make('users.industry.dashboard',$data)->with($ind_data);
    }


}
