<?php

class IndProfileBuildController extends BaseController{

    public function buildProfile(){

        $inputs = Input::all();
        $valid = Validator::make($inputs,
            array(
                //'comp_name' => 'required|max:100|min:3',
                'website' => 'max:250',
                //  'mail' => 'required|min:5|max:100|email',
                // 'phone' => 'max:250',
                'linkedin' => 'max:250', //active_url|
                'twitter' => 'max:250',
                // 'about' => ''

            )
        );

        if ($valid->fails() || Auth::user()->role !== 'ind' || !Input::has('image-data')) {
            var_dump($valid->messages());

            //return Redirect::to('/test')->with('error', $valid->messages());
        } else {
            $task = DB::table('industry')
                ->where('user_id',Auth::user()->id)
                ->update(array(

                    'about' => Input::get('cmp-details'),
                    // 'phone' => Input::get('phone'),

                    'linkedin' => Input::get('linkedin'),
                    'twitter' => Input::get('twitter')
                ));

            $profile_pic_filename =  uniqid() . '.png';
            //$cover_pic_filename = md5(Auth::user()->p_email, "cover") . '.png';
            $data = Input::get('image-data');
            list($type, $data) = explode(';', $data);
            list(, $data) = explode(',', $data);
            $data = base64_decode($data);

            /*            $data2 = Input::get('cover-image-data');
                        list($type, $data2) = explode(';', $data2);
                        list(, $data2) = explode(',', $data2);
                        $data2 = base64_decode($data2);*/

            if (file_put_contents('uploads/' . $profile_pic_filename, $data) === false) {
                return Redirect::route('index')->with('error', $valid->messages());
            } else {
                $pic = DB::table('user')
                    ->where('id', '=', Auth::user()->id)
                    ->update(array('profile_pic' => '/ind_covers/' . $profile_pic_filename));
                /*    $cover_pic = DB::table('industry')
                        ->where('user_id', '=', Auth::user()->id)
                        ->update(array('cover_pic' => '/uploads/' . $cover_pic_filename));*/
            }
            DB::table('user')
                ->where('id','=',Auth::user()->id)
                ->where('confirmation_token','=',Auth::user()->confirmation_token)
                ->update(array('confirmation_token' => ""));
            return Redirect::to('/industry/profile');
        }

    }

}
