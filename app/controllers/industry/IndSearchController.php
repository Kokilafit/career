<?php

class IndSearchController extends BaseController
{
    public function search()
    {


        $job_details = DB::table('vacancy')->where('id',Input::get('job_id'))->first();

        if(is_null($job_details)){
            echo "Sorry Your Vacancy has no requirements for Applicants";
            exit();
        }



        $terms['ogpa'] = $job_details->gpa;
        $terms['skills'] = $job_details->skills;
        $terms['faculty'] = $job_details->fac;
        $terms['query'] = "";


        $tagCount = 0;
        if($terms["skills"]!=""){
            $taglist = explode(", ",$terms['skills']);
            DB::setFetchMode(PDO::FETCH_COLUMN);
            $orders  = DB::table('skills_pool')->select('skill')->get();
            DB::setFetchMode(PDO::FETCH_CLASS);
            $tags    = array_intersect(array_map('strtoupper', $taglist), array_map('strtoupper', $orders));
            $tagCount = count($tags);
            $conditions = "skill = '". $tags[0]. "'";
            for($i=1 ; $i < count($tags) ; $i++){
                $conditions .= " OR skill = '". $tags[$i]. "'";
            }
        }

        $queryString ="";
        $pageNo = Input::get('page', 1);
        $perPage = 4;
        $from = $pageNo*$perPage-$perPage;
        $to = $perPage;
        $data['allData'] = null;
        $totalData = null;

        if($tagCount == 0  && isset($terms['ogpa']) && $terms['query']=="" && !isset($terms['employee']) && $terms['faculty'] == "none"){
            $queryString = "SELECT undergraduate.name,undergraduate.faculty,undergraduate.user_id,user.profile_pic FROM skills
            INNER JOIN skills_pool
            ON skills_pool.id = skills.skills_pool_id
            INNER JOIN undergraduate
            ON undergraduate.user_id = skills.undergraduate_id
            INNER JOIN user
            ON undergraduate.user_id = user.id
            WHERE ogpa >= :ogpaVal
            GROUP BY round(ogpa,1),user_id DESC ORDER BY round(ogpa,1) DESC,count(user_id) DESC";
            $totalData =DB::select( DB::raw($queryString),array('ogpaVal' => $terms['ogpa']));

            $data['allData'] =DB::select( DB::raw($queryString .' LIMIT :from,:to'),
                array('ogpaVal' => $terms['ogpa'],'from'=>$from,'to'=>$to));
       }
        elseif($tagCount > 0  && isset($terms['ogpa']) && $terms['query']=="" && !isset($terms['employee']) && $terms['faculty'] == "none"){
            $queryString = "SELECT undergraduate.name,undergraduate.faculty,undergraduate.user_id,user.profile_pic FROM skills
            INNER JOIN skills_pool
            ON skills_pool.id = skills.skills_pool_id
            INNER JOIN undergraduate
            ON undergraduate.user_id = skills.undergraduate_id
            INNER JOIN user
            ON undergraduate.user_id = user.id
            WHERE (". $conditions .") and ogpa >= :ogpaVal
            GROUP BY round(ogpa,1),user_id DESC ORDER BY round(ogpa,1) DESC,count(user_id) DESC";
            $totalData =DB::select( DB::raw($queryString),array('ogpaVal' => $terms['ogpa']));

            $data['allData'] =DB::select( DB::raw($queryString .' LIMIT :from,:to'),
                array('ogpaVal' => $terms['ogpa'],'from'=>$from,'to'=>$to));
        }
        elseif($tagCount > 0  && isset($terms['ogpa']) && $terms['query']=="" && !isset($terms['employee']) && $terms['faculty'] != "none"){
            $queryString = "SELECT undergraduate.name,undergraduate.faculty,undergraduate.user_id,user.profile_pic FROM skills
            INNER JOIN skills_pool
            ON skills_pool.id = skills.skills_pool_id
            INNER JOIN undergraduate
            ON undergraduate.user_id = skills.undergraduate_id
            INNER JOIN user
            ON undergraduate.user_id = user.id
            WHERE (". $conditions .") and ogpa >= :ogpaVal and faculty = :faculty
            GROUP BY round(ogpa,1),user_id DESC ORDER BY round(ogpa,1) DESC,count(user_id) DESC";
            $totalData =DB::select( DB::raw($queryString),array('ogpaVal' => $terms['ogpa'],'faculty'=>$terms['faculty']));

            $data['allData'] =DB::select( DB::raw($queryString .' LIMIT :from,:to'),
                array('ogpaVal' => $terms['ogpa'],'faculty'=>$terms['faculty'],'from'=>$from,'to'=>$to));
        }
        elseif($tagCount == 0  && isset($terms['ogpa']) && $terms['query']=="" && !isset($terms['employee']) && $terms['faculty'] != "none"){
            $queryString = "SELECT undergraduate.name,undergraduate.faculty,undergraduate.user_id,user.profile_pic FROM skills
            INNER JOIN skills_pool
            ON skills_pool.id = skills.skills_pool_id
            INNER JOIN undergraduate
            ON undergraduate.user_id = skills.undergraduate_id
            INNER JOIN user
            ON undergraduate.user_id = user.id
            WHERE ogpa >= :ogpaVal and faculty = :faculty
            GROUP BY round(ogpa,1),user_id DESC ORDER BY round(ogpa,1) DESC,count(user_id) DESC";
            $totalData =DB::select( DB::raw($queryString),array('ogpaVal' => $terms['ogpa'],'faculty'=>$terms['faculty']));

            $data['allData'] =DB::select( DB::raw($queryString .' LIMIT :from,:to'),
                array('ogpaVal' => $terms['ogpa'],'faculty'=>$terms['faculty'],'from'=>$from,'to'=>$to));
        }
        elseif($tagCount > 0  || isset($terms['ogpa']) || $terms['query'] !="" || isset($terms['employee']) || $terms['faculty'] != "none"){
            $queryString = "SELECT undergraduate.name,undergraduate.faculty,undergraduate.user_id,user.profile_pic FROM skills
            INNER JOIN skills_pool
            ON skills_pool.id = skills.skills_pool_id
            INNER JOIN undergraduate
            ON undergraduate.user_id = skills.undergraduate_id
            INNER JOIN user
            ON undergraduate.user_id = user.id
            WHERE name LIKE :name
            GROUP BY user_id";


            $totalData =DB::select( DB::raw($queryString),array('name' => '%'.$terms['query'].'%'));

            $data['allData'] =DB::select( DB::raw($queryString .' LIMIT :from,:to'),
                array('name' => '%'.$terms['query'].'%','from'=>$from,'to'=>$to));
        }

        $data['paginator'] = Paginator::make($data['allData'], count($totalData), $perPage);


        if(count($totalData)===0){
            echo "Sorry Your Vacancy has no suggested Applicants,
            May be your requirements(GPA,Skills,Faculty) are wrongly inserted";
            exit();
        }


        if (Request::ajax()) {
            return Response::json(View::make('users.industry.search_result', array('results' => $data['paginator']))->render());
        }

        return View::make('users.industry.search_result', array('results' =>$data['paginator']));




    }


    public function pop_result()
    {

        $test = DB::table('undergraduate')->join('user','undergraduate.user_id','=','user.id')->select('undergraduate.name','undergraduate.n_email','undergraduate.faculty','undergraduate.department','undergraduate.ogpa','undergraduate.Linkedin','user.profile_pic')->where('undergraduate.user_id',Input::get('uid'))->first();


        if (Request::ajax()) {
            return Response::json(View::make('users.industry.popup.result_view', array('stddata' => $test))->render());
        }
        //return View::make('users.admin.popup.Search_Result', array('asd' => $data));
    }
}
