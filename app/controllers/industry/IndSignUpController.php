<?php
/*
 * this is the supper class for users registration process
 * this will be extended by Student registration and
 * company registration controllers
 * */



class IndSignUpController extends BaseController{

     public function postSignUp(){
        $inputs = Input::all();
        $valid = Validator::make($inputs,
            array(
                'username' => 'required|max:20|min:3|unique:user',
                'company_name'=> 'required|max:200|min:3',
                'p_email' => 'required|min:5|max:100|email|unique:user',
                'phone' => 'required|max:15|min:10',
                //'website'=> 'active_url|max:150',
                'password' => 'required|max:32|min:6',
                'user_role'=> 'required|in:ind,',
                'g-recaptcha-response' => 'required'
            )
            );

        if($valid->fails()){
            return Redirect::route('index')->with('error',$valid->messages());
        }
        else{


            $user = new User;
            $user->username = Input::get('username');
            $user->password = Hash::make(Input::get('password'));
            $user->role = Input::get('user_role');
            $user->p_email = Input::get('p_email');
            $token=$user->confirmation_token = str_random(60);
            $user->confirmed = 3;
            $user->save();

            $Industry = new Industry();
            $Industry->comp_name = Input::get('company_name');
            $Industry->website = Input::get('website');
            $Industry->phone = Input::get('phone');
            $user->Industry()->save($Industry);


            Mail::send('emails.confirmation', array('token_url' => URL::route('confirmation', $token),'task' => "ind_mail"), function($message)
            {
                $message->to(Input::get('p_email'))->subject('Activation CareerPlus!');
            });

            $inbox = $this->findinbox(Input::get('p_email'));

            $data =array(
                'popup'=>'show',
                'popup_msg' => '<h4 class="text-success">Account Created !</h4>,<br/> Confirmation email was sent to '.$inputs['p_email'].' </br>',
                'redirect'=>$inbox[0],
                'but_msg'=>$inbox[1],
                'title'=>'Confirmation email was sent !');
            return View::make('public.index',$data);
        }
    }

    private function findinbox($email){

        $domain = explode('@', strtolower($email))[1];
        $gmail = array("gmail.com");
        $yahoo_mail = array("ymail.com","yahoo.com");

        switch(true){
            case in_array($domain,$gmail) :
                return array("https://mail.google.com","Go to Gmail &raquo;");
                break;
            case in_array($domain,$yahoo_mail) :
                return array("https://mail.google.com","Go to Gmail &raquo;");
                break;
            default:
                return array("https://mail.google.com","Go to Gmail &raquo;");
        }

    }

}

