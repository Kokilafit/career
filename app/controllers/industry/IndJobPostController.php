<?php

class IndJobPostController extends BaseController
{
    public function add()
    {
        $inputs = Input::all();

            if(isset($inputs['img'])){  //check img existence

                $valid1 = Validator::make($inputs,
                    array(
                        'title' => 'required|max:100|min:3',
                        'description' => 'required|max:500|min:10',
                        'img' => 'image|mimes:jpeg,jpg,png',
                        'tag_list' => 'max:300',
                        //'GPA' => 'max:300',
                        'fac' => 'max:6',
                        'dep' => 'max:6'

                    )
                );

                        if ($valid1->fails() || Auth::user()->role !== 'ind') {
                            dd($valid1->messages());
                            return View::make('users.admin.dashboard')->with(array('page'=>"adm_portal",'error', $valid->messages()));

                        }
                        else
                        {
                            $img_fname = uniqid(Auth::user()->id, true) . '.png';
                            $post = DB::table('post')
                                ->insertGetId(array(
                                    'uid' => Auth::user()->id,
                                    'title' => Input::get('title'),
                                    'desc' => Input::get('description'),
                                    'img' => $img_fname,
                                    'cat' => 'ind-job'
                                ));
                            $task = DB::table('vacancy')
                                ->insert(array(
                                    'id' => $post,
                                    'gpa' => Input::get('GPA'),
                                    'skills' => Input::get('tag_list'),
                                    'fac' => Input::get('fac'),
                                    'dep' => Input::get('dep')
                                ));
                            Input::file('img')->move(base_path() . '/public/post_img', $img_fname);

                        }

            }
            else
            {

                $valid2 = Validator::make($inputs,
                    array(
                        'title' => 'required|max:100|min:3',
                        'description' => 'required|max:500|min:10',
                        'tag_list' => 'max:300',
                        //'GPA' => 'max:300',
                        'fac' => 'max:6',
                        'dep' => 'max:6'
                    )
                );

                if ($valid2->fails() || Auth::user()->role !== 'ind') {
                    dd($valid2->messages());
                    return View::make('users.admin.dashboard')->with(array('page'=>"adm_portal",'error', $valid2->messages()));
                }
                else
                {
                    $post = DB::table('post')
                        ->insertGetId(array(
                            'uid' => Auth::user()->id,
                            'title' => Input::get('title'),
                            'desc' => Input::get('description'),
                            'cat' => 'ind-job'
                        ));
                    $task = DB::table('vacancy')
                        ->insert(array(
                            'id' => $post,
                            'gpa' => Input::get('GPA'),
                            'skills' => Input::get('tag_list'),
                            'fac' => Input::get('fac'),
                            'dep' => Input::get('dep')
                        ));

                }

            }
        $posts = DB::table('post')->where('uid',Auth::id())->orderBy('id', 'desc')->get();

        return Redirect::route('ind_dashboard','Vacancy_post');

       // return View::make('users.industry.dashboard')->with(array('page'=>"Vacancy_post",'posts'=>$posts));

        }


    public function init(){
        $posts = DB::table('post')->where('uid',Auth::id())->orderBy('id', 'desc')->get();

       return View::make('users.industry.dashboard')->with(array('page'=>"Vacancy_post",'posts'=>$posts));
    }
}




