<?php

class StdSearchController extends BaseController
{

    public function search()
    {
        $terms = Input::all();

        $queryString ="";
        $pageNo = Input::get('page', 1);
        $perPage = 4;
        $from = $pageNo*$perPage-$perPage;
        $to = $perPage;
        $data['allData'] = null;
        $totalData = null;


        $queryString = "SELECT undergraduate.name,undergraduate.faculty,undergraduate.user_id,user.profile_pic FROM user
            INNER JOIN undergraduate
            ON undergraduate.user_id = user.id
            WHERE name LIKE :name
            GROUP BY user_id";


        $totalData =DB::select( DB::raw($queryString),array('name' => '%'.$terms['query'].'%'));

        $data['allData'] =DB::select( DB::raw($queryString .' LIMIT :from,:to'),
            array('name' => '%'.$terms['query'].'%','from'=>$from,'to'=>$to));

        $data['paginator'] = Paginator::make($data['allData'], count($totalData), $perPage);

        if (Request::ajax()) {
            return Response::json(View::make('users.student.search_result', array('results' => $data['paginator']))->render());
        }

        return View::make('users.student.search_result', array('results' =>$data['paginator']));





    }
    public function pop_result()
    {

        $test = DB::table('undergraduate')->join('user','undergraduate.user_id','=','user.id')->select('undergraduate.name','undergraduate.n_email','undergraduate.faculty','undergraduate.department','undergraduate.ogpa','undergraduate.Linkedin','user.profile_pic')->where('undergraduate.user_id',Input::get('uid'))->first();

        $dt = DB::table('friends')->select('state')->where('requester', Auth::user()->id)->where('acceptor', Input::get('uid'))->first();

        if (Request::ajax()) {
            return Response::json(View::make('users.student.popup.result_view', array('stddata' => $test))->render(),200,array('isFriend'=>$dt));
        }
        //return View::make('users.admin.popup.Search_Result', array('asd' => $data));
    }

}
