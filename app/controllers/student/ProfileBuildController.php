<?php

class ProfileBuildController extends BaseController{

    public function buildProfile(){

        $inputs = Input::all();
        $valid = Validator::make($inputs,
            array(
                'name' => 'required|max:100|min:3',
                'bday' => 'required|date',
                'mail' => 'required|min:5|max:100|email',
                'linkedin' => 'max:250',
                'gplus' => 'max:250', //active_url|
                'yoe' => 'required|digits:4',
                'fac' => 'required|in:Efac,ITfac,Afac',
                'dep' => 'in:DTLM,DCT,DMech,DMSE,DENTC,DTEE,DER,DCSE,DCE,DCPE,DTCP,DBE,DID,DArc'
            )
        );

        if ($valid->fails() || Auth::user()->role !== 'std' || !Input::has('image-data')) {
            var_dump($valid->messages());
            //return Redirect::to('/test')->with('error', $valid->messages());
        } else {
            $task = DB::table('undergraduate')
                ->insert(array(
                    'user_id' => Auth::user()->id,
                    'name' => Input::get('name'),
                    'dob' => Input::get('bday'),
                    'n_email' => Input::get('mail'),
                    'entrance_year' => Input::get('yoe'),
                    'faculty' => Input::get('fac'),
                    'department' => Input::get('dep'),
                    'Linkedin' => Input::get('linkedin'),
                    'googleplus' => Input::get('gplus')
                ));
            $profile_pic_filename = uniqid() . '.png';
            $data = Input::get('image-data');
            list($type, $data) = explode(';', $data);
            list(, $data) = explode(',', $data);
            $data = base64_decode($data);
            if (file_put_contents('uploads/' . $profile_pic_filename, $data) === false) {
                //return Redirect::route('index')->with('error', $valid->messages());
            } else {
                $pic = DB::table('user')
                    ->where('id', '=', Auth::user()->id)
                    ->update(array('profile_pic' => '/uploads/' . $profile_pic_filename));
            }
            DB::table('user')
                ->where('id','=',Auth::user()->id)
                ->where('confirmation_token','=',Auth::user()->confirmation_token)
                ->update(array('confirmation_token' => ""));

            return Redirect::to('/student/profile');
        }

    }

}
