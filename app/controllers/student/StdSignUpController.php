<?php
/*
 * this is the supper class for users registration process
 * this will be extended by Student registration and
 * company registration controllers
 * */



class StdSignUpController extends BaseController{

    public function postSignUp(){
        $inputs = Input::all();
        if(Input::get('user_role')=="std"){
            $inputs['p_email'] =Input::get('username').'@uom.lk';
        }
        $valid = Validator::make($inputs,
            array(
                'g-recaptcha-response' => 'required',  // for avoid automated request
                'username' => 'required|max:10|min:3|unique:user',
                'p_email' => 'required|min:5|max:100|email|unique:user',
                'password' => 'required|max:32|min:6',
                'user_role'=> 'required|in:std'  // check user role
            )
            );

        if($valid->fails()){ // if validation fails
            return Redirect::route('index')->with('error',$valid->messages());
        }
        else{ // validation pass
            $user = new User; // create new student user
            $user->username = Input::get('username');
            $user->password = Hash::make(Input::get('password'));
            $user->role = Input::get('user_role');
            $user->p_email = $inputs['p_email'];
            $token=$user->confirmation_token = str_random(60);
            $user->confirmed = 0;
            $user->save(); // save student info to user table via user model

            Mail::send('emails.confirmation', array('token_url' => URL::route('confirmation', $token),'task' => "std_mail"), function($message)
            {
                $message->to(Input::get('username').'@uom.lk')->subject('Activation CareerPlus!');
            }); // send confirmation email to *****@uom.lk email
            $data =array(
                'popup'=>'show',
                'popup_msg' => '<h4 class="text-success">Account Created !</h4>,<br/> Confirmation email was sent to '.$inputs['p_email'].' </br>',
                'redirect'=>'http://webmail.mrt.ac.lk',
                'but_msg'=>'Go to Web Mail &raquo;',
                'title'=>'Confirmation email was sent !');
            return View::make('public.index',$data); // return view with success msg
        }
    }

}