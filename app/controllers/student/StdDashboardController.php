<?php

class StdDashboardController extends BaseController {

    private  $std_data;
    private  $std_data2;
    private  $std_data1;

    public function initialize($page='home')
    {



        $data['page'] = $page;
        if($page === 'profile'){
            $std = DB::table('undergraduate')->where('user_id','=',Auth::id())->first();
            $stdMore = DB::table('more_detail')->where('undergraduate_id','=',Auth::id())->first();

            DB::setFetchMode(PDO::FETCH_ASSOC);
            $stdExp = DB::table('experience')->select('ex_id','title','descr')->where('undergraduate_id','=',Auth::id())->get();
            $stdResult = DB::table('result')->select('result_id','semester','gpa')->where('undergraduate_id','=',Auth::id())->get();
            $stdProj = DB::table('project')->select('proj_id','proj_title','proj_descr','members')->where('undergraduate_id','=',Auth::user()->id)->get();

            DB::setFetchMode(PDO::FETCH_CLASS);
            switch($std->faculty){
                case 'Efac':
                    $faculty = 'Faculty of Engineering';
                    break;
                case 'ITfac':
                    $faculty = 'Faculty of Information Technology';
                    break;
                case 'Afac':
                    $faculty = 'Faculty of Architecture';
                    break;
                default :
                    $faculty = '...';
            }
            $marital_status = "";
            if(isset($stdMore->marital_status)){
                switch($stdMore->marital_status){
                    case 'S':
                        $marital_status = 'Single';
                        break;
                    case 'M':
                        $marital_status = 'Married';
                        break;
                    case 'D':
                        $marital_status = 'Divorced';
                        break;
                    default :
                        $marital_status = '...';
                }
            }


            $this->std_data1 = array(
                'profile_pic' => Auth::user()->profile_pic,
                'name' => $std->name,
                'dob' => $std->dob,
                'n_email' => $std->n_email,
                'entrance_year' => $std->entrance_year,
                'faculty' => $faculty,
                'department' => $std->department,
                'Linkedin' => $std->Linkedin,
                'googleplus' => $std->googleplus,
                'result_id' => array_column($stdResult, 'result_id'),
                'semester' => array_column($stdResult, 'semester'),
                'gpa' => array_column($stdResult, 'gpa'),
                'proj_id' => array_column($stdProj, 'proj_id'),
                'proj_title' => array_column($stdProj, 'proj_title'),
                'proj_descr' =>  array_column($stdProj, 'proj_descr'),
                'members' =>array_column($stdProj, 'members'),

                'ex_id' => array_column($stdExp, 'ex_id'),
                'title' => array_column($stdExp, 'title'),
                'descr' => array_column($stdExp, 'descr')

            );
            if(isset($stdMore->more_id)){
                $this->std_data2 = array($this->std_data,
                    'more_id' => $stdMore->more_id,
                    'summary' => $stdMore->summary,
                    'marital_status' => $marital_status,
                    'employe' => $stdMore->employe);

                    $this->std_data = array_merge($this->std_data1,$this->std_data2);

            }else{
                $this->std_data = $this->std_data1;
            }







            return View::make('users.student.dashboard',$data)->with($this->std_data);

        }else{
            /*$std = DB::table('students')->select('profile_pic', 'name')->where('user_id','=',Auth::user()->id)->first();
            Auth::user()->setAttribute('profile_pic',$std->profile_pic);
            Auth::user()->setAttribute('name',$std->name);*/
        }
        return View::make('users.student.dashboard',$data);
    }
}
