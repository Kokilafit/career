<?php

class ProfileUpdateController2 extends BaseController
{


    public function updateProfile()
    {
        $data = Input::all();
        dd($data);
        $extra_uid = "";
        $table ="";
        $index= "";
        $pk ="";
        $pk =Input::get('pk');

        $valuename = Validator::make($data, array('name' => 'required|max:20|min:3'));
        if($valuename->fails()){
            return Response::json(array('success' => false , 'msg' => 'This is not a valid request !'));
        }else{
            switch(Input::get('name')){
                case   'name':
                    $table = 'undergraduate';
                    $extra_uid = 'user_id';
                    $validation_str = 'max:150|min:3';
                    break;
                case   'dob':
                    $table = 'undergraduate';
                    $extra_uid = 'user_id';
                    $validation_str = 'max:300|min:3';
                    break;
                case   'n_email':
                    $table = 'undergraduate';
                    $extra_uid = 'user_id';
                    $validation_str = 'max:300|min:3';
                    break;
                case   'entrance_year':
                    $validation_str = 'max:300|min:3';
                    break;
                case   'faculty':
                    $table = 'undergraduate';
                    $extra_uid = 'user_id';
                    $validation_str = 'max:300|min:3';
                    break;
                case   'department':
                    $table = 'undergraduate';
                    $extra_uid = 'user_id';
                    $validation_str = 'max:300|min:3';
                    break;
                case   'Linkedin':
                    $table = 'undergraduate';
                    $extra_uid = 'user_id';
                    $validation_str = 'max:300|min:3';
                    break;
                case   'googleplus':
                    $table = 'undergraduate';
                    $extra_uid = 'user_id';
                    $validation_str = 'max:300|min:3';
                    break;
                case   'result_id':
                    $extra_uid = "result_uid";
                    $validation_str = 'max:300|min:3';
                    break;
                case   'semester':
                    $extra_uid = "result_uid";
                    $validation_str = 'max:300|min:3';
                    break;
                case   'gpa':
                    $extra_uid = "result_uid";
                    $validation_str = 'max:300|min:3';
                    break;
                case   'proj_id':
                    $table = "project";
                    $extra_uid = "proj_id";
                    $validation_str = 'max:300|min:3';
                    break;
                case   'proj_title':

                    $table = "project";
                    $index = "proj_id";
                    $extra_uid = "undergraduate_id";
                    $validation_str = 'max:300|min:3';
                    break;
                case   'proj_descr':
                    $table = "project";
                    $index = "proj_id";
                    $extra_uid = "undergraduate_id";
                    $validation_str = 'max:300|min:3';
                    break;
                case   'members':
                    $index = "proj_id";
                    $index = "proj_id";
                    $extra_uid = "undergraduate_id";
                    $validation_str = 'max:300|min:3';
                    break;
                case   'more_id':
                    $extra_uid = "more_uid";
                    $validation_str = 'max:300|min:3';
                    break;
                case   'summary':
                    $table = "more_detail";
                    $extra_uid = "undergraduate_id";
                    $validation_str = 'max:300|min:3';
                    break;
                case   'marital_status' :
                    $table = "more_detail";
                    $extra_uid = "undergraduate_id";
                    $validation_str = 'max:300|min:1';
                    break;
                case   'employe':
                    $extra_uid = "more_uid";
                    $validation_str = 'max:300|min:3';
                    break;
                case  'ex_id':
                    $extra_uid = "ex_id";
                    $validation_str = 'max:300|min:3';
                    break;
                case   'title':
                    $index = "ex_id";
                    $table = "experience";

                    $extra_uid = "undergraduate_id";
                    $validation_str = 'max:300|min:3';
                    break;
                case   'descr':
                    $index = "ex_id";
                    $table = "experience";
                    $extra_uid = "undergraduate_id";
                    $validation_str = 'max:300|min:3';
                    break;
                case   'skills_pool_id' :
                    $index = "skills_pool_id";
                    $table = "skills";
                    $extra_uid = "undergraduate_id";
                    $validation_str = 'max:300|min:3';
                    break;
                case   'percentage':
                    $index = "skills_pool_id";
                    $table = "skills";
                    $extra_uid = "undergraduate_id";
                    $validation_str = 'max:300|min:3';
                    break;
                default:
                    return Response::json(array('success' => false, 'msg' => 'This is not a valid request..!'));
            }
            $value = Validator::make($data, array('value' => $validation_str));
            if($value->fails()){
                return Response::json(array('success' => false, 'msg' => 'Your input has invalid content !'));
            }if($pk!=null){
                $this->saveDB2(Input::get('name'),Input::get('value'),$extra_uid,$table,$index,$pk);
            }
            else {

                $this->saveDB(Input::get('name'), Input::get('value'), $extra_uid, $table);
            }
        }
    }

    private function saveDB($name,$value,$extra_uid="",$table){
        $qu = DB::table($table)->where($extra_uid, '=', Auth::user()->id)->update(array($name => $value));

        if($qu==0){

            $qu = DB::table($table)->insert(array($extra_uid => Auth::user()->id , $name => $value));

        }

        if($qu>0){
            return Response::json(array('success' => true));
        }else{
            return Response::json(array('success' => false, 'msg' => 'DB Query failed..!'));//Input::all()
        }

    }



    private function saveDB2($name,$value,$extra_uid="",$table,$index,$pk){
        $place = DB::table($table)->where($extra_uid,'=',Auth::user()->id)->pluck($index);

        $qu = DB::table($table)->where($extra_uid, '=', Auth::user()->id)->where($index, '=',$place+ $pk)->update(array($name => $value));

        if($qu==0){

            $qu = DB::table($table)->insert(array($extra_uid => Auth::user()->id , $name => $value));

        }

        if($qu>0){
            return Response::json(array('success' => true));
        }else{
            return Response::json(array('success' => false, 'msg' => 'DB Query failed..!'));//Input::all()
        }

    }
}