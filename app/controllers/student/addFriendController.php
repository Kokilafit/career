<?php

class addFriendController extends BaseController{
    public function addFriend(){
        DB::table('friends')->insert(
            array('requester' => Auth::user()->id , 'acceptor' => Input::get('uid') ,'state'=> 0)
        );
    }
    public function removeFriend(){
        DB::table('friends')->where('acceptor', Input::get('uid'))->where('requester', Auth::user()->id)->delete();
        }
    public function showFriends(){
        $test = DB::table('friends')->join('user','user.id','=','friends.acceptor')->select('user.profile_pic')->where('friends.state',1)->get();
        $test2 = DB::table('friends')->join('user','user.id','=','friends.acceptor')->select('user.profile_pic')->where('friends.state',0)->get();
            return View::make('users.student.dashboard', array('page'=>'friends', 'pics' => $test , 'pends' => $test2));

        //return View::make('users.student.friends', array('results' => $test));
    }

   /* public function changeButton(){
        $test = DB::table('friends')->select('state')->where('requester', Auth::user()->id , 'acceptor', Input::get('uid'))->first();
        return value(test);
    }*/
}


