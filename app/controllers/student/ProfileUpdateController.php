<?php

class ProfileUpdateController extends BaseController
{

    private $data;
    private $std;

    public function ProfileInit()
    {

        $data['page'] = 'profile';

        DB::setFetchMode(PDO::FETCH_ASSOC);
            $this->std = DB::table('undergraduate')->where('user_id', '=', Auth::id())->first();
            $stdMore = DB::table('more_detail')->where('undergraduate_id', '=', Auth::id())->first();
            $stdExp = DB::table('experience')->select('ex_id', 'title', 'descr')->where('undergraduate_id', '=', Auth::id())->get();
            $stdResult = DB::table('result')->select('result_id', 'semester', 'gpa')->where('undergraduate_id', '=', Auth::id())->get();
            $stdProj = DB::table('project')->select('proj_id', 'proj_title', 'proj_descr', 'members')->where('undergraduate_id', '=', Auth::user()->id)->get();
        DB::setFetchMode(PDO::FETCH_CLASS);

        switch ($this->std['faculty']) {
                case 'Efac':
                    $this->std['faculty'] = 'Faculty of Engineering';
                    break;
                case 'ITfac':
                    $this->std['faculty'] = 'Faculty of Information Technology';
                    break;
                case 'Afac':
                    $this->std['faculty'] = 'Faculty of Architecture';
                    break;
                default :
                    $faculty = '...';
            }

        switch ($this->std['department']) {
            case 'DArc':
                $this->std['department'] = 'Department of Architecture';
                break;
            case 'DID':
                $this->std['department'] = 'Department of Integrated Design';
                break;
            case 'DBE':
                $this->std['department'] = 'Department of Building Economics';
                break;
            case 'DTCP':
                $this->std['department'] = 'Department of Town & Country Planning';
                break;
            case 'DCPE':
                $this->std['department'] = 'Department of Chemical & Process Engineering';
                break;
            case 'DCE':
                $this->std['department'] = 'Civil Engineering';
                break;
            case 'DCSE':
                $this->std['department'] = 'Department of computer Science & Engineering';
                break;
            case 'DER':
                $this->std['department'] = 'Earth Resource Engineering';
                break;
            case 'DTEE':
                $this->std['department'] = 'Electrical Engineering';
                break;
            case 'DENTC':
                $this->std['department'] = 'Electronic & Telecommunication Engineering';
                break;
            case 'DMSE':
                $this->std['department'] = 'Material Science & Engineering';
                break;
            case 'DMech':
                $this->std['department'] = 'Mechanical Engineering';
                break;
            case 'DCT':
                $this->std['department'] = 'Textile & Clothing Technology';
                break;
            case 'DTLM':
                $this->std['department'] = 'Transport & Logistics Management';
                break;
            default :
                $this->std['department'] = '...';
        }

        switch ($stdMore['marital_status']) {
            case 'S':
                $this->std['marital_status'] = 'Single';
                break;
            case 'M':
                $this->std['marital_status'] = 'Married';
                break;
            case 'D':
                $this->std['marital_status'] = 'Divorced';
                break;
            default :
                $this->std['marital_status'] = '...';
        }

        $this->std['profile_pic'] = Auth::user()->profile_pic;
        $this->std['more_id'] = $stdMore['more_id'];
        $this->std['summary'] = $stdMore['summary'];
        //$this->std['marital_status'] = $stdMore['marital_status'];
        $this->std['employe'] = $stdMore['employe'];
        $this->std['result_id'] = array_column($stdResult, 'result_id');
        $this->std['semester'] = array_column($stdResult, 'semester');
        $this->std['gpa'] = array_column($stdResult, 'gpa');
        $this->std['proj_id'] = array_column($stdProj, 'proj_id');
        $this->std['proj_title'] = array_column($stdProj, 'proj_title');
        $this->std['proj_descr'] = array_column($stdProj, 'proj_descr');
        $this->std['members'] = array_column($stdProj, 'members');
        $this->std['ex_id'] = array_column($stdExp, 'ex_id');
        $this->std['title'] = array_column($stdExp, 'title');
        $this->std['descr'] = array_column($stdExp, 'descr');

        return View::make('users.student.dashboard', $data)->with($this->std);

    }


    public function updateProfile()
    {
        $this->data = Input::all();
        $table = 'undergraduate';
        $extra_uid = 'user_id';
        $index_column = "";

        $valuename = Validator::make($this->data, array('name' => 'required|max:20|min:3'));
        if($valuename->fails()){
            return Response::json(array('success' => false , 'msg' => 'This is not a valid request !'));
        }else{
            switch(Input::get('name')){
                case   'name':
                    $validation_str = 'max:150|min:3';
                    $this->validate($validation_str);
                    break;
                case   'dob':
                    $table = 'undergraduate';
                    $extra_uid = 'user_id';
                    $validation_str = 'max:300|min:3';
                    $this->validate($validation_str);
                    break;
                case   'n_email':
                    $validation_str = 'email|max:100|min:5';
                    $this->validate($validation_str);
                    break;
                case   'entrance_year':
                    $validation_str = 'max:300|min:3';
                    $this->validate($validation_str);
                    break;
                case   'faculty':
                    $validation_str = 'max:5|min:4';
                    DB::table($table)->where($extra_uid, '=', Auth::user()->id)->update(array('department' => ''));
                    $this->validate($validation_str);
                    break;
                case   'department':
                    $table = 'undergraduate';
                    $extra_uid = 'user_id';
                    $validation_str = 'max:300|min:3';
                    $this->validate($validation_str);
                    break;
                case   'Linkedin':
                    $table = 'undergraduate';
                    $extra_uid = 'user_id';
                    $validation_str = 'max:300|min:3';
                    $this->validate($validation_str);
                    break;
                case   'googleplus':
                    $table = 'undergraduate';
                    $extra_uid = 'user_id';
                    $validation_str = 'max:300|min:3';
                    break;
                case   'mobile_no':
                    $validation_str = 'max:15|min:10';
                    break;
                case   'result_id':
                    $extra_uid = "result_uid";
                    $validation_str = 'max:300|min:3';
                    break;
                case   'semester':
                    $extra_uid = "result_uid";
                    $validation_str = 'max:300|min:3';
                    break;
                case   'gpa':
                    $extra_uid = "result_uid";
                    $validation_str = 'max:300|min:3';
                    break;
                case   'proj_id':
                    $table = "project";
                    $extra_uid = "proj_id";
                    $validation_str = 'max:300|min:3';
                    break;
                case   'proj_title':

                    $table = "project";
                    $index = "proj_id";
                    $extra_uid = "undergraduate_id";
                    $validation_str = 'max:300|min:3';
                    break;
                case   'proj_descr':
                    $table = "project";
                    $index = "proj_id";
                    $extra_uid = "undergraduate_id";
                    $validation_str = 'max:300|min:3';
                    break;
                case   'members':
                    $index = "proj_id";
                    $index = "proj_id";
                    $extra_uid = "undergraduate_id";
                    $validation_str = 'max:300|min:3';
                    break;
                case   'more_id':
                    $extra_uid = "more_uid";
                    $validation_str = 'max:300|min:3';
                    break;
                case   'summary':
                    $table = "more_detail";
                    $extra_uid = "undergraduate_id";
                    $validation_str = 'max:300|min:3';
                    break;
                case   'marital_status' :
                    $table = "more_detail";
                    $extra_uid = "undergraduate_id";
                    $validation_str = 'max:300|min:1';
                    break;
                case   'employe':
                    $extra_uid = "more_uid";
                    $validation_str = 'max:300|min:3';
                    break;
                case  'ex_id':
                    $extra_uid = "ex_id";
                    $validation_str = 'max:300|min:3';
                    break;
                case   'title':
                    $index_column = "ex_id";
                    $extra_uid = "undergraduate_id";
                    $table = "experience";
                    $validation_str = 'max:300|min:3';
                    break;
                case   'descr':
                    $index_column = "ex_id";
                    $extra_uid = "undergraduate_id";
                    $table = "experience";
                    $validation_str = 'max:300|min:3';
                    break;
                case   'skills_pool_id' :
                    $index = "skills_pool_id";
                    $table = "skills";
                    $extra_uid = "undergraduate_id";
                    $validation_str = 'max:300|min:3';
                    break;
                case   'percentage':
                    $index = "skills_pool_id";
                    $table = "skills";
                    $extra_uid = "undergraduate_id";
                    $validation_str = 'max:300|min:3';
                    break;
                default:
                    return Response::json(array('success' => false, 'msg' => 'This is not a valid request..!'));
            }
            
            $this->saveDefault(Input::get('name'), Input::get('value'), $extra_uid, $table,$index_column);
        }
    }


    private function validate($validation_str){
        $value = Validator::make($this->data, array('value' => $validation_str));
        if($value->fails()){
            return Response::json(array('success' => false, 'msg' => 'Your input has invalid content !'));
        }else{
            return true;
        }
    }




    private function saveDefault($name,$value,$extra_uid="",$table,$index_column){

        if(DB::table($table)->where($extra_uid, '=', Auth::user()->id)->get()){
            if($index_column===""){
                $qu = DB::table($table)->where($extra_uid, Auth::user()->id)->update(array($name => $value));
            }else{
                if(empty(Input::get('pk'))) {
                    $qu = DB::table($table)->insert(array($extra_uid => Auth::user()->id , $name => $value));
                }
                else{
                    $qu = DB::table($table)->where($extra_uid, Auth::user()->id)->where($index_column, Input::get('pk') )->update(array($name => $value));
                }
            }
        }else{
            $qu = DB::table($table)->insert(array($extra_uid => Auth::user()->id , $name => $value));
        }
        return Response::json(array('success' => true));

    }




    public function add_new_exp(){


        DB::table('experience')->insert(array('undergraduate_id' => Auth::user()->id , 'title' => ''));

        DB::setFetchMode(PDO::FETCH_ASSOC);
        $stdExp = DB::table('experience')->select('ex_id', 'title', 'descr')->where('undergraduate_id', '=', Auth::id())->get();
        DB::setFetchMode(PDO::FETCH_CLASS);

        $std['ex_id'] = array_column($stdExp, 'ex_id');
        $std['title'] = array_column($stdExp, 'title');
        $std['descr'] = array_column($stdExp, 'descr');
        if (Request::ajax()) {
            return Response::json(View::make('users.student.add_exp')->with($std)->render());
        }
    }

    public function remove_exp(){

        if (Request::ajax()) {
            DB::table('experience')->where('ex_id', Input::get('id'))->where('undergraduate_id', Auth::id())->delete();
        }

        DB::setFetchMode(PDO::FETCH_ASSOC);
        $stdExp = DB::table('experience')->select('ex_id', 'title', 'descr')->where('undergraduate_id', '=', Auth::id())->get();
        DB::setFetchMode(PDO::FETCH_CLASS);

        $std['ex_id'] = array_column($stdExp, 'ex_id');
        $std['title'] = array_column($stdExp, 'title');
        $std['descr'] = array_column($stdExp, 'descr');
        if (Request::ajax()) {
            return Response::json(View::make('users.student.add_exp')->with($std)->render());
        }
    }
    
}