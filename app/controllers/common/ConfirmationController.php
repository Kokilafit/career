<?php

class ConfirmationController extends BaseController
{

    public function getToken($token="default",$do="conf")
    {
        $token = str_limit($token, 60);
        $do = str_limit($do,10);

        switch($this->checkToken($token,$do)){ // according to the confirmation token, responses to the users
            case "ind_sent_admin" :
                $data = array(
                    'popup' => 'show',
                    'popup_msg' => '<h4 class="text-success">Your Account is waiting for Admin approval !
                    </br> you will receive notification email soon !</h4>',
                    'title' => 'email verification success !');
                return View::make('public.index', $data);
                break;

            case "std_active" :
                $data = array(
                    'popup' => 'show',
                    'popup_msg' => '<h4 class="text-success">Welcome to CareerPlus</h4>',
                    'title' => 'Account Confirmed !');
                return View::make('public.index', $data);
                break;

            case "alm_active" :
                $data = array(
                    'popup' => 'show',
                    'popup_msg' => '<h4 class="text-success">Welcome to CareerPlus</h4>',
                    'title' => 'Account Confirmed !');
                return View::make('public.index', $data);
                break;

            default :
                $data = array(
                    'popup' => 'show',
                    'popup_msg' => '<h4 class="text-warning">Not a valid confirmation link</h4>',
                    'title' => 'Account not Confirmed !');
                return View::make('public.index', $data);

        }
    }


    private function checkToken($token,$do)
    {
        if($do==="ind_mail"){ // if confirmation link is belong to industry user
            $task = DB::table('user')
                ->where('confirmed','=',3)
                ->where('role','=','ind')
                ->where('confirmation_token','=',$token)
                ->update(array('confirmed' => 2));
            if($task){ // if confirmation token is valid
                return "ind_sent_admin";
            }
            else{
                return false;
            }
        }

        elseif($do==="std_mail"){ // if confirmation link is belong to student user
            $task = DB::table('user')
                ->where('confirmed','=',0)
                ->where('role','=','std')
                ->where('confirmation_token','=',$token)
                ->update(array('confirmed' => 1));
            if($task){ // if confirmation token is valid
                return "std_active";
            }
            else{
                return false;
            }
        }
        elseif($do==="alm_mail"){ // if confirmation link is belong to student user
            $task = DB::table('user')
                ->where('confirmed','=',0)
                ->where('role','=','alm')
                ->where('confirmation_token','=',$token)
                ->update(array('confirmed' => 1));
            if($task){ // if confirmation token is valid
                return "alm_active";
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
    }

}