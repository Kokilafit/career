<?php

class SignInController extends BaseController{

//logout method for all users-------------------------------
    public  function logOut(){
        Auth::logout();
        return Redirect::route('index');
    }
//End of logout method--------------------------------------


    public function postSignIn(){
//Validator for users who login with email------------------
        $valid_email = Validator::make(Input::all(),
            array(
                'username' => 'required|email|max:100|min:5',
                'password' => 'required|max:32|min:6'
            ));
        if(!$valid_email->fails()){
//end of email validation block----------------------------


//login with email--------------------------------------------
            $auth = Auth::attempt(array(
                'p_email' => Input::get('username'),
                'password' => Input::get('password'),
                'confirmed' => 1
            ));
            if($auth){
                $role = Auth::user()->role;
                switch($role){
                    case 'adm':
                        return Redirect::route('adm_dashboard');
                        break;
                    case 'ind':
                        return Redirect::route('ind_dashboard');
                        break;
                    case 'std':
                        return Redirect::route('std_dashboard');
                        break;
                    case 'alm':
                        return Redirect::route('alm_dashboard');
                        break;
                    default:
                        $this::logOut();
                }
            }
//End of login with email--------------------------------------

//login error msg------------------------------------------
                $data =array(
                    'popup'=>'show',
                    'popup_msg' => '<h4 class="text-danger">Please retry with valid details.</h4>',
                    'title'=>'Login Details Incorrect !');
                return View::make('public.index',$data);
//end of login error msg-----------------------------------

        }
        else{

//Validator for users who login with username------------------
            $valid_username = Validator::make(Input::all(),
                array(
                    'username' => 'required|max:10|min:3|alpha_num',
                    'password' => 'required|max:32|min:6'
                ));
            if(!$valid_username->fails()){
//end of username validation block----------------------------


//login with username--------------------------------------------
                $auth = Auth::attempt(array(
                    'username' => Input::get('username'),
                    'password' => Input::get('password'),
                    'confirmed' => 1
                ));
                if($auth){
                    $role = Auth::user()->role;
                    switch($role){
                        case 'adm':
                            return Redirect::route('adm_dashboard');
                            break;
                        case 'ind':
                            return Redirect::route('ind_dashboard');
                            break;
                        case 'std':
                            return Redirect::route('std_dashboard');
                            break;
                        case 'alm':
                            return Redirect::route('alm_dashboard');
                            break;
                        default:
                            $this::logOut();
                    }
                }
//End of login with username--------------------------------------

//login error msg------------------------------------------
                $data =array(
                    'popup'=>'show',
                    'popup_msg' => '<h4 class="text-danger">Please retry with valid details..</h4>',
                    'title'=>'Login Details Incorrect !');
                return View::make('public.index',$data);

//end of login error msg-----------------------------------

            }
            else{

//validation failed error msg------------------------------------------
                $data =array(
                    'popup'=>'show',
                    'popup_msg' => '<h4 class="text-danger">Please retry with valid details...</h4>',
                    'title'=>'Login Details Incorrect !');
                return View::make('public.index',$data);
//end of validation failed error msg-----------------------------------

            }
        }
    }
}