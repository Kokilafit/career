<?php

class GetTagController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $q = Input::get('query');
        $data = DB::table('skills_pool')->select(array('skill'))->where('skill','LIKE','%'.$q.'%')->get();
        return Response::json($data);
	}


}
