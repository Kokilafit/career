<?php

class AlmDashboardController extends BaseController {

    private  $alm_data;

    public function initialize($page='home')
    {



        $data['page'] = $page;
        if($page === 'profile'){
            $alm = DB::table('alumni')->where('id','=',Auth::user()->id)->first();
            $emp = DB::table('employee')->where('id','=',Auth::user()->id)->first();
            switch($alm->faculty){
                case 'Efac':
                    $faculty = 'Faculty of Engineering';
                    break;
                case 'ITfac':
                    $faculty = 'Faculty of Information Technology';
                    break;
                case 'Afac':
                    $faculty = 'Faculty of Architecture';
                    break;
                default :
                    $faculty = '...';
            }
            switch($alm->dep){
                case 'DCPE':
                    $dep = 'Department of Chemical & Process Engineering';
                    break;
                case 'DCE':
                    $dep = 'Civil Engineering';
                    break;
                case 'DCSE':
                    $dep = 'Department of computer Science & Engineering';
                    break;
                case 'DER':
                    $dep = 'Earth Resource Engineering';
                    break;
                case 'DTEE':
                    $dep = 'Electrical Engineering';
                    break;
                case 'DENTC':
                    $dep = 'Electronic & Telecommunication Engineering';
                    break;
                case 'DMSE':
                    $dep = 'Material Science & Engineering';
                    break;
                case 'DMech':
                    $dep = 'Mechanical Engineering';
                    break;
                case 'DCT':
                    $dep = 'Textile & Clothing Technologyg';
                    break;
                case 'DTLM':
                    $dep = 'Transport & Logistics Management';
                    break;
                default :
                    $dep = '...';
            }


            $this->alm_data = array(
                'profile_pic' => Auth::user()->profile_pic,
                'name' => $alm->name,
                'faculty' => $faculty,
                'dep' => $dep,
                'g_year' => $alm->g_year,
                'phone_no' => $alm->phone_no,
                'linkedin' => $alm->linkedin,
                'workplace' => $emp->workplace,
                'join_year' => $emp->join_year,
                'designation' => $emp->designation
            );
            return View::make('users.alumni.dashboard',$data)->with($this->alm_data);

        }else{
            $alm = DB::table('alumni')->select('name')->where('id','=',Auth::user()->id)->first();
            Auth::user()->setAttribute('name',$alm->name);
        }
        return View::make('users.alumni.dashboard',$data);
    }
}
