<?php
/*
 * this is the supper class for users registration process
 * this will be extended by Student registration and
 * company registration controllers
 * */



class AluminaVerifyController extends BaseController{

    public function verify(){
        $inputs = Input::all();

        $valid = Validator::make($inputs, // validate inputs
            array(
                //'g-recaptcha-response' => 'required',  //for avoid automated request
                'surname' => 'required|max:50|min:3',
                'index' => 'required|min:6|max:15',
                'nic' => 'required|max:10',
                'cert'=> 'required|max:10'
            )
            );

        if($valid->fails()){ // if validation fails
            return Redirect::route('index')->with('error',$valid->messages());
        }
        else{ //validation passed

            $client = new \GuzzleHttp\Client(); // call validation api using GuzzleHttp (via cURL)
            $response = $client
                ->get('http://api.mrt.ac.lk/api/v1/'.Input::get('index').'/'.Input::get('nic').'/'.Input::get('surname').'/'.Input::get('cert').'');
            if($response->json()['msg']==='verified'){ // if api response msg:verified
                if (Request::ajax()) {
                    return Response::json(View::make('users.alumni.popup.Signup')->render(),200,(array("msg"=>'valid')));
                }
            }
            else{
                return Response::json(View::make('users.alumni.popup.error')->render(),200,(array("msg"=>'invalid')));
            }

        }
    }

}



/*$user = new User;
$user->username = Input::get('username');
$user->password = Hash::make(Input::get('password'));
$user->role = Input::get('user_role');
$user->p_email = $inputs['p_email'];
$token=$user->confirmation_token = str_random(60);
$user->confirmed = 0;
$user->save();

Mail::send('emails.confirmation', array('token_url' => URL::route('confirmation', $token),'task' => "std_mail"), function($message)
{
    $message->to(Input::get('username').'@uom.lk')->subject('Activation CareerPlus!');
});
$data =array(
    'popup'=>'show',
    'popup_msg' => '<h4 class="text-success">Account Created !</h4>,<br/> Confirmation email was sent to '.$inputs['p_email'].' </br>',
    'redirect'=>'http://webmail.mrt.ac.lk',
    'but_msg'=>'Go to Web Mail &raquo;',
    'title'=>'Confirmation email was sent !');
return View::make('public.index',$data);*/