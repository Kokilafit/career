<?php

class AlmProfileBuildController extends BaseController{

    public function buildProfile(){



        $inputs = Input::all();
        $valid = Validator::make($inputs,
            array(
                'name' => 'required|max:100|min:3',
                'g_year' => 'required|digits:4',
                'fac' => 'required|in:Efac,ITfac,Afac',
                'dep' => 'in:DTLM,DCT,DMech,DMSE,DENTC,DTEE,DER,DCSE,DCE,DCPE,DTCP,DBE,DID,DArc',
                'linkedin' => 'max:250', //active_url|
                'phone' => 'max:14|min:10',
                'workplace' => 'max:150|min:2',
                'designation' => 'max:150|min:2',
                'j_year' => 'digits:4'
            )
        );

        if ($valid->fails() || Auth::user()->role !== 'alm' || !Input::has('image-data')) {
            var_dump($valid->messages());
            //return Redirect::to('/test')->with('error', $valid->messages());
        } else {
            $task = DB::table('alumni')
                ->insert(array(
                    'id' => Auth::user()->id,
                    'name' => Input::get('name'),
                    'faculty' => Input::get('fac'),
                    'dep' => Input::get('dep'),
                    'g_year' => Input::get('g_year'),
                    'phone_no' => Input::get('phone'),
                    'linkedin' => Input::get('linkedin')
                ));
            if(isset($inputs['workplace'])){
                $task = DB::table('employee')
                    ->insert(array(
                        'id' => Auth::user()->id,
                        'workplace' => Input::get('workplace'),
                        'designation' => Input::get('designation'),
                        'join_year' => Input::get('j_year')
                    ));
            }

            $profile_pic_filename  = uniqid() . '.png';
            $data = Input::get('image-data');
            list($type, $data) = explode(';', $data);
            list(, $data) = explode(',', $data);
            $data = base64_decode($data);
            if (file_put_contents('uploads/' . $profile_pic_filename, $data) === false) {
                //return Redirect::route('index')->with('error', $valid->messages());
            } else {
                $pic = DB::table('user')
                    ->where('id', '=', Auth::user()->id)
                    ->update(array('profile_pic' => '/uploads/' . $profile_pic_filename));
            }
            DB::table('user')
                ->where('id','=',Auth::user()->id)
                ->where('confirmation_token','=',Auth::user()->confirmation_token)
                ->update(array('confirmation_token' => ""));

            return Redirect::to('/alumni/profile');
        }

    }

}
