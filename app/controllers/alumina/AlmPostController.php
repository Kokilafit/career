<?php

class AlmPostController extends BaseController
{
    public function add()
    {
        $inputs = Input::all();

            if(isset($inputs['img'])){  //check img existence

                $valid = Validator::make($inputs,
                    array(
                        'title' => 'required|max:100|min:3',
                        'description' => 'required|max:500|min:10',
                        'img' => 'image|mimes:jpeg,jpg,png'
                    )
                );

                        if ($valid->fails() || Auth::user()->role !== 'alm') {
                            dd($valid->messages());
                            return View::make('users.alumni.dashboard')->with(array('page'=>"alm_portal",'error', $valid->messages()));

                        }
                        else
                        {
                            $img_fname = uniqid(Auth::user()->id, true) . '.png';
                            $task = DB::table('post')
                                ->insert(array(
                                    'uid' => Auth::user()->id,
                                    'title' => Input::get('title'),
                                    'desc' => Input::get('description'),
                                    'img' => $img_fname,
                                    'cat' => 'alm-post'
                                ));
                            Input::file('img')->move(base_path() . '/public/post_img', $img_fname);

                        }

            }
            else
            {

                $valid = Validator::make($inputs,
                    array(
                        'title' => 'required|max:100|min:3',
                        'description' => 'required|max:500|min:10'
                    )
                );

                if ($valid->fails() || Auth::user()->role !== 'alm') {
                    dd($valid->messages());
                    return View::make('users.alumni.dashboard')->with(array('page'=>"alm_portal",'error', $valid->messages()));
                }
                else
                {
                    $task = DB::table('post')
                        ->insert(array(
                            'uid' => Auth::user()->id,
                            'title' => Input::get('title'),
                            'desc' => Input::get('description'),
                            'cat' => 'alm-post'
                        ));

                }

            }
        $posts = DB::table('post')->where('uid',Auth::id())->orderBy('id', 'desc')->get();

        return View::make('users.alumni.dashboard')->with(array('page'=>"alm_portal",'posts'=>$posts));

        }





    public function init(){
        $posts = DB::table('post')->where('uid',Auth::id())->orderBy('id', 'desc')->get();

       return View::make('users.alumni.dashboard')->with(array('page'=>"alm_portal",'posts'=>$posts));
    }
}




