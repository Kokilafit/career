<?php
/*
 * this is the supper class for users registration process
 * this will be extended by Student registration and
 * company registration controllers
 * */



class AlmSignUpController extends BaseController{

    public function postSignUp(){
        $inputs = Input::all();
        $valid = Validator::make($inputs,
            array(
                //'g-recaptcha-response' => 'required',  // for avoid automated request
                'username' => 'required|max:10|min:3|unique:user',
                'p_email' => 'required|min:5|max:100|email|unique:user',
                'password' => 'required|max:32|min:6',
                'user_role'=> 'required|in:alm'  // check user role
            )
            );

        if($valid->fails()){ // if validation fails
            return Redirect::route('index')->with('error',$valid->messages());
        }
        else{ // validation pass
            $user = new User; // create new student user
            $user->username = Input::get('username');
            $user->password = Hash::make(Input::get('password'));
            $user->role = Input::get('user_role');
            $user->p_email = $inputs['p_email'];
            $token=$user->confirmation_token = str_random(60);
            $user->confirmed = 0;
            $user->save(); // save student info to user table via user model

            Mail::send('emails.confirmation', array('token_url' => URL::route('confirmation', $token),'task' => "alm_mail"), function($message)
            {
                $message->to(Input::get('username').'@uom.lk')->subject('Activation CareerPlus!');
            }); // send confirmation email to *****@uom.lk email

            $inbox = $this->findinbox(Input::get('p_email'));

            $data =array(
                'popup'=>'show',
                'popup_msg' => '<h4 class="text-success">Account Created !</h4>,<br/> Confirmation email was sent to '.$inputs['p_email'].' </br>',
                'redirect'=>$inbox[0],
                'but_msg'=>$inbox[1],
                'title'=>'Confirmation email was sent !');
            return View::make('public.index',$data);
        }
    }

    private function findinbox($email){

        $domain = explode('@', strtolower($email))[1];
        $gmail = array("gmail.com");
        $yahoo_mail = array("ymail.com","yahoo.com");

        switch(true){
            case in_array($domain,$gmail) :
                return array("https://mail.google.com","Go to Gmail &raquo;");
                break;
            case in_array($domain,$yahoo_mail) :
                return array("https://mail.google.com","Go to Gmail &raquo;");
                break;
            default:
                return array("https://mail.google.com","Go to Gmail &raquo;");
        }

    }

}

