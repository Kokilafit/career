<?php
use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


/*
|--------------------------------------------------------------------------
| Authentication Routest
|--------------------------------------------------------------------------
*/
Route::get('signup',array('as'=>'signup','uses'=>'StdSignUpController@getSignUp'));

Route::post('std_signup',array('as'=>'stdsignup','uses'=>'StdSignUpController@postSignUp'));

Route::post('ind_signup',array('as'=>'indsignup','uses'=>'IndSignUpController@postSignUp'));

Route::post('alm_signup',array('as'=>'almsignup','uses'=>'AlmSignUpController@postSignUp'));

Route::get('/confirmation/{token?}/{do?}',array('as' => 'confirmation','uses' => 'ConfirmationController@getToken'));

Route::post('signin',array('as' => 'signin','uses' => 'SignInController@postSignIn'));

Route::get('/logout/',array('as' => 'logout','uses' => 'SignInController@logOut'));

Route::post('/alumina_api_caller/',array('as' => 'AluminaVerifier','uses' => 'AluminaVerifyController@verify'));

Route::get('get_token',array('as'=>'get_token','uses'=>'GetTagController@index'));


//End of Authentication Routes ----------------------------------------------------



/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
*/
Route::group(array('before' => 'isAdmin'), function()
{

    Route::group(array('before' => 'profile_filled'), function() {
        Route::get('admin/{page?}', array('as' => 'adm_dashboard', 'uses' => 'AdmDashboardController@initialize'));
    });
    Route::get('adm_search',array('as'=>'adm_search','uses'=>'AdmSearchController@search'));
    Route::get('A_result_data',array('as'=>'A_result_data','uses'=>'AdmSearchController@pop_result'));

    Route::post('adm_post',array('as'=>'adm_post','uses'=>'AdmPostController@add'));

    Route::get('adm_post_init',array('as'=>'adm_post_init','uses'=>'AdmPostController@init'));


    Route::get('adm_accept_ind',array('as'=>'adm_accept_ind','uses'=>'AdmAcceptIndController@load'));

    Route::get('adm_ind_acceptor',array('as'=>'adm_ind_acceptor','uses'=>'AdmAcceptIndController@accept'));

    Route::get('show_de',array('as'=>'show_de','uses'=>'AdmAcceptIndController@showDetail'));

    Route::get('ind_acc',array('as'=>'ind_acc','uses'=>'AdmAcceptIndController@accept'));

    Route::get('ind_rej',array('as'=>'ind_rej','uses'=>'AdmAcceptIndController@reject'));




    Route::get('web_admin',function(){
        return View::make('users.admin.web_admin')->render();
    });
    Route::post('web_admin',function(){
        return View::make('users.admin.web_admin')->render();
    });
});

//End of Admin Routes ----------------------------------------------------




/*
|--------------------------------------------------------------------------
| Student Routes
|--------------------------------------------------------------------------
*/

Route::group(array('before' => 'isStudent'), function()
{

    Route::group(array('before' => 'profile_filled'), function() {
        Route::get('student/{page?}', array('as' => 'std_dashboard', 'uses' => 'StdDashboardController@initialize'));
    });

    Route::post('profile_build',array('as'=>'profile_build','uses'=>'ProfileBuildController@buildProfile'));

    Route::post('profile_update',array('as'=>'profile_update','uses'=>'ProfileUpdateController@updateProfile'));

    Route::get('profile_exp_add',array('as'=>'profile_exp_add','uses'=>'ProfileUpdateController@add_new_exp'));

    Route::get('profile_exp_remove',array('as'=>'profile_exp_remove','uses'=>'ProfileUpdateController@remove_exp'));

    Route::get('profile_init',array('as'=>'profile_init','uses'=>'ProfileUpdateController@ProfileInit'));
    
    Route::get('std_search',array('as'=>'std_search','uses'=>'StdSearchController@search'));

    Route::get('display_friends',array('as'=>'display_friends','uses'=>'addFriendController@showFriends'));

    Route::get('S_result_data',array('as'=>'S_result_data','uses'=>'StdSearchController@pop_result'));

    Route::get('add_friend',array('as'=>'add_friend','uses'=>'addFriendController@addFriend'));

    Route::get('change_btn',array('as'=>'change_btn','uses'=>'addFriendController@changeButton'));

    Route::get('remove_friend',array('as'=>'remove_friend','uses'=>'addFriendController@removeFriend'));


});

//End of Student Routes ----------------------------------------------------



/*
|--------------------------------------------------------------------------
| Alumni Routes
|--------------------------------------------------------------------------
*/

Route::group(array('before' => 'isAlumni'), function()
{

    Route::group(array('before' => 'profile_filled'), function() {
        Route::get('alumni/{page?}', array('as' => 'alm_dashboard', 'uses' => 'AlmDashboardController@initialize'));
    });

    Route::get('alm_search',array('as'=>'alm_search','uses'=>'AlmSearchController@search'));
    Route::get('alm_result_data',array('as'=>'alm_result_data','uses'=>'AlmSearchController@pop_result'));

    Route::post('alm_profile_build',array('as'=>'alm_profile_build','uses'=>'AlmProfileBuildController@buildProfile'));

    Route::post('alm_profile_update',array('as'=>'alm_profile_update','uses'=>'AlmProfileUpdateController@updateProfile'));

    Route::post('alm_post',array('as'=>'alm_post','uses'=>'AlmPostController@add'));

    Route::get('alm_post_init',array('as'=>'alm_post_init','uses'=>'AlmPostController@init'));

});

//End of Student Routes ----------------------------------------------------


/*
|--------------------------------------------------------------------------
| Industry Routes
|--------------------------------------------------------------------------
*/

Route::group(array('before' => 'isIndustry'), function()
{

    Route::group(array('before' => 'profile_filled'), function() {
        Route::get('industry/{page?}', array('as' => 'ind_dashboard', 'uses' => 'IndDashboardController@initialize'));
    });


    Route::get('ind_search',array('as'=>'ind_search','uses'=>'IndSearchController@search'));
    Route::get('I_result_data',array('as'=>'I_result_data','uses'=>'IndSearchController@pop_result'));

    Route::post('ind_job_post',array('as'=>'ind_job_post','uses'=>'IndJobPostController@add'));

    Route::get('ind_posted_jobs',array('as'=>'ind_posted_jobs','uses'=>'IndJobPostController@init'));

    Route::post('ind_profile_build',array('as'=>'ind_profile_build','uses'=>'IndProfileBuildController@buildProfile'));



});




//End of Industry Routes ----------------------------------------------------



/*
|--------------------------------------------------------------------------
| public Routes
|--------------------------------------------------------------------------
*/
Route::get('/',array('as' => 'index',function(){
    return View::make('public.index');
}));

Route::get('about',function(){
   return View::make('public.about');
});

Route::get('contact',function(){
    return View::make('public.contact');
});

Route::get('alum_ver',function(){
    return View::make('public.popup.alumni_verif');
});

Route::post('send_msg',array('as'=>'send_msg','uses'=>'messageController@storeMessage'));
//End of public Routes ----------------------------------------------------





/*
|--------------------------------------------------------------------------
| Testing Routes
|--------------------------------------------------------------------------
*/
Route::get('test',function(){

    /*$client = new \GuzzleHttp\Client(); // call validation api using GuzzleHttp (via cURL)
    $response = $client
        ->get('https://lms.mrt.ac.lk/login_index.php');
    $result = $response->getHeaders();

    echo $result;*/





// then we make the client
  $client = new Client();
    $client->setDefaultOption('verify', false);
    $req= $client->get('https://www.youtube.com/watch?v=nQY4dIxY1H4');
    //echo $req->getBody();


//    $html = <<<'HTML'
//<!DOCTYPE html>
//<html>
//    <body>
//        <p class="message">Hello World!</p>
//        <p>Hello Crawler!</p>
//        <div id="product">
//            <a data-type="bla">
//                <img src="OK">
//            </a>
//        </div>
//    </body>
//</html>
//HTML;

    $crawler = new Crawler($req->getBody()->getContents());

    $link = $crawler->filter('#player')->each(function (Crawler $node, $i) {
        echo $node->html().'</br>';
    });






});





//Settings: show form to create settings
Route::get( '/path/user/confirmationcode', function(){
    //return View::make('doo');
    ?>
    <script>
        var url      = window.location.href;
        var result = url.split('/');

        window.document.write(result.count.toString());
    </script>

    <?php
    //print_r(Config::get('view.paths'));
} );


