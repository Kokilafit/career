<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	//
});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the users of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest())
	{
		if (Request::ajax())
		{
			return Response::make('Unauthorized', 401);
		}
		else
		{
			return Redirect::guest('login');
		}
	}
});

Route::filter('isAdmin', function()
{
    if (Auth::guest() || Auth::user()->role !== "adm")
    {
        return Redirect::to('/');
    }
});


Route::filter('isStudent', function()
{
    if (Auth::guest() || Auth::user()->role !== "std")
    {
        return Redirect::to('/');
    }
});

Route::filter('isIndustry', function()
{
    if (Auth::guest() || Auth::user()->role !== "ind")
    {
        return Redirect::to('/');
    }
});

Route::filter('isAlumni', function()
{
    if (Auth::guest() || Auth::user()->role !== "alm")
    {
        return Redirect::to('/');
    }
});



Route::filter('profile_filled', function()
{
    if(Auth::user()->confirmation_token !== "" && Auth::user()->role == 'std'){
        return View::make('users.student.registration');
    }
    if(Auth::user()->confirmation_token !== "" && Auth::user()->role == 'ind'){
        return View::make('users.industry.registration');
    }
    if(Auth::user()->confirmation_token !== "" && Auth::user()->role == 'alm'){
        return View::make('users.alumni.registration');
    }
});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current users is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a users
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});

