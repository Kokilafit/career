<?php


class Industry extends Eloquent {
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    //protected $primaryKey  = 'users_id';\

	protected $table = 'industry';
    protected $primaryKey = 'user_id';
    public $timestamps = false;



    public function User(){
        return $this->belongsTo('User');
    }

}
