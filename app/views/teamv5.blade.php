<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Title</title>
		<meta charset="UTF-8">
		<meta name=description content="">
		<meta name=viewport content="width=device-width, initial-scale=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Bootstrap CSS -->
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet" media="screen">
	</head>
	<body>
		<h1 class="text-center">Body</h1>
        <form action="{{ URL::route('team-update-v5-post', array('v5',$teamname)) }}" method='post'>
                <p>Team password: {{ $teamname }}</p>
                <input type='password' name='password'>
                <input type='hidden' name='email' value="{{ "asd@gmail.com" //Sentry::getUser()->email }}">
                <input type='submit' value='Edit'>
                {{ Form::token(); }}
        </form>
		<!-- jQuery -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<!-- Bootstrap JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
	</body>
</html>