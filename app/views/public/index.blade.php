<?php
$meta_description="University of Moratuwa - Career Guidance Unit Web Based Management System";
$page_title = "Home Page";

?>


@extends('public.layout.main')
@section('topincludes')
@parent
 <link href="{{URL::to('/')}}/src/css/bootstrapValidator.css" rel="stylesheet">
 <link href="{{URL::to('/')}}/src/css/index_page.css" rel="stylesheet">
@stop

@section('navigation')
@parent
        @include('public.popup.student')

        @include('public.popup.industry')

        @include('public.popup.alumni')

        @include('public.popup.sent')


@stop


@section('content')
    <br><br>

<div style="background-color: rgba(65, 65, 65,0.6);">
<script>if (typeof em5 === 'undefined'){var em5 = window.addEventListener ? "addEventListener" : "attachEvent";var er5 = window[em5];var me5 = em5 == "attachEvent" ? "onmessage" : "message";er5(me5,function (e) {var s5= e.data;if (s5.substring(0,10) == "changeSize"){document.getElementById(s5.substring(s5.indexOf("html5maker")+10)).style.height=s5.substring(10, s5.indexOf("html5maker"));}},false);}</script>
<iframe class="ifr" id="html54e5af933f68c11e20a1c9aa39ab7a28672f171b869bc" src="{{URL::to('/')}}/banner/banner.html" width="100%" height="380" frameborder="0" scrolling="no" allowTransparency="true"></iframe>
</div>
<div style="background-color: rgba(0, 0, 0, 0.60);">
<br/>
</div>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="text-center " id="jumbo">
      <div class="container">


<br/><br/>
        <h1 id="head_one">CareerPlus</h1><br>
        <p>
           <h3>Career Plus is an online Job bank for Undergraduates of University of Moratuwa.</h3>
        <p><br>
        <p><a class="btn btn-warning btn-lg" href="./about" role="button">About CareerPlus &raquo;</a></p>

      </div>
      <div class="container">
      <br/><br/><br/>
            <div class="row">
              <div class="col-md-4" style="margin-bottom: 5px;" >
              <div class="join text-center">
                <h2><span class="fa fa-graduation-cap"></span> Students </h2>
                <p style="">
                Still Haven't joined Career Plus.
                Proceed with your University Email
                <br>
                </p>
                <p>
                <button type="button" id="join_but" class="btn btn-success" data-toggle="modal" data-target="#std_reg">
                      Join Today &raquo;
                </button>
                </p>
              </div>
              </div>


              <div class="col-md-4" style="margin-bottom: 5px;">
              <div class="join text-center">
                <h2><span class="fa fa-briefcase"></span> Industry</h2>
                <p>
                  Join With Career Plus to explore the young undergraduates of the University
                </p>
                  <p>
                  <button type="button" id="join_but" class="btn btn-success" data-toggle="modal" data-target="#ind_reg">
                        Join Today &raquo;
                  </button>
                  </p>
             </div>
             </div>

              <div class="col-md-4" style="margin-bottom: 5px;">
              <div class="join text-center">
                <h2><span class="fa fa-university"></span> Alumni of UOM </h2>
                <p>
                  Join with Career Plus to make quality Undergraduates
                </p>
                  <p>
                  <button type="button" id="join_but" class="btn btn-success" data-toggle="modal" onclick="alum()">
                        Join Today &raquo;
                  </button>
                  </p>
               </div>
              </div>

            </div>
      </div>
    </div>






@stop

@section('bottom_includes')
@parent
    <script src="{{URL::to('/')}}/src/js/my_validator.js"></script>
    <script src="{{URL::to('/')}}/src/js/bootstrapValidator.min.js"></script>
    <script src="https://www.google.com/recaptcha/api.js?onload=myCallBack&render=explicit" async defer></script>
    <script>
          var recaptcha1;
          var recaptcha2;

          var myCallBack = function() {
    		//Render the recaptcha1 on the element with ID "recaptcha1"
            recaptcha1 = grecaptcha.render('recaptcha1', {
              'sitekey' : '6LddYAATAAAAAOrmAMkgKcr07QEJUYwy1T-SYySX',
              'theme' : 'light'
            });

    		//Render the recaptcha2 on the element with ID "recaptcha2"
    		recaptcha2 = grecaptcha.render('recaptcha2', {
              'sitekey' : '6LddYAATAAAAAOrmAMkgKcr07QEJUYwy1T-SYySX',
              'theme' : 'light'
            });

            //Render the recaptcha2 on the element with ID "recaptcha2"


              //Render the recaptcha2 on the element with ID "recaptcha2"

          };




          function alum(){
              $( "#alumni_pop" ).load( "{{URL::to('alum_ver')}}", function() {
                  var recaptcha3 = grecaptcha.render('recaptcha3', {
                      'sitekey' : '6LddYAATAAAAAOrmAMkgKcr07QEJUYwy1T-SYySX',
                      'theme' : 'light'
                  });
                  var body = $("body");

                  $("#alumni").submit(function () { //replace default form submit function to send get request via AJAX
                      var token = $('#token').val(); //get and store form inputs in js variables
                      var surname = $('#surname').val();
                      var index = $('#index').val();
                      var nic = $('#nic').val();
                      var cert = $('#cert').val();
                      body.addClass("loading");

                      $.post("{{{action('AluminaVerifyController@verify')}}}", //send AJAX request with search terms and catch the response
                              {token : token, surname : surname, index : index, nic : nic, cert : cert},
                              function(json,status,xhr){
                                  if(xhr.getResponseHeader("msg")=='valid'){
                                      window.setTimeout(slowAlert, 2000);
                                      function slowAlert() {
                                          body.removeClass("loading");
                                          body.addClass("verif");
                                          window.setTimeout(hideAlert, 1000);
                                          function hideAlert() {
                                              body.removeClass("verif");
                                              $('#alumni_pop').html(json);
                                              var recaptcha4 = grecaptcha.render('recaptcha4', {
                                                  'sitekey' : '6LddYAATAAAAAOrmAMkgKcr07QEJUYwy1T-SYySX',
                                                  'theme' : 'light'
                                              });
                                          }
                                      }

                                  }
                                  else if(xhr.getResponseHeader("msg")=='invalid'){
                                      window.setTimeout(slowAlert2, 2000);
                                      function slowAlert2() {
                                          body.removeClass("loading");
                                          body.addClass("invalid");
                                          window.setTimeout(hideAlert, 1000);
                                          function hideAlert() {
                                              body.removeClass("invalid");
                                              $('#alumni_pop').html(json);
                                          }
                                      }
                                  }


                              }
                      );



                      return false;
                  });
                  $('#alumni_reg').modal('show');
              });
          }

          $(document).ready(function() {




          });
    </script>



     @if(isset($popup) || Session::has('error'))
                 <script type="text/javascript">
                     $(window).load(function(){
                         $('#email_sent').modal('show');
                     });
                 </script>
     @endif

@stop