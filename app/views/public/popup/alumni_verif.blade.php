

                    <h5 class="text-center">Please provide following details to Clarify</h5>



                    <div class="container-fluid">


                        <form class="form-horizontal" id="alumni">

                            <input type="hidden" id="token" name="token" value="{{ csrf_token(); }}">


                            <div class="form-group">
                                <label for="surname" class="col-sm-3 control-label">
                                    Surname:
                                </label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" value="Ranasinghe" id="surname" name="surname">
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="index" class="col-sm-3 control-label">
                                    You Index no:
                                </label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="index" value="094007H" name="index"   >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="nic" class="col-sm-3 control-label">
                                    NIC no:
                                </label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="nic" value="901391086V" name="nic"   >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="cert_no" class="col-sm-3 control-label">
                                    Certificate no:
                                </label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="cert" value="9998" name="cert"   >
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="password_confirmation" class="col-sm-3 control-label">

                                </label>
                                <div class="col-sm-9 text-center">
                                    <div id="recaptcha3"></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-9">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="agreement[]">
                                            I agree Website Terms and Conditions by University Of Moratuwa
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="row text-right">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>

                        </form>
                    </div>
                    <div class="ajaxloader"><!-- Place at bottom of page --></div>
