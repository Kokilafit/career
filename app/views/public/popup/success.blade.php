<?php
$meta_description="University of Moratuwa - Career Guidance Unit Web Based Management System";
$page_title = "Admin panel";
?>

@extends('public.layout.main')



@section('content')
<style type="text/css">
footer{
	width:100%;
	height:80px;
	position:absolute;
	bottom:0;
	left:0;
	background:#42210e;
}
.center{
display: block;
top: 0;
bottom: 0;
left: 0;
right: 0;
width: 100%;
height: 100%;
margin: auto;
position: absolute;
}
</style>
<div class="container">
<div style="min-height: 500px; background-color: #bcb6a2">
<input  class="btn btn-primary center" type="button" onclick="OpenInNewTab('http://webmail.mrt.ac.lk')" value="Confirmation email was sent to Your UOM Webmail">
</div>

</div>

<script type="text/javascript">
function OpenInNewTab(url) {
  var win = window.open(url, '_blank');
  win.focus();
}
</script>
@stop