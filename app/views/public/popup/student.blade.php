<div class="modal fade" id="std_reg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h3 class="page-header" id="myModalLabel">Student Sign Up</h3>
      <div class="modal-body">



       <div class="container-fluid">


                    <form class="form-horizontal" id="std_form" action="{{ action('StdSignUpController@postSignUp') }}" method="post">


                       <input type="hidden" name="user_role" value="std" />
                       <!--<input type="hidden" name="token" value="{{ csrf_token(); }}">-->


                       <div class="form-group">
                           <label for="username" class="col-sm-3 control-label">
                           Student Index No
                           </label>
                           <div class="col-sm-9">
                           <input type="text" class="form-control" value="" id="username" name="username">
                           </div>
                       </div>


                       <div class="form-group">
                           <label for="password" class="col-sm-3 control-label">
                           Password
                           </label>
                           <div class="col-sm-9">
                           <input type="password" class="form-control" id="password" value="" name="password"   >
                         </div>
                       </div>

                       <div class="form-group">
                           <label for="password_confirmation" class="col-sm-3 control-label">
                           Password Confirmation
                           </label>
                           <div class="col-sm-9">
                           <input type="password" class="form-control" id="password_confirmation" value="" name="password_confirmation"   >
                         </div>
                       </div>

                        <div class="form-group">
                              <label for="human" class="col-sm-3 control-label">

                              </label>
                              <div class="col-sm-9 text-center">
                                    <div id="recaptcha1"></div>
                              </div>
                        </div>

                       <div class="form-group">
                           <div class="col-sm-offset-3 col-sm-9">
                           <div class="checkbox">
                           <label>
                           <input type="checkbox" name="agreement[]">
                           I agree Website Terms and Conditions <br/>by University Of Moratuwa
                           </label>
                           </div>
                           </div>
                       </div>

                       <div class="row text-right">
                           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                           <button type="submit" class="btn btn-primary">Create Account</button>
                       </div>

       </form>
       </div>



      </div>
    </div>
  </div>
</div>
</div>



