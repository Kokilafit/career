@if(isset($title) || Session::has('error'))
<div class="modal fade" id="email_sent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="text-primary" id="myModalLabel">@if(Session::has('error'))Validation Failed !@else {{$title}} @endif</h4>
      <div class="modal-body">

        <script type="text/javascript">
        function OpenInNewTab(url) {
          var win = window.open(url, '_blank');
          win.focus();
        }
        </script>

       <div class="container-fluid">
                        <?php print_r(Session::get('error')); ?>
                       <div class="text-primary">@if(Session::has('error'))Your Sign up details are not Valid.@else {{$popup_msg}} @endif</div>
                       <div class="row text-right">
                           <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                           @if(isset($redirect))
                           <button type="button" onclick="OpenInNewTab('{{{$redirect}}}')" class="btn btn-success">{{$but_msg}}</button>
                           @endif
                       </div>
       </div>



      </div>
    </div>
  </div>
</div>
</div>
@endif