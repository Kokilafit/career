<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
         <!--prevent browser caching for developing purpose-->
         <meta http-equiv='cache-control' content='no-cache'>
         <meta http-equiv='expires' content='0'>
         <meta http-equiv='pragma' content='no-cache'>


        <link rel="shortcut icon" href="{{URL::to('/')}}/favicon.ico" type="image/vnd.microsoft.icon" />
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <title>{{$page_title}}</title>

        <meta name="description" content="{{$meta_description}}">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        @section('topincludes')
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
        <link href="{{asset('/src/css/main.css')}}" rel="stylesheet">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
        @show


    </head>
    <body>
    <div id="wrap">
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

@section('navigation')
<div id="top-bar" class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="{{URL::to('/')}}"><img class="navbar-brand" style="height: 35px;padding: 0;" src="{{URL::to('/')}}/img/logo.png"></a>
        </div>
        <div class="navbar-collapse collapse">

@if(!Auth::check())
          <form class="navbar-form navbar-right" action="{{{URL::route('signin')}}}" method="post" role="form">
            {{Form::token()}}
            <div class="form-group">
              <input type="text" placeholder="Index No | Email" name="username" class="form-control">
            </div>
            <div class="form-group">
              <input type="password" placeholder="Password" name="password" class="form-control">
            </div>
            <button type="submit" class="btn btn-success">Sign in</button>
              <button type="button" class="btn btn-warning" ><a style="text-decoration: none; color: #ffffff" href="../#head_one">Sign Up</a></button>
          </form>

@elseif(Auth::user()->role==='std')
<a style="margin-top: 5px" class="btn btn-success btn-md navbar-right" href="./student" role="button">Go To Dashboard &raquo;</a>
@elseif(Auth::user()->role==='ind')
<a style="margin-top: 5px" class="btn btn-success btn-md navbar-right" href="./industry" role="button">Go To Dashboard &raquo;</a>
@elseif(Auth::user()->role==='adm')
<a style="margin-top: 5px" class="btn btn-success btn-md navbar-right" href="./admin" role="button">Go To Dashboard &raquo;</a>
@elseif(Auth::user()->role==='alm')
<a style="margin-top: 5px" class="btn btn-success btn-md navbar-right" href="./alumni" role="button">Go To Dashboard &raquo;</a>
@endif

        </div><!--/.navbar-collapse -->

      </div>
    </div>
@show

@section('content')

@show


@section('footer')

<div id="push"></div>
</div>


<div id="footer">
<div class="container">
<br>
    <div class="col-md-6 text-left">
        <strong><a href="{{URL::to('/')}}/about">About</a> <a href="{{URL::to('/')}}/contact"> | Contact Us</a> <a href="http://mrt.ac.lk"> | Main Site</a></strong>
    </div>
    <div class="col-md-6 text-right">
            <p>Copyright &copy; 2014 University of Moratuwa All right reserved.</p>
    </div>
</div>
</div>
@show


@section('bottom_includes')

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script src="{{URL::to('/')}}/src/js/main.js"></script>

@show

        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X');ga('send','pageview');
        </script>

    </body>
</html>
