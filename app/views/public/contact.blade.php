<?php
$meta_description="University of Moratuwa - Career Guidance Unit Web Based Management System";
$page_title = "Contact Us";
?>


@extends('public.layout.main')
@section('topincludes')
@parent
<div id = 'contact' class="container">
    <div class="row">
        <div class="col-md-12" >
                <form class="form-horizontal"  id="con_box">
                    <fieldset>

                        <legend class="text-center header" id="header_c"><i class="fa fa-envelope bigicon" id="bigi_c"></i> Contact Us</legend>

                        <div class="text-center" id="header_c">J.A.K.S Jayasinghe <br />Director - Carrier Guidance Unit
                        </div>
                        <div class="header" id="header_c"> <i class="fa fa-phone-square bigicon" id="bigi_c"></i> Telephone
                         <div class="text-left" id="info_header">
                             0112564789 /
                             0773125486
                         </div>
                        </div>

                        <div class="header" id="header_c"> <i class="fa fa-envelope-square bigicon" id="bigi_c"></i> E mail
                            <div class="text-left" id="info_header">
                                jaks777@yahoo.com
                            </div>
                        </div>
                        <div class="text-center" id="header_c">
                            Carrier Guidance Unit
                        </div>
                        <div class="header" id="header_c"> <i class="fa fa-phone-square bigicon" id="bigi_c"></i> Telephone
                            <div class="text-left" id="info_header">
                                0112584589 /
                                0112578468
                            </div>
                        </div>

                        <div class="header" id="header_c"> <i class="fa fa-envelope-square bigicon" id="bigi_c"></i> E mail
                            <div class="text-left" id="info_header">
                                cgu@mrt.ac.lk
                            </div>
                        </div>
                        <br />
                        <div class="modal-header text-left" id="header_c">
                            Message Us
                        </div>
                        <br />
                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon" id="bigi_c"></i></span>
                            <div class="col-md-8">
                                <input id="fname" name="name" type="text" placeholder="Name" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-info bigicon" id="bigi_c"></i></span>
                            <div class="col-md-8">
                                <input id="lname" name="name" type="text" placeholder="Index No" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-building bigicon" id="bigi_c"></i></span>
                            <div class="col-md-8">
                                <input id="lname" name="name" type="text" placeholder="Faculty" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-envelope-o bigicon" id="bigi_c"></i></span>
                            <div class="col-md-8">
                                <input id="email" name="email" type="text" placeholder="Email Address" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-pencil-square-o bigicon" id="bigi_c"></i></span>
                            <div class="col-md-8">
                                <textarea class="form-control" id="message" name="message" placeholder="Enter your massage for us here. " rows="7"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-success btn-lg">Submit</button>
                            </div>
                        </div>
                    </fieldset>
                </form>
        </div>
    </div>
</div>


