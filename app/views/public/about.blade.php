<?php
$meta_description="University of Moratuwa - Career Guidance Unit Web Based Management System";
$page_title = "About Us";
?>


@extends('public.layout.main')
@section('topincludes')
@parent
@stop

@section('navigation')
@parent
@stop

@section('content')
<!-- Main jumbotron for a primary marketing message or call to action -->
<div class="jumbotron" id="jumbo">
    <div class="container">
        <br>

        <h2 id="head_one"><i class="fa fa-info-circle"></i> About CareerPlus</h2><br><br>

        <p>
            Career Plus is an online Job bank for Undergraduates of University of Moratuwa.For the first time in Sri Lankan Unversity history, University of Moratuwa presents 'CareerPlus',
            an on-line job bank exclusively for the students of University of Moratuwa, Sri Lanka. CareerPlus is a result of a dream of the Career Guidance Unit of University of Moratuwa,
            to offer students and corporate entities an opprtunity to maintain a mutually benefitial relationship on a common web platform.
        </p><br/>
        <div id="dir_head">
        <h2 id="head_one"><i class="fa fa-info-circle"></i> Message from the Director,<br/>Career Guidance Unit
          <img class="img-thumbnail" src="{{URL::to('/')}}/contact/Nama_2010.JPG" id="dir"/></h2> </div>
        <br/><br/><br/><br/><br/><br/><br/>
        <div id="dir_body">
        <p>
            The University of Moratuwa has been the preferred choice of students opting for higher
            studies in the discipline of technology education and the main reason has been the proven
            track record of the high employability of its graduates. This has led to a 'branded' image of its
            graduates in the minds of the public of the country. To retain this coveted leading position,
            University of Moratuwa has consistently strived to upgrade the attributes of its graduates in
            order to surpass the expectations of the Industry.<br/>
            The objective of the Career Guidance Unit (CGU) is to assist the undergraduates in pursuing
            courses which can guide them in choosing a suitable career and reach standards matching the
            potential employer's requirements. To this end, the CGU collaborates with the Academic
            Departments, Student Societies and Other Units (such as Student Welfare, Student
            Counseling, Industrial Training, etc.) within the university in their activities and initiatives.
            The CGU provides its services to the Faculties of Engineering, Architecture & Information
            Technology of the University. It has also provided potential employers with support services
            to recruit undergraduates for employment.<br/>
            The present goals and objectives of the University of Moratuwa are focused on our
            positioning in global ranking to raise the level of student attributes in order to produce globally
            competitive and world class graduates. Obviously it is a challenge to sustain the quality of
            graduates given the increased intake of student numbers.<br/>
            CGU usually conducts a number of short duration programs on various aspects of career
            throughout the year. These are aimed at increasing the aspect of soft skills in e.g. personality
            development, leadership qualities, communication and presentation skills etc. We also provide
            students opportunities for pragmatic training e.g. writing an effective CV, facing interviews
            etc to help in building self confidence and in preparation to undertake progressive steps
            towards entering the corporate sector.<br/>
            Career Guidance activities are steered under the guidance of Career Guidance Advisory Board
            (CGAB). <br/></div>
            <div id="dir_foot"><p> Dr. Satish Namasivayam<br/>Acting Director/Career Guidance</p></div>
        </p><br/>


    </div>
</div>


@stop