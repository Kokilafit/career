<?php
$meta_description="University of Moratuwa - Career Guidance Unit Web Based Management System";
$page_title = "Registration";
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <!--prevent browser caching for developing purpose-->
    <meta http-equiv='cache-control' content='no-cache'>
    <meta http-equiv='expires' content='0'>
    <meta http-equiv='pragma' content='no-cache'>

    <link rel="shortcut icon" href="{{URL::to('/')}}/favicon.ico" type="image/vnd.microsoft.icon" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>{{$page_title}}</title>

    <meta name="description" content="{{$meta_description}}">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    @section('topincludes')
    <link href="{{URL::to('/')}}/src/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{URL::to('/')}}/src/css/stdregistration.css" rel="stylesheet">
    <link rel="stylesheet" href="{{URL::to('/')}}/src/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="{{URL::to('/')}}/src/css/main.css">
    <script src="{{URL::to('/')}}/src/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <link href="{{URL::to('/')}}/src/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    @show


</head>
<body>

    <!--[if lt IE 7]>
    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->


@section('content')
<div>

</div>
{{ Form::open(array('url'=>action('ProfileBuildController@buildProfile'),'method'=>'POST', 'files'=>true,'id'=>'msform')) }}

        <!-- progressbar -->
        <ul id="progressbar">
            <li class="active">Hello</li>
            <li>Basic Details</li>
            <li>Academic Details</li>
            <li>Make Your Profile</li>
            <li>Thank You</li>
        </ul>
        <!-- fieldsets -->
        <fieldset>
            <h1 class="fs-title">Hi There</h1>
            <h3 class="fs-subtitle">This is Career Plus</h3>
            <h3 class="fs-subtitle">You are about to register into Career Plus</h3>
            <h3 class="fs-subtitle">Browse through and find great opportunities and stand tall among others </h3>
            <h3 class="fs-subtitle">Good Luck </h3>
            <h3 class="fs-subtitle">Click Next to proceed </h3>
            <h3 class="fs-subtitle">Read <a>Privacy policy</a> </h3>
            <input type="button" id="reg_btn" name="next" class="next" />
        </fieldset>
        <fieldset>
            <h2 class="fs-title">Basic Details</h2>
            <h3 class="fs-subtitle">Collecting basic information</h3>
            <div class="fs-input">
            <p><label>Name : </label><input type="text" id="name" name="name" placeholder="Your Name" /></p>
            <p><label>Date of birth : </label><input id="date" type="date" name="bday" /></p>
            <p><label>Personal Email : </label><input id="email" type="email" name="mail" placeholder="yourmail@mrt.ac.lk"/></p>
            <p><label>Linkedin url : </label><input type="text" name="linkedin" placeholder="Linkedin" /></p>
            <p><label>google+ url : </label><input type="text" name="gplus" placeholder="google plus" /></p>

            </div>
            <input type="button" id="2-1" name="previous" class="previous" />
            <input type="button" id="2-3" name="next" class="next" />
        </fieldset>
        <fieldset>
            <h2 class="fs-title">Academic Details</h2>
            <h3 class="fs-subtitle">Collecting academic information</h3>
            <div class="fs-input">
                <p><label>Year of entrance :
                    </label>
                    <select name="yoe">
                        <?php for($i = date("Y"); $i >= 1900; --$i)
                            printf('<option value="%d">%d</option>', $i, $i);
                        ?>
                    </select>
                </p>
                <br />
                <p><label>Faculty :</label>
                        <select id="fac" name="fac">
                            <option selected disabled hidden value=""></option>
                            <option value="Efac">Faculty of Engineering</option>
                            <option value="ITfac">Faculty of Information Technology</option>
                            <option value="Afac">Faculty of Architecture</option>
                        </select>
                </p>
                <br />
                <p><label>Department :</label>
                    <select id="Afac" class="dep_select" name="dep">
                        <option selected disabled hidden value=""></option>
                        <option value="DArc">Department of Architecture</option>
                        <option value="DID"> Department of Integrated Design</option>
                        <option value="DBE">Department of Building Economics</option>
                        <option value="DTCP">Department of Town & Country Planning</option>
                    </select>
                    <select id="Efac" class="dep_select" name="dep">
                        <option selected disabled hidden value=""></option>
                        <option value="DCPE">Department of Chemical & Process Engineering</option>
                        <option value="DCE">Civil Engineering</option>
                        <option value="DCSE">Department of computer Science & Engineering</option>
                        <option value="DER">Earth Resource Engineering</option>
                        <option value="DTEE">Electrical Engineering</option>
                        <option value="DENTC">Electronic & Telecommunication Engineering</option>
                        <option value="DMSE">Material Science & Engineering</option>
                        <option value="DMech">Mechanical Engineering</option>
                        <option value="DCT">Textile & Clothing Technology</option>
                        <option value="DTLM">Transport & Logistics Management</option>
                    </select>
                <br />
            </div>
            <input type="button" id="reg_btn" name="previous" class="previous" />
            <input type="button" id="reg_btn" name="next" class="next" />
        </fieldset>
        <fieldset>
            <h2 class="fs-title">Profile details</h2>
            <h3 class="fs-subtitle">Setting up profile</h3><br />
            <div>
               <!--<div id="img-upload">
                <label for="files" id="overlay">
                    <input type="file" id="files" name="profile_pic" onchange="loadFile(event)" class="hidden"/>
                    <span class="fa fa-camera fa-3x"></span><br/>
                    Select Image to Upload as Your Profile Picture
                    </label>
                </div>-->


                <div class="image-editor">

                        <input type="file" id="files" class="cropit-image-input hidden">

                        <label for="files" id="overlay">
                        <span class="fa fa-camera fa-2x"></span><br/>
                        Browse Profile Picture
                        </label>
                        <div class="cropit-image-preview" style="background:url('{{URL::to('/').Auth::user()->profile_pic}}') no-repeat center">
                        </div>
                        <input type="range" class="cropit-image-zoom-input">


                        <input type="hidden" name="image-data" class="hidden-image-data" />
                </div>


                <script>
                  var loadFile = function(event) {
                    var reader = new FileReader();
                    reader.onload = function(){
                      var output = document.getElementById('img-upload');
                      output.style.backgroundImage = "url("+reader.result+")" ;
                    };
                    reader.readAsDataURL(event.target.files[0]);
                  };
                </script>

            </div><br />

             <input type="button" id="reg_btn" name="previous" class="previous"/>
             <input type="button" id="reg_btn1" name="next" class="next"/>

        </fieldset>
        <fieldset>
            <h2 class="fs-title">Thank You</h2>
            <h3 class="fs-subtitle">You are almost done</h3>
            <h3 class="fs-subtitle">You can add and customize your Profile</h3>
            <input type="submit" name="next" class="btn btn-success" value="Save and exit" style="font-size: medium"/>

            <input type="button" id="reg_btn" name="previous" class="previous"/>

        </fieldset>
    </form>
    @show

@section('footer')
<!--<div id="push"></div>
</div>


<div id="footer">
<div class="container">
<br>
    <div class="col-md-6 text-left">
        <strong><a href="{{URL::to('/')}}/about">About</a> <a href="{{URL::to('/')}}/contact"> | Contact Us</a> <a href="http://mrt.ac.lk"> | Main Site</a></strong>
    </div>
    <div class="col-md-6 text-right">
            <p>Copyright &copy; 2014 University of Moratuwa All right reserved.</p>
    </div>
</div>
</div>-->
@show

@section('bottom_includes')
<script src="{{URL::to('/')}}/src/js/jquery.min.js"></script>
<script src="{{URL::to('/')}}/src/js/jquery.easing.min.js"></script>
<script src="{{URL::to('/')}}/src/js/jquery.cropit.min.js"></script>
<script>window.jQuery || document.write('<script src="{{URL::to('/')}}/src/js/jquery.min.js"><\/script>')</script>
<script src="{{URL::to('/')}}/src/js/bootstrap.min.js"></script>
<script src="{{URL::to('/')}}/src/js/registration.js"></script>
<script>
      $(function() {
        $('.image-editor').cropit();

        $('#reg_btn1').click(function() {
          // Move cropped image data to hidden input
          var imageData = $('.image-editor').cropit('export');
          $('.hidden-image-data').val(imageData);

        });
      });
    </script>
@show

<script>
    (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
        function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
    ga('create','UA-XXXXX-X');ga('send','pageview');
</script>

</body>
</html>
