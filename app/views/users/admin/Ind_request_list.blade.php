@include('users.admin.popup.ind_request_popup')
@section('css')
    @parent


    <style>
        .pagination{
            position: relative;
            left: 50%;
            transform: translateX(-50%);
        }
        tr:hover .profile_thumb{
            z-index: 10;
            -webkit-transform:scale(1.25); /* Safari and Chrome */
            -moz-transform:scale(1.25); /* Firefox */
            -ms-transform:scale(1.25); /* IE 9 */
            -o-transform:scale(1.25); /* Opera */
            transform:scale(1.25);
            border-radius: 5px;
        }
        tr:hover{
            background-color: rgba(0, 141, 231, 0.24);
        }
        tr{
            font-size: medium;
        }
        .panel-heading{
            color: #008DE7 !important;
            background-color: #303641 !important;
        }
        .panel-heading:hover{
            background-color: #282b49 !important;
        }
        .panel-body {
            background-color: #B0AFAF;
        }

    </style>
    @stop

            <!--@include('users.admin.popup.ind_request_popup')-->
    <div class="container-fluid">
        <div class="row-fluid">
            <br/>
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="SearchOptions" data-toggle="collapse"  data-parent="#accordion"  data-target="#collapseOne">
                        <h4 class="panel-title">
                            <a data-toggle="collapse"  data-parent="#accordion"  href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                <i class="fa fa-search-plus"></i> Company Requests :
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="SearchOptions">
                        <div class="panel-body">
                            <div id="results">

                                <table id="myTable" class="table table-responsive" width="100%">




                                    <tbody >

                                    @if(isset($posts))
                                        @foreach($posts as $post)
                                            <tr id="Search_row" data-toggle="modal" data-target=".popup">
                                                <td>{{$post->id}}</td>
                                                <td><img class="profile_thumb" width="50px" src="{{URL::to('/')}}{{$post->profile_pic}}?imgchash={{time()}}" width="100px"/></td>
                                                <td>{{ $post->username }}</td>
                                                <td>{{ $post->p_email }}</td>
                                                <td>
                                                    <button id="view" class="btn btn-primary">View</button>
                                                    <button id="accept" class="btn btn-success"><i class="fa fa-check"></i></button>
                                                    <button id="reject" class="btn btn-danger"><i class="fa fa-remove"></i></button>
                                                </td>

                                            </tr>
                                        @endforeach
                                    @endif


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>



    {{-- $results->links() --}}
@section('js_lib')
    @parent

    <script type="text/javascript">

        $(document).ready(function($){

            $("#view").click(function(){

                $('#popup_ind1').modal('show');

                $uid = $(this).parent().parent().find('td:first-child').text();

                $.get("{{{action('AdmAcceptIndController@showDetail')}}}", {uid : $uid},function(user_data){

                            $('#udata').html(user_data);
                        }
                );

            })
        });

        $(document).ready(function($){

            $("#accept").click(function(){
                $uid = $(this).parent().parent().find('td:first-child').text();

                $.get("{{{action('AdmAcceptIndController@accept')}}}", {uid : $uid},function(user_data){
                            $('#udata1').html(user_data);
                        }
                );
                alert("User accepted");

            })
        });

        $(document).ready(function($){

            $("#reject").click(function(){
                $uid = $(this).parent().parent().find('td:first-child').text();

                $.get("{{{action('AdmAcceptIndController@reject')}}}", {uid : $uid},function(user_data){
                            $('#udata1').html(user_data);
                        }
                );
                alert("User rejected");

            })
        });


    </script>
@stop