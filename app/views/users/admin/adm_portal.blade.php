@section('css')
    @parent
    {{HTML::style('/src/css/alm_css.css')}}
@stop

    <div class="row">

        <div class="timeline-centered">

            <script type="text/javascript">
                function toggleDiv(divId) {
                    $("#"+divId).slideToggle();
                }


            </script>

            <article class="timeline-entry">

                <div class="timeline-entry-inner">

                    <div onclick="toggleDiv('post_form')" class="timeline-icon" style="-webkit-transform: rotate(-90deg); -moz-transform: rotate(-90deg);">
                        <i class="entypo-flight"></i> +
                    </div>


                    <div hidden id="post_form" class="timeline-label">
                        <form class="form-horizontal" enctype="multipart/form-data" role="form" method="post" action="{{action('AdmPostController@add')}}">
                            <div class="form-group">
                                <label for="name" class="col-sm-2 control-label">Title</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="title" name="title" placeholder="add title for the post" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="message" class="col-sm-2 control-label">Description</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" rows="4" name="description"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email" class="col-sm-2 control-label">Image</label>
                                {{--<div class="col-sm-8">
                                    <input type="email" class="form-control" id="email" name="email" placeholder="example@domain.com" value="">
                                </div>--}}
                                <div class="col-sm-8">
                                <span class="btn btn-default btn-file" style="width: 100%">
                                    Select image for post <input type="file" name="img">
                                </span>
                                </div>
                                <div class="col-sm-2">
                                    <button type="submit" style="width: 100%" class="btn btn-default btn-md">Submit</button>
                                </div>
                            </div>

                            </form>

                    </div>
                </div>

            </article>

            <br>


@if(isset($posts))

            @foreach($posts as $post)
                <article class="timeline-entry">

                    <div class="timeline-entry-inner">

                        <div class="timeline-icon bg-secondary">
                            <i class="entypo-feather"></i>
                        </div>

                        <div class="timeline-label">
                            <h2><a href="#">{{{$post->title}}}</a> <span> @ {{{$post->posted_time}}}</span></h2>
                            <p>{{{$post->desc}}}</p>
                            @if(isset($post->img))
                                <img class="img-responsive" src="{{URL::to('/')}}/post_img/{{{$post->img}}}">
                            @endif
                        </div>
                    </div>

                </article>
            @endforeach


@endif
            <article class="timeline-entry">

                <div class="timeline-entry-inner">

                    <div class="timeline-icon bg-secondary">
                        <i class="entypo-feather"></i>
                    </div>


                </div>

            </article>



        </div>


    </div>
