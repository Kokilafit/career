<?php
$meta_description="University of Moratuwa - Career Guidance Unit Web Based Management System";
$page_title = "Admin panel";
?>

@extends('users.layout.main')
@section('css')
    @parent
    <link href="{{URL::to('/')}}/src/css/plugins/timeline.css" rel="stylesheet">
    <link href="{{URL::to('/')}}/src/css/admin_panel.css" rel="stylesheet">
    <link href="{{URL::to('/')}}/src/css/profile.css" rel="stylesheet" type="text/css">
@stop

@section('content')
    @include('users.admin.topbar')
    <div class="page-container">
        @include('users.admin.sidebar')

        <div id="page-wrapper">
            @if($page == 'home')
                @include('users.admin.home')
            @elseif($page=='profile')
                @include('users.admin.profile')
            @elseif($page=='friends')
                @include('users.admin.friends')
            @elseif($page=='adv_search')
                @include('users.admin.adv_search')
            @elseif($page=='adm_portal')
                @include('users.admin.adm_portal')
            @elseif($page=='ind_request_list')
                @include('users.admin.Ind_request_list')
            @endif
        </div>
    </div>

@stop

@section('footer')

@stop

@section('js_lib')
    @parent

@stop

