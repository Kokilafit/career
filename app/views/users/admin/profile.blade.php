
<div class="container-fluid">
<div class="row info" style="text-align:center;">

</div>

<div class="resume">
    <header class="page-header">
    <button type="button" id="editbut" class="bg-primary btn-sm" style="position: absolute; left:5%;">Toogle</button>
    <h3 class="page-title">CareerPlus My Profile</h3>
    <small> <i class="fa fa-clock-o"></i> Last Updated on: <time>Sunday, October 05, 2014</time></small>

  </header>


    <div class="row">

  <div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-1 col-lg-10">

    <div id="profile_edit" class="panel panel-default">
      <div class="panel-heading resume-heading">

        <div class="row">
          <div class="col-lg-12">
            <div class="col-xs-12 col-sm-12">

<table style="margin-top:3% ">
		<tr>
			<td  style="width: 27%;">
			<figure>
                 <img width="100%" class="img-rounded img-responsive"  alt="profile picture not loaded" src="{{Auth::user()->profile_pic}}">
            </figure>
			</td>
			<td  style="width: 73%; padding-left: 5% ">
                <li class="list-group-item"><i class="fa fa-chevron-circle-right"></i> Name :
                <a href="#" id="name" data-type="text" data-placement="right" data-title="Enter your name"> {{{$name}}}</a>
                </li>
                <li class="list-group-item"><i class="fa fa-chevron-circle-right"></i> Faculty :
                <a href="#" id="faculty" data-type="select" data-placement="right" data-title="Enter your name"> {{{$contact_email}}}</a>
                </li>
                <li class="list-group-item"><i class="fa fa-chevron-circle-right"></i> Department :
                <a href="#" id="department" data-type="text" data-placement="right" data-title="Enter your name"> {{{$telephone}}}</a>
                </li>
                <li class="list-group-item"><i class="fa fa-chevron-circle-right"></i> <a href="#" id="status" data-type="select" data-placement="right" data-title="Enter username"> +94716330371</a> </li>
                <li class="list-group-item"><i class="fa fa-chevron-circle-right"></i><a href="#" id="pno" data-type="text" data-placement="right" data-title="Enter username"> +94716330371</a> </li>
			</td>
		</tr>
</table>



            </div>
              <div class="row pull-right">

                  <p><a class="btn btn" href="cvbtn" role="button"><span class="glyphicon glyphicon-upload"></span>
                      Upload CV here</a></p>
              </div>
          </div>

        </div>


      </div>
      <div class="bs-callout bs-callout-danger">
        <h4>Summary</h4>
        <p>
            I am Currently a second year undergraduate of Faculty of Information Technology, University of Moratuwa.
            I have completed BCS Higher Educational Qualification.
            I am interested in New Tech, Movies, Music and anything related to computer.
        </p>
        <p>


        </p>
      </div>
      <div class="bs-callout bs-callout-danger">
        <h4>Personal Details</h4>
        <p>Birthday	August 12</p>
          <p>  Marital Status	Single

        </p>

      </div>

      <div class="bs-callout bs-callout-danger">
        <h4>Experiences</h4>
        <ul class="list-group">
          <a class="list-group-item inactive-link" href="#">
            <h4 class="list-group-item-heading">
              Software Engineer at Twitter
            </h4>
            <p class="list-group-item-text">
              Lorem ipsum dolor sit amet, ea vel prima adhuc, scripta liberavisse ea quo, te vel vidit mollis complectitur. Quis verear mel ne. Munere vituperata vis cu,
         te pri duis timeam scaevola, nam postea diceret ne. Cum ex quod aliquip mediocritatem, mei habemus persecuti mediocritatem ei.
            </p>
          </a>
          <a class="list-group-item inactive-link" href="#">
            <h4 class="list-group-item-heading">Software Engineer at LinkedIn</h4>
            <p class="list-group-item-text">
              Lorem ipsum dolor sit amet, ea vel prima adhuc, scripta liberavisse ea quo, te vel vidit mollis complectitur.
              Quis verear mel ne. Munere vituperata vis cu, te pri duis timeam scaevola,
              nam postea diceret ne. Cum ex quod aliquip mediocritatem, mei habemus persecuti mediocritatem ei.
                </p><ul>
                  <li>
                 Lorem ipsum dolor sit amet, ea vel prima adhuc, scripta liberavisse ea quo, te vel vidit mollis complectitur.
              Quis verear mel ne. Munere vituperata vis cu, te pri duis timeam scaevola,
              nam postea diceret ne. Cum ex quod aliquip mediocritatem, mei habemus persecuti mediocritatem ei.
                  </li>
                  <li>
                 Lorem ipsum dolor sit amet, ea vel prima adhuc, scripta liberavisse ea quo, te vel vidit mollis complectitur.
              Quis verear mel ne. Munere vituperata vis cu, te pri duis timeam scaevola,
              nam postea diceret ne. Cum ex quod aliquip mediocritatem, mei habemus persecuti mediocritatem ei.
                  </li>
                </ul>
            <p></p>
          </a>
        </ul>
      </div>
      <div class="bs-callout bs-callout-danger">
        <h4>Projects</h4>
        <ul class="list-group">
          <li class="list-group-item"> Lorem ipsum dolor sit amet, ea vel prima adhuc </li>
          <li class="list-group-item"> Lorem ipsum dolor sit amet, ea vel prima adhuc</li>
          <li class="list-group-item"> Lorem ipsum dolor sit amet, ea vel prima adhuc</li>
          <li class="list-group-item"> Lorem ipsum dolor sit amet, ea vel prima adhuc</li>
          <li class="list-group-item">Lorem ipsum dolor sit amet, ea vel prima adhuc</li>
          <li class="list-group-item"> Lorem ipsum dolor sit amet, ea vel prima adhuc</li>



        </ul>
      </div>
      <div class="bs-callout bs-callout-danger">
        <h4>Language and Platform Skills</h4>
        <ul class="list-group">
          <a class="list-group-item inactive-link" href="#">


            <div class="progress">
              <div data-placement="top" style="width: 80%;"
              aria-valuemax="100" aria-valuemin="0" aria-valuenow="80" role="progressbar" class="progress-bar progress-bar-success">
                <span class="sr-only">80%</span>
                <span class="progress-type">Java/ JavaEE/ Spring Framework </span>
              </div>
            </div>
            <div class="progress">
              <div data-placement="top" style="width: 70%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="1" role="progressbar" class="progress-bar progress-bar-success">
                <span class="sr-only">70%</span>
                <span class="progress-type">PHP/ Lamp Stack</span>
              </div>
            </div>
            <div class="progress">
              <div data-placement="top" style="width: 70%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="1" role="progressbar" class="progress-bar progress-bar-success">
                <span class="sr-only">70%</span>
                <span class="progress-type">JavaScript/ NodeJS/ MEAN stack </span>
              </div>
            </div>
            <div class="progress">
              <div data-placement="top" style="width: 65%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="1" role="progressbar" class="progress-bar progress-bar-warning">
                <span class="sr-only">65%</span>
                <span class="progress-type">Python/ Numpy/ Scipy</span>
              </div>
            </div>
            <div class="progress">
              <div data-placement="top" style="width: 60%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="60" role="progressbar" class="progress-bar progress-bar-warning">
                <span class="sr-only">60%</span>
                <span class="progress-type">C</span>
              </div>
            </div>
            <div class="progress">
              <div data-placement="top" style="width: 50%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="50" role="progressbar" class="progress-bar progress-bar-warning">
                <span class="sr-only">50%</span>
                <span class="progress-type">C++</span>
              </div>
            </div>
            <div class="progress">
              <div data-placement="top" style="width: 10%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="50" role="progressbar" class="progress-bar progress-bar-danger">
                <span class="sr-only">10%</span>
                <span class="progress-type">Go</span>
              </div>
            </div>

            <div class="progress-meter">
              <div style="width: 25%;" class="meter meter-left"><span class="meter-text">Poor</span></div>
              <div style="width: 25%;" class="meter meter-left"><span class="meter-text">Weak</span></div>
              <div style="width: 30%;" class="meter meter-right"><span class="meter-text">Better</span></div>
              <div style="width: 20%;" class="meter meter-right"><span class="meter-text">Good</span></div>
            </div>

          </a>

        </ul>
      </div>
      <div class="bs-callout bs-callout-danger">
        <h4>Education</h4>
        <table class="table table-striped table-responsive ">
          <thead>
            <tr><th>Degree</th>
            <th>Graduation Year</th>
            <th>GPA</th>
          </tr></thead>
          <tbody>
            <tr>
              <td>Masters in Computer Science and Engineering</td>
              <td>2014</td>
              <td> 3.50 </td>
            </tr>
            <tr>
              <td>BSc. in Computer Science and Engineering</td>
              <td>2011</td>
              <td> 3.25 </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>

  </div>
</div>

</div>

</div>

