<style>
    #input_text{
        margin-left: 30px;
    }
    #msg_body{
        height: 200px;
        width: 80%;
        margin: 40px;
    }
</style>
<div class="modal" id="msg_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h2>Private Massage</h2>
            </div>
            <div class="modal-body">
               <h4> <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> Message :</h4> <div id="input_text"><textarea id="msg_body" ></textarea></div>
            </div>

            <div class="modal-footer" id="buttons">
                <button type="submit" class="btn btn-primary" id="send">Send</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('#send').click(function() {
            var masg = $('#msg_body').val();
           $.post("{{{action('messageController@storeMessage')}}}", {uid : $uid , msg : masg });
                    $('#msg_modal').modal('toggle');
                    alert("Massage Sent Successfully");
        })
    });

</script>
