@include('users.student.popup.mesage_model')
<style>
    .modal .modal-dialog {
        width: 60%;
    }
</style>
<script>
    $(document).ready(function(){
        $('#follow').click(function() {
            $.get("{{{action('addFriendController@addFriend')}}}", {uid : $uid});
            $('#popup').modal('toggle');
            alert("Friend Request Sent");
        })
    });
    $(document).ready(function(){
        $('#remove,#request').click(function() {
            $.get("{{{action('addFriendController@removeFriend')}}}", {uid : $uid});
            $('#popup').modal('toggle');
            alert("Removed Friend");

        })
    });

  $(document).ready(function($){
        $("#pvt_msg").click(function(){
            $('#popup').modal('toggle');
            $('#msg_modal').modal('show');

        })
    });
</script>



<div class="modal" id="popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">@if(isset($asd->name)){{ $asd->name }}@endif</h4>
            </div>
            <div class="modal-body" id="udata">

            </div>
            <div class="modal-footer" id="buttons">
                <button type="button" class="btn btn-primary">Visit Profile</button>
                <button type="button" class="btn btn-primary" id="follow" value="follow">Add Friend</button>
                <button type="button" class="btn btn-primary" id="request" value="follow">Cancel Request</button>
                <button type="button" class="btn btn-primary" id="remove" value="remove">Remove Friend</button>
                <button type="button" class="btn btn-primary" id="pvt_msg">Private Massage</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

