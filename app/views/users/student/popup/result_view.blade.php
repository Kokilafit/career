<style>
    .prof_pic{
        height:200px;
        margin-right: 100px;
        width: 200px;
        float: right;
    }
    .head{
        margin-left: 20px;
    }
    .sub{
        margin-left: 80px;
    }
</style>



<div >
  <img class=".img-rounded prof_pic" src="{{URL::to('/')}}{{$stddata->profile_pic}}?imgchash={{time()}}" />
    <h3><p class="head">Details</p></h3>
    <p class="sub">Name : @if(isset($stddata)){{$stddata->name;}}@endif</p>
    <p class="sub">Faculty : @if(isset($stddata)){{$stddata->faculty;}}@endif</p>
    <p class="sub">Department : @if(isset($stddata)){{$stddata->department;}}@endif</p>
    <p class="sub">Overall GPA : @if(isset($stddata)){{$stddata->ogpa;}}@endif</p>
    <h4 class="head">Contact Details</h4>
    <p class="sub">Email : @if(isset($stddata)){{$stddata->n_email;}}@endif</p>
    <p class="sub">LinkedIn : @if(isset($stddata)){{$stddata->Linkedin;}}@endif</p>

</div>

