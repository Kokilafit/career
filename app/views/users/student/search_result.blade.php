@include('users.student.popup.popup_Model')
<table id="myTable" class="table table-responsive" width="100%">


    <thead>
    <tr>
        <th>#</th>
        <th>Name</th>
        <th>Fac</th>
    </tr>
    </thead>
    <tbody>

    @foreach($results as $result)
        <tr id="Search_row" data-toggle="modal" data-target=".popup">
            <td>{{$result->user_id}}</td>
            <td><img class="profile_thumb" width="50px" src="{{URL::to('/')}}{{$result->profile_pic}}?imgchash={{time()}}" width="100px"/></td>
            <td>{{ $result->name }}</td>
            <td>{{ $result->faculty }}</td>
        </tr>
    @endforeach
    </tbody>
</table>

{{ $results->links() }}
<script type="text/javascript">


    $(document).ready(function($){
        $("tr#Search_row").click(function(){

            $('#popup').modal('show');


            $uid = $(this).find('td:first-child').text();

            $.get("{{{action('StdSearchController@pop_result')}}}", {uid : $uid},function(user_data,status,xhr){

                        if(xhr.getResponseHeader('isFriend') == '1'){

                            $('#follow').hide();
                            $('#remove').show();
                            $('#request').hide();

                        }else if(xhr.getResponseHeader('isFriend') == '0'){
                            $('#follow').hide();
                            $('#remove').hide();
                            $('#request').show();
                        }else{
                            $('#follow').show();
                            $('#remove').hide();
                            $('#request').hide();
                        };
                        $('#udata').html(user_data);
                    }
            );
        })
    });
</script>
<style>
    #Search_row{
        cursor: hand;
    }
</style>