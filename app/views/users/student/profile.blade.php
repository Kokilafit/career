
@section('css')
    @parent
    <style>
        .exp_remove{
            visibility: hidden;
            float: right;
        }
        #exp_list>a:hover .exp_remove{
            visibility: visible;
        }
    </style>
@stop




<div class="container-fluid">
<div class="row info" style="text-align:center;">

</div>

<div class="resume">
    <header class="page-header">
    <button type="button" id="editbut" class="bg-primary btn-sm" style="position: absolute; left:5%;">Toogle</button>
    <h3 class="page-title">CareerPlus My Profile</h3>
    <small> <i class="fa fa-clock-o"></i> Last Updated on: <time>Sunday, October 05, 2014</time></small>

  </header>


    <div class="row">

  <div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-1 col-lg-10">

    <div id="profile_edit" class="panel panel-default">
      <div class="panel-heading resume-heading">

        <div class="row">
          <div class="col-lg-12">
            <div class="col-xs-12 col-sm-12">

<table style="margin-top:3%; width:100%">
		<tr>
			<td  style="width: 27%;">
			<figure>
                 <img width="100%" style="max-width:210px;" class="img-rounded img-responsive"  alt="profile picture not loaded" src="{{Auth::user()->profile_pic}}">
            </figure>
			</td>
			<td  style="width: 73%; padding-left: 5% ">
                <li class="list-group-item"><i class="fa fa-chevron-circle-right"></i> Name :
                <a href="#" id="name" data-type="text" data-placement="right" data-title="Enter your name"> {{{$name}}}</a>
                </li>
                <li class="list-group-item"><i class="fa fa-chevron-circle-right"></i> Faculty :
                <a href="#" id="faculty" data-type="select" data-placement="right" data-title="Enter your name">{{{$faculty}}}</a>
                </li>
                @if(isset($department))
                <li class="list-group-item"><i class="fa fa-chevron-circle-right"></i> Department :
                <a href="#" id="department" data-type="select" data-placement="right" data-title="Enter your name"> {{{$department}}}</a>
                </li>
                @endif
                <li class="list-group-item"><i class="fa fa-chevron-circle-right"></i> Mobile :
                    <a href="#" id="mobile_no" data-type="text" data-placement="right" data-title="Enter username">{{{$mobile_no}}}</a>
                </li>
                <li class="list-group-item"><i class="fa fa-chevron-circle-right"></i> Contact email :
                    <a href="#" id="n_email" data-type="text" data-placement="right" data-title="Enter username">{{{$n_email}}}</a>
                </li>
			</td>
		</tr>
</table>



            </div>
              <div class="row pull-right">

                  <p><a class="btn btn" href="cvbtn" role="button"><span class="glyphicon glyphicon-upload"></span>
                      Upload CV here</a></p>
              </div>
          </div>

        </div>


      </div>
      <div class="bs-callout bs-callout-danger">
        <h4>Summary</h4>
        <p class=" wide-area">
            <span id="summary" data-type="textarea">
                {{{$summary != "" ? $summary :'~Add summarized info about you and who are you ? ~ '}}}
            </span>
        </p>
      </div>
      <div class="bs-callout bs-callout-danger">
        <h4>Personal Details</h4>
        <p>Birthday	August 12</p>
          <p>  Marital Status	Single

        </p>

      </div>

      <div class="bs-callout bs-callout-danger">
        <h4>Experiences</h4>
        <ul class="list-group">

           <li id="exp_list"> @if(isset($ex_id))
                @foreach($ex_id as $index => $ex_id_no)
                    <a @if($descr[$index] == "" || $title[$index] == "") style="background-color: rgb(206, 165, 179)" @endif class="list-group-item">

                        <button id="remove_exp" onclick="exp_remove({{{$ex_id_no}}})" class="btn btn-danger btn-xs exp_remove"><i class="fa fa-times"></i></button>

                        <h4 class="list-group-item-heading wide-area">
                     <span class="title" data-name="title" data-type="textarea" data-pk="{{{$ex_id_no}}}">
                              {{{$title[$index] != "" ? $title[$index] :'Title text'}}}
                     </span>
                        </h4>
                        <p class="wide-area">
                          <span class="descr" data-name="descr" data-type="textarea" data-pk="{{{$ex_id_no}}}">
                              {{{$descr[$index] != "" ? $descr[$index] :'~Add summarized info about you and who are you ? ~ '}}}
                          </span>
                        </p>
                    </a>
                @endforeach
            @endif
           </li>
<li><button id="exp_add" style="margin: 5px 0px 5px 5px; float: right;" class="btn btn-success btn-sm">Add Experience</button></li>


        </ul>
      </div>

      <div class="bs-callout bs-callout-danger">
        <h4>Projects</h4>
        <ul class="list-group">
          <li class="list-group-item"> Lorem ipsum dolor sit amet, ea vel prima adhuc </li>
          <li class="list-group-item"> Lorem ipsum dolor sit amet, ea vel prima adhuc</li>
          <li class="list-group-item"> Lorem ipsum dolor sit amet, ea vel prima adhuc</li>
          <li class="list-group-item"> Lorem ipsum dolor sit amet, ea vel prima adhuc</li>
          <li class="list-group-item">Lorem ipsum dolor sit amet, ea vel prima adhuc</li>
          <li class="list-group-item"> Lorem ipsum dolor sit amet, ea vel prima adhuc</li>



        </ul>
      </div>
      <div class="bs-callout bs-callout-danger">
        <h4>Language and Platform Skills</h4>
        <ul class="list-group">
          <a class="list-group-item inactive-link" href="#">


            <div class="progress">
              <div data-placement="top" style="width: 80%;"
              aria-valuemax="100" aria-valuemin="0" aria-valuenow="80" role="progressbar" class="progress-bar progress-bar-success">
                <span class="sr-only">80%</span>
                <span class="progress-type">Java/ JavaEE/ Spring Framework </span>
              </div>
            </div>
            <div class="progress">
              <div data-placement="top" style="width: 70%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="1" role="progressbar" class="progress-bar progress-bar-success">
                <span class="sr-only">70%</span>
                <span class="progress-type">PHP/ Lamp Stack</span>
              </div>
            </div>
            <div class="progress">
              <div data-placement="top" style="width: 70%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="1" role="progressbar" class="progress-bar progress-bar-success">
                <span class="sr-only">70%</span>
                <span class="progress-type">JavaScript/ NodeJS/ MEAN stack </span>
              </div>
            </div>
            <div class="progress">
              <div data-placement="top" style="width: 65%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="1" role="progressbar" class="progress-bar progress-bar-warning">
                <span class="sr-only">65%</span>
                <span class="progress-type">Python/ Numpy/ Scipy</span>
              </div>
            </div>
            <div class="progress">
              <div data-placement="top" style="width: 60%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="60" role="progressbar" class="progress-bar progress-bar-warning">
                <span class="sr-only">60%</span>
                <span class="progress-type">C</span>
              </div>
            </div>
            <div class="progress">
              <div data-placement="top" style="width: 50%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="50" role="progressbar" class="progress-bar progress-bar-warning">
                <span class="sr-only">50%</span>
                <span class="progress-type">C++</span>
              </div>
            </div>
            <div class="progress">
              <div data-placement="top" style="width: 10%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="50" role="progressbar" class="progress-bar progress-bar-danger">
                <span class="sr-only">10%</span>
                <span class="progress-type">Go</span>
              </div>
            </div>

            <div class="progress-meter">
              <div style="width: 25%;" class="meter meter-left"><span class="meter-text">Poor</span></div>
              <div style="width: 25%;" class="meter meter-left"><span class="meter-text">Weak</span></div>
              <div style="width: 30%;" class="meter meter-right"><span class="meter-text">Better</span></div>
              <div style="width: 20%;" class="meter meter-right"><span class="meter-text">Good</span></div>
            </div>

          </a>

        </ul>
      </div>
      <div class="bs-callout bs-callout-danger">
        <h4>Education</h4>
        <table class="table table-striped table-responsive ">
          <thead>
            <tr><th>Degree</th>
            <th>Graduation Year</th>
            <th>GPA</th>
          </tr></thead>
          <tbody>
            <tr>
              <td>Masters in Computer Science and Engineering</td>
              <td>2014</td>
              <td> 3.50 </td>
            </tr>
            <tr>
              <td>BSc. in Computer Science and Engineering</td>
              <td>2011</td>
              <td> 3.25 </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>

  </div>
</div>

</div>

</div>
@section('js_lib')
    @parent
    <script>
        $(document).ready(function($){
            $('#exp_add').click(function(){
                $.get("{{action('profile_exp_add')}}",function(response){
$('#exp_list').html(response);
                    $('.descr').editable({
                        type:'textarea',
                        showbuttons: 'bottom',
                        rows: 5
                    });

                    $('.title').editable({
                        type:'textarea',
                        showbuttons: 'bottom',
                        rows: 1
                    });
                });
            });

        });


        function exp_remove(id){
            $.get("{{action('profile_exp_remove')}}",{id:id},function(response){
                $('#exp_list').html(response);
                $('.descr').editable({
                    type:'textarea',
                    showbuttons: 'bottom',
                    rows: 5
                });

                $('.title').editable({
                    type:'textarea',
                    showbuttons: 'bottom',
                    rows: 1
                });
            });
        }

    </script>
@stop
