
@if(isset($ex_id))
    @foreach($ex_id as $index => $ex_id_no)
        <a @if($descr[$index] == "" || $title[$index] == "") style="background-color: rgb(206, 165, 179)" @endif class="list-group-item">
            <button id="remove_exp" onclick="exp_remove({{{$ex_id_no}}})" class="btn btn-danger btn-xs exp_remove"><i class="fa fa-times"></i></button>
            <h4 class="list-group-item-heading wide-area">
                     <span class="title" data-name="title" data-type="textarea" data-pk="{{{$ex_id_no}}}">
                             {{{$title[$index] != "" ? $title[$index] :'Title text'}}}
                     </span>
            </h4>
            <p class="wide-area">
                          <span class="descr" data-name="descr" data-type="textarea" data-pk="{{{$ex_id_no}}}">
                              {{{$descr[$index] != "" ? $descr[$index] :'~Add summarized info about you and who are you ? ~ '}}}
                          </span>
            </p>
        </a>
    @endforeach
@endif


