<?php
$username="Sachintha Nanayakara";
$meta_description="University of Moratuwa - Career Guidance Unit Web Based Management System";
$page_title = "Student panel";
?>

@extends('users.layout.main')
@section('css')
@parent
            <link href="{{URL::to('/')}}/src/css/plugins/timeline.css" rel="stylesheet">
            <link href="{{URL::to('/')}}/src/css/student_panel.css" rel="stylesheet">
            <link href="{{URL::to('/')}}/src/css/profile.css" rel="stylesheet" type="text/css">
@stop

@section('content')
    @include('users.student.topbar')
<div class="page-container">
    @include('users.student.sidebar')

     <div id="page-wrapper">
        @if($page == 'home')
            @include('users.student.home')
        @elseif($page=='profile')
            @include('users.student.profile')
        @elseif($page=='friends')
            @include('users.student.friends')
        @elseif($page=='adv_search')
             @include('users.student.adv_search')
        @endif
    </div>
    </div>

@stop

@section('footer')

@stop

@section('js_lib')
@parent

@stop