
<div class="container-fluid">



<div class="raw">
<br/><br/>
    <div class="col-lg-6">

        <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-rss fa-fw"></i> Events
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">

                            <?php $events = DB::table('post')->where('cat','adm-event')->orderBy('posted_time','desc')->get();?>

                @foreach($events as $event)
                                <div class="media">
                                    @if(isset($event->img))
                                  <a class="media-left" href="#">
                                    <img src="{{URL::to('/')}}/post_img/{{$event->img}}" alt="...">
                                  </a>
                                    @else
                                        <a class="media-left" href="#">
                                            <img src="{{URL::to('/')}}/img/av1.jpg" alt="...">
                                        </a>
                                    @endif
                                  <div class="media-body">
                                    <h4 class="media-heading">{{{$event->title}}}</h4>
                                      {{{$event->desc}}}
                                      <br>
                                      <small>posted @ {{{$event->posted_time}}}</small>
                                  </div>
                                </div>
                                <br>
                @endforeach



                         </div>
                        <!-- /.panel-body -->
         </div>


     </div>

     <div class="col-lg-6">

 <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-rss fa-fw"></i> Vacancies
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">



                            <?php $events = DB::table('post')->where('cat','ind-job')->orderBy('posted_time','desc')->get();?>

                            @foreach($events as $event)
                                <div class="media">
                                    @if(isset($event->img))
                                        <a class="media-left" href="#">
                                            <img src="{{URL::to('/')}}/post_img/{{$event->img}}" alt="...">
                                        </a>
                                    @else
                                        <a class="media-left" href="#">
                                            <img src="{{URL::to('/')}}/img/av1.jpg" alt="...">
                                        </a>
                                    @endif
                                    <div class="media-body">
                                        <h4 class="media-heading">{{{$event->title}}}</h4>
                                        {{{$event->desc}}}
                                        <br>
                                        <small>posted @ {{{$event->posted_time}}}</small>
                                    </div>
                                </div>
                                <br>
                            @endforeach

                         </div>
                        <!-- /.panel-body -->
         </div>
     </div>
</div>
</div>
</div>