<!doctype html>
    <html>
        <head>
     <!--prevent browser caching for developing purpose-->
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="cache-control" content="no-store" />
<meta http-equiv="cache-control" content="must-revalidate" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="{{{$meta_description}}}">

     <title>{{{$page_title}}}</title>

    <link rel="shortcut icon" href="{{URL::to('/')}}/favicon.ico" type="image/vnd.microsoft.icon" />


@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.4/css/bootstrap.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/css/bootstrap2/bootstrap-switch.css" rel="stylesheet">
@show

</head>
    <body>
        @yield('content')



@section('footer')
    <footer id="footer" style="background-color: #28120d">
      <div class="container" role="contentinfo">
        <div class="row">
          <div class="col-sm-12">
          <br><br>
          	<div class="row">
              <div class="col-md-12">
                <a href="#">Home</a>
                <a href="#"> | About Us</a>
                <a href="#"> | Contact</a>
                <a href="#"> | Main Site</a>
              </div>
           	</div>
          </div>
        </div>

        <div class="row">
        	<div class="col-md-12">
              	<p class="text-right">
              	Developed by Team-Flash | Maintained by UOM Career Guidance Unit
                </p>
          	</div>
        </div>
      </div>
    </footer>
@show
</body>
@section('js_lib')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.4/js/bootstrap.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/js/bootstrap-switch.js" ></script>
@show
@section('js_user')
    <script src="{{URL::to('/')}}/src/js/dashboard.js"></script>
    <script src="{{URL::to('/')}}/src/js/editable.js"></script>
@show
</html>
