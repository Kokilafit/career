<div class="container-fluid">


    <form class="form-horizontal" id="std_form" action="{{ action('AlmSignUpController@postSignUp') }}" method="post">


        <input type="hidden" name="user_role" value="alm" />
        <input type="hidden" name="token" value="{{ csrf_token(); }}">


        <div class="form-group">
            <label for="username" class="col-sm-3 control-label">
                Index No :
            </label>
            <div class="col-sm-9">
                <input type="text" class="form-control" placeholder="124000H" id="username" name="username">
            </div>
        </div>

        <div class="form-group">
            <label for="username" class="col-sm-3 control-label">
                Email :
            </label>
            <div class="col-sm-9">
                <input type="email" class="form-control" placeholder="example@gmail.com" id="email " name="p_email">
            </div>
        </div>


        <div class="form-group">
            <label for="password" class="col-sm-3 control-label">
                Password
            </label>
            <div class="col-sm-9">
                <input type="password" class="form-control" id="password" value="" name="password"   >
            </div>
        </div>

        <div class="form-group">
            <label for="password_confirmation" class="col-sm-3 control-label">
                Password Confirmation
            </label>
            <div class="col-sm-9">
                <input type="password" class="form-control" id="password_confirmation" value="" name="password_confirmation"   >
            </div>
        </div>

        <div class="form-group">
            <label for="human" class="col-sm-3 control-label">

            </label>
            <div class="col-sm-9 text-center">
                <div id="recaptcha4"></div>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="agreement[]">
                        I agree Website Terms and Conditions <br/>by University Of Moratuwa
                    </label>
                </div>
            </div>
        </div>

        <div class="row text-right">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Create Account</button>
        </div>

    </form>
</div>