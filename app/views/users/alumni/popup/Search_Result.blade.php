<style>
    .modal .modal-dialog { width: 80%;
    }
</style>


    <div class="modal" id="popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">@if(isset($asd->name)){{ $asd->name }}@endif</h4>
                </div>
                <div class="modal-body" id="udata">


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary">Private Massage</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                </div>
            </div>
        </div>
    </div>