
<?php
$meta_description="University of Moratuwa - Career Guidance Unit Web Based Management System";
$page_title = "Alumni panel";
?>

@extends('users.layout.main')
@section('css')
    @parent
    <link href="{{URL::to('/')}}/src/css/plugins/timeline.css" rel="stylesheet">
    <link href="{{URL::to('/')}}/src/css/admin_panel.css" rel="stylesheet">
    <link href="{{URL::to('/')}}/src/css/profile.css" rel="stylesheet" type="text/css">
@stop

@section('content')
    @include('users.alumni.topbar')
    <div class="page-container">
        @include('users.alumni.sidebar')

        <div id="page-wrapper">
            @if($page == 'home')
                @include('users.alumni.home')
            @elseif($page=='profile')
                @include('users.alumni.profile')
            @elseif($page=='friends')
                @include('users.alumni.friends')
            @elseif($page=='adv_search')
                @include('users.alumni.adv_search')
            @elseif($page=='alm_portal')
                @include('users.alumni.alm_portal')
            @endif
        </div>
    </div>

@stop

@section('footer')

@stop

@section('js_lib')
    @parent

@stop