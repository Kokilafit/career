
<div class="container-fluid">



<div class="raw">
<br/><br/>
    <div class="col-lg-6">

        <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-rss fa-fw"></i> Events
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">


                                <div class="media">
                                  <a class="media-left" href="#">
                                    <img src="{{URL::to('/')}}/img/av1.jpg" alt="...">
                                  </a>
                                  <div class="media-body">
                                    <h4 class="media-heading">CV Clinic</h4>
                                   Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                                  </div>
                                </div
                                <br>

                                <div class="media">
                                  <a class="media-left" href="#">
                                    <img src="{{URL::to('/')}}/img/ev2.jpeg" alt="...">
                                  </a>
                                  <div class="media-body">
                                    <h4 class="media-heading">Mock Interview session</h4>
                                    Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus
                                  </div>
                                </div>
                                <div class="media">
                                  <a class="media-left" href="#">
                                    <img src="{{URL::to('/')}}/img/av1.jpg" alt="...">
                                  </a>
                                  <div class="media-body">
                                    <h4 class="media-heading">Interview session</h4>
                                   Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                                  </div>
                                </div
                                <br>
                                <div class="media">
                                  <a class="media-left" href="#">
                                    <img src="{{URL::to('/')}}/img/ev2.jpeg" alt="...">
                                  </a>
                                  <div class="media-body">
                                    <h4 class="media-heading">Mock Interview session</h4>
                                    Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus
                                  </div>
                                </div>

                         </div>
                        <!-- /.panel-body -->
         </div>


     </div>

     <div class="col-lg-6">

 <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-rss fa-fw"></i> Vacancies
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">


                                <div class="media">
                                  <a class="media-left" href="#">
                                    <img src="{{URL::to('/')}}/img/vac1.jpg" alt="...">
                                  </a>
                                  <div class="media-body">
                                    <h4 class="media-heading">Vacancy for software Engineer</h4>
                                   Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                                  </div>
                                </div
                                <br>
                                <div class="media">
                                  <a class="media-left" href="#">
                                    <img src="{{URL::to('/')}}/img/vac2.gif" alt="...">
                                  </a>
                                  <div class="media-body">
                                    <h4 class="media-heading">Vacancy for soWeb Designer</h4>
                                    Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus
                                  </div>
                                </div>
                                <div class="media">
                                  <a class="media-left" href="#">
                                    <img src="{{URL::to('/')}}/img/vac3.png" alt="...">
                                  </a>
                                  <div class="media-body">
                                    <h4 class="media-heading">Vacancy for Civil Engineer</h4>
                                   Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                                  </div>
                                </div
                                <br>
                                <div class="media">
                                  <a class="media-left" href="#">
                                    <img src="{{URL::to('/')}}/img/vac4.jpg" alt="...">
                                  </a>
                                  <div class="media-body">
                                    <h4 class="media-heading">Vacancy 4</h4>
                                    Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus
                                  </div>
                                </div>

                         </div>
                        <!-- /.panel-body -->
         </div>
     </div>
</div>
</div>
</div>