@section('asd')

<div class="sidebar-menu" style="z-index: 1001;">
    <header class="logo">
        <a href="#" class="sidebar-icon"> <span class="fa fa-bars"></span> </a>
        <a href="#"><!-- <span id="logo" class="fa fa-html5 fa-5x"></span>--><img id="logo" src="{{URL::to('/')}}/{{Auth::user()->profile_pic}}" alt="Logo"/></a>
    </header>

    <div style="border-top:1px solid rgba(69, 74, 84, 0.7)"></div>

    <div class="menu">
      <ul id="menu" >
        <li id="menu-home" ><a href="{{{ route('ind_dashboard', "home")}}}"><i class="fa fa-home fa-2x"></i><span>Home</span></a></li>
        <li id="menu-home" ><a href="{{{ route('ind_dashboard', "profile")}}}"><i class="fa fa-edit fa-fw fa-2x"></i><span>profile</span></a></li>
        <li><a href="#"><i class="fa fa-sitemap fa-2x"></i><span>Connections</span><span class="fa fa-angle-right" style="float: right"></span></a>
          <ul>
            <li><a href="{{{ route('ind_dashboard', "friends")}}}"><i class="fa fa-check-square-o"></i> Followed Students</a></li>
            <li><a href="{{{ route('ind_dashboard', "add_connections")}}}"><i class="fa fa-plus-circle"></i> Add connections</a></li>
          </ul>
        </li>

          <li id="menu-home" ><a href="{{{ route('ind_dashboard', "Vacancy_post")}}}"><i class="fa fa-briefcase fa-2x"></i><span>Post Vacancy</span></a></li>

        {{--<li id="menu-academico" ><a href="#"><i class="fa fa-briefcase fa-2x"></i><span>Jobs</span><span class="fa fa-angle-right" style="float: right"></span></a>
          <ul id="menu-academico-sub" >
            <li id="menu-academico-avaliacoes" ><a href="#">Posted Jobs</a></li>
            <li id="menu-academico-boletim" ><a href="#">Post New Job</a></li>
            <li id="menu-academico-boletim" ><a href="#">Post New Event</a></li>
          </ul>
        </li>--}}
          <li><a href="{{{ route('ind_dashboard', "adv_search")}}}"><i class="fa fa-search-plus fa-2x"></i><span>Advanced Search</span></a></li>
        <li><a href="#"><i class="fa fa-gears fa-2x"></i><span>Settings</span></a></li>
        <li><a href="{{{ URL::to('/')}}}/logout"><i class="fa fa-sign-out fa-2x"></i><span>Logout</span></a></li>
        <!--<li id="menu-comunicacao" ><a href="#"><i class="fa fa-anchor"></i><span>Category</span><span class="fa fa-angle-double-right" style="float: right"></span></a>
                  <ul id="menu-comunicacao-sub" >
                    <li id="menu-mensagens" style="width: 120px" ><a href="#">Item 1<i class="fa fa-angle-right" style="float: right; margin-right: -8px; margin-top: 2px;"></i></a>
                      <ul id="menu-mensagens-sub" >
                        <li id="menu-mensagens-enviadas" ><a href="#">Item 1.1</a></li>
                        <li id="menu-mensagens-recebidas" ><a href="#">Item 1.2</a></li>
                        <li id="menu-mensagens-nova" ><a href="#">Item 1.3</a></li>
                      </ul>
                    </li>
                    <li id="menu-arquivos" ><a href="#">Item 2</a></li>
                  </ul>
              </li>-->
      </ul>
    </div>

  </div>

            <!-- /.navbar-static-side -->
@show