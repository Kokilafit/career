<?php
$username="CareerPlus admin";
$meta_description="University of Moratuwa - Career Guidance Unit Web Based Management System";
$page_title = "Industry panel";
?>

@extends('users.layout.main')
@section('css')
@parent
            <!-- Timeline CSS-->
            <link href="{{URL::to('/')}}/src/css/plugins/timeline.css" rel="stylesheet">
            <!-- Custom CSS-->
            <link href="{{URL::to('/')}}/src/css/industry_panel.css" rel="stylesheet">
            <!-- Morris Charts CSS-->
            <link href="{{URL::to('/')}}/src/css/plugins/morris.css" rel="stylesheet">
            <!-- Custom Fonts -->
            <link href="{{URL::to('/')}}/src/font-awesome-4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
            <link href="{{URL::to('/')}}/src/css/profile.css" rel="stylesheet" type="text/css">

            <!-- bootstrap -->
@stop

@section('content')
    @include('users.industry.topbar')
<div class="page-container">
    @include('users.industry.sidebar')

     <div id="page-wrapper">
        @if($page == 'home')
            @include('users.industry.home')
        @elseif($page=='profile')
            @include('users.industry.profile')
        @elseif($page=='friends')
            @include('users.industry.friends')
         @elseif($page=='job_post')
             @include('users.industry.job_post')
         @elseif($page=='Vacancy_post')
             @include('users.industry.Vacancy_post')
         @elseif($page=='adv_search')
             @include('users.industry.adv_search')
        @endif
    </div>
    </div>
@stop
@section('footer')
@stop

@section('js')
@parent
<script type="text/javascript" src="{{URL::to('/')}}/src/js/typeahead.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	var substringMatcher = function(strs) {
  return function findMatches(q, cb) {
    var matches, substrRegex;

    // an array that will be populated with substring matches
    matches = [];

    // regex used to determine if a string contains the substring `q`
    substrRegex = new RegExp(q, 'i');

    // iterate through the pool of strings and for any string that
    // contains the substring `q`, add it to the `matches` array
    $.each(strs, function(i, str) {
      if (substrRegex.test(str)) {
        // the typeahead jQuery plugin expects suggestions to a
        // JavaScript object, refer to typeahead docs for more info
        matches.push({ value: str });
      }
    });

    cb(matches);
  };
};

var states = ['Students belongs to IT Faculty : ','Students belongs to Engineer Faculty : '
,'Students belongs to Engineer Faculty : ','Students belongs to Engineer Faculty : '
];

$('#the-basics .typeahead').typeahead({
  hint: true,
  highlight: true,
  minLength: 1
},
{
  name: 'states',
  displayKey: 'value',
  source: substringMatcher(states)
});
});
</script>
@stop