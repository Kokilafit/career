@section('css')
    @parent
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/css/bootstrap-tokenfield.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.css"/>
    <style>
        .pagination{
            position: relative;
            left: 50%;
            transform: translateX(-50%);
        }
        tr:hover .profile_thumb{
            z-index: 10;
            -webkit-transform:scale(1.25); /* Safari and Chrome */
            -moz-transform:scale(1.25); /* Firefox */
            -ms-transform:scale(1.25); /* IE 9 */
            -o-transform:scale(1.25); /* Opera */
            transform:scale(1.25);
            border-radius: 5px;
        }
        tr:hover{
            background-color: rgba(0, 141, 231, 0.24);
        }
        tr{
            font-size: medium;
        }
        .panel-heading{
            color: #008DE7 !important;
            background-color: #303641 !important;
        }
        .panel-heading:hover{
            background-color: #282b49 !important;
        }
        .panel-body {
            background-color: #B0AFAF;
        }


        .tt-query, /* UPDATE: newer versions use tt-input instead of tt-query */
        .tt-hint {
            width: 396px;
            height: 30px;
            padding: 8px 12px;
            font-size: 15px;
            line-height: 30px;
            border: 2px solid #ccc;
            border-radius: 8px;
            outline: none;
        }

        .tt-query { /* UPDATE: newer versions use tt-input instead of tt-query */
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        }

        .tt-hint {
            color: #999
        }

        .tt-dropdown-menu {
            width: 422px;
            margin-top: 12px;
            padding: 8px 0;
            background-color: #fff;
            border: 1px solid #ccc;
            border: 1px solid rgba(0, 0, 0, 0.2);
            border-radius: 8px;
            box-shadow: 0 5px 10px rgba(0,0,0,.2);
        }

        .tt-suggestion {
            padding: 3px 20px;
            font-size: 14px;
            line-height: 18px;
        }

        .tt-suggestion.tt-is-under-cursor { /* UPDATE: newer versions use .tt-suggestion.tt-cursor */
            color: #fff;
            background-color: #0097cf;

        }

        .tt-suggestion p {
            margin: 0;
        }

    </style>
@stop


<div class="container-fluid">
	<div class="row-fluid">
<br/>

        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="SearchOptions" data-toggle="collapse"  data-parent="#accordion"  data-target="#collapseOne">
                    <h4 class="panel-title">
                        <a data-toggle="collapse"  data-parent="#accordion"  href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            <i class="fa fa-search-plus"></i> Plus Search
                        </a>
                    </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="SearchOptions">
                    <div class="panel-body">
                        <form id="searchForm"  method="get" class="form-horizontal" role="form">
                            <div class="form-group">
                                    <label for="query" class="col-sm-2 control-label">Student Name :</label>

                                    <div class="col-sm-8">
                                        <input name="query" type="text" class="form-control" id="query_txt" placeholder="">
                                    </div>

                                    <div class="col-sm-2">
                                    <button id="search_btn" class="collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#collapseTwo" style="width: 100%;" type="submit" class="btn"><i class="fa fa-search-plus"></i> Search</button>
                                    </div>
                            </div>

                            <div class="form-group">
                                    <label for="tag_list" class="col-sm-2 control-label">Skill tags :</label>
                                    <div class="col-sm-10">
                                        <input name="tag_list" type="text"  class="form-control" id="tag_list" placeholder="enter tag ex : list JAVA,PHP,CIMA">
                                    </div>
                            </div>

                            <div class="form-group">
                                <label for="GPA" class="col-xs-2 control-label">GPA :</label>
                                <div class="col-xs-2">
                                    <input name="GPA" type="number" value="2.5" step="0.1" min="0.5" max="4.2" class="form-control" id="GPA"/>
                                </div>

                                <div class="col-xs-8">
                                    <div class="col-xs-2">  <label for="faculty" class="control-label">Faculty:</label></div>
                                    <div class="col-xs-10">
                                        <select class="form-control" name="faculty" id="faculty">
                                            <option value="none">Select Faculty(optional)</option>
                                            <option value="Efac">Faculty of Engineering</option>
                                            <option value="ITfac">Faculty of Information Technology</option>
                                            <option value="Afac">Faculty of Architecture</option>
                                        </select></div>
                                </div>
                            </div>


                            <div class="form-group">

                                <label for="EmployeeStatus" class="col-sm-2 control-label">Employee ? :</label>
                                <div class="col-sm-10 radio">
                                    <label><input type="radio" id="emp1" name="EmployeeStatus" value="Y"> Employed</label>
                                    <label><input type="radio" id="emp2" name="EmployeeStatus" value="N"> Unemployed</label>
                                    <button type="button" class="btn btn-danger btn-xs" onclick="document.getElementById('emp1').checked = false; document.getElementById('emp2').checked = false"><i class="fa fa-remove"></i></button>
                                </div>
                            </div>




                        </form>


                    </div>
                </div>
            </div>






            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="SearchResults" class="collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#collapseTwo">
                    <h4 class="panel-title">
                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            <i class="fa fa-group"></i> Results :
                        </a>
                    </h4>
                </div>
                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="SearchResults">
                    <div class="panel-body">
                        <div id="results">

                        </div>
                    </div>
                </div>
            </div>


	</div>
</div>
</div>
@section('js_lib')
    @parent
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.js"></script>-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.10.4/dist/typeahead.bundle.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/bootstrap-tokenfield.js" type="text/javascript"></script>
@stop

@section('js_user')
@parent
<script type="text/javascript">



$(document).ready(function(){

    $("#searchForm").submit(function() { //replace default form submit function to send get request via AJAX
        getPosts(1);
        return false;
    });

    $(window).on('hashchange', function() {   // for pagination
        if (window.location.hash) {
            var page = window.location.hash.replace('#', '');
            if (page == Number.NaN || page <= 0) {
                return false;
            } else {
                getPosts(page);
            }
        }
    });

    $(document).ready(function() {  // for pagination
        $(document).on('click', '.pagination a', function (e) {
            getPosts($(this).attr('href').split('page=')[1]);
            e.preventDefault();
        });
    });




    function getPosts(page) {
        var querytxt = $('#query_txt').val(); //get and store form inputs in js variables
        var tags = $('#tag_list').val();
        var ogpa = $('#GPA').val();
        var faculty = $('#faculty').val();
        var employee = $('input[name=EmployeeStatus]:checked').val();
        $.get("{{{action('IndSearchController@search')}}}", //send AJAX request with search terms and catch the response
                {query : querytxt, skills : tags, ogpa : ogpa, faculty : faculty, employee : employee, page : page},
                function(json){
                    $('#results').html(json);
                    var toggle = $("[aria-expanded^='false']") //for UI animation- expand result segment and collapse search from
                    $("[aria-expanded^='true']").attr('aria-expanded',false);
                    toggle.attr('aria-expanded',true);
                }
        );
    }

    var $tagList = $('#tag_list');
    var engine = new Bloodhound({
        remote: {
            url: '/get_token?query=%QUERY'
        },
        datumTokenizer: function (d) {
            return Bloodhound.tokenizers.whitespace(d.value);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace
    });

    engine.initialize();

    $tagList.tokenfield({
        typeahead: [
            {hint: true,
            highlight: true,
            minLength: 2},
            {displayKey: 'skill', source: engine.ttAdapter()}
        ]
    });








});
</script>
@stop

