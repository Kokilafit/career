@section('css')
    @parent
    {{HTML::style('/src/css/alm_css.css')}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/css/bootstrap-tokenfield.min.css" rel="stylesheet">


@stop

<div class="row">

    <div class="timeline-centered">

         <article class="timeline-entry">

            <div class="timeline-entry-inner">

                <div onclick="toggleForm('post_form')" class="timeline-icon" style="-webkit-transform: rotate(-90deg); -moz-transform: rotate(-90deg);">
                    <i class="entypo-flight"></i> +
                </div>


                <div hidden id="post_form" class="timeline-label">
                    <form class="form-horizontal" enctype="multipart/form-data" role="form" method="post" action="{{action('IndJobPostController@add')}}">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Title</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="title" name="title" placeholder="add title for the post" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="description" class="col-sm-2 control-label">Description</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" rows="3" name="description"></textarea>
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="tag_list" class="col-sm-2 control-label">Skill tags :</label>
                            <div class="col-sm-10">
                                <input name="tag_list" type="text"  class="form-control" id="tag_list" placeholder="enter tag ex : list JAVA,PHP,CIMA">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="GPA" class="col-xs-2 control-label">GPA Above :</label>
                            <div class="col-xs-2">
                                <input name="GPA" type="number" value="2.5" step="0.1" min="0.5" max="4.2" class="form-control" id="GPA"/>
                            </div>

                            <div class="col-xs-8">
                                <div class="col-xs-2">  <label for="faculty" class="control-label">Faculty:</label></div>
                                <div class="col-xs-10" style="padding-right: 0px">
                                        <select id="fac" class="form-control" name="fac">
                                            <option selected disabled hidden value=""></option>
                                            <option selected="true" value="Efac">Faculty of Engineering</option>
                                            <option value="ITfac">Faculty of Information Technology</option>
                                            <option value="Afac">Faculty of Architecture</option>
                                        </select>
                                </div>
                        </div>

                        </div>



                            <div class="form-group">
                                <label for="name" class="col-sm-2 control-label">Department </label>
                                <div class="col-sm-10">
                                    <select id="Afac" class="dep_select form-control" name="dep">
                                        <option selected disabled hidden value=""></option>
                                        <option value="DArc">Department of Architecture</option>
                                        <option value="DID"> Department of Integrated Design</option>
                                        <option value="DBE">Department of Building Economics</option>
                                        <option value="DTCP">Department of Town & Country Planning</option>
                                    </select>
                                    <select id="Efac" class="dep_select form-control" name="dep">
                                        <option selected disabled hidden value=""></option>
                                        <option value="DCPE">Department of Chemical & Process Engineering</option>
                                        <option value="DCE">Civil Engineering</option>
                                        <option value="DCSE">Department of computer Science & Engineering</option>
                                        <option value="DER">Earth Resource Engineering</option>
                                        <option value="DTEE">Electrical Engineering</option>
                                        <option value="DENTC">Electronic & Telecommunication Engineering</option>
                                        <option value="DMSE">Material Science & Engineering</option>
                                        <option value="DMech">Mechanical Engineering</option>
                                        <option value="DCT">Textile & Clothing Technology</option>
                                        <option value="DTLM">Transport & Logistics Management</option>
                                    </select>
                                </div>
                            </div>





                        <div class="form-group">

                            <label for="email" class="col-sm-2 control-label">Image</label>
                            {{--<div class="col-sm-8">
                                <input type="email" class="form-control" id="email" name="email" placeholder="example@domain.com" value="">
                            </div>--}}
                            <div class="col-sm-8">
                                <span class="btn btn-default btn-file" style="width: 100%">
                                    Select image for post <input type="file" name="img">
                                </span>
                            </div>
                            <div class="col-sm-2">
                                <button type="submit" style="width: 100%" class="btn btn-primary">Post</button>
                            </div>
                        </div>

                    </form>

                </div>
            </div>

        </article>

        <br>
<?php $posts = DB::table('post')->where('uid',Auth::id())->where('cat','ind-job')->orderBy('id', 'desc')->get(); ?>

        @if(isset($posts))

            @foreach($posts as $post)
                <article class="timeline-entry">

                    <div class="timeline-entry-inner">

                        <div class="timeline-icon bg-secondary" style="background-image: url('{{URL::to('/')}}/{{Auth::user()->profile_pic}}'); background-size: cover">
                            <i class="entypo-feather"></i>
                        </div>

                        <div class="timeline-label">
                            <h2><a href="#">{{{$post->title}}}</a> <span> @ {{{$post->posted_time}}}</span></h2>
                            <p>{{{$post->desc}}}</p>
                            @if(isset($post->img))
                                <img class="img-responsive" src="{{URL::to('/')}}/post_img/{{{$post->img}}}">
                            @endif
                        </div>
                        <button onclick="toggleDiv('list_'+'{{$post->id}}')" style="float: right; transform: translateY(-50%)" type="submit" class="btn btn-success btn-xs">Top 5 Suggestion</button>
                    </div>

                </article>

                <article class="timeline-entry" id="list_{{$post->id}}" hidden>

                    <div class="timeline-entry-inner">
                        <div class="und_list" style="width: 90%; float: right">
test
                        </div>
                    </div>

                </article>
            @endforeach

                <article class="timeline-entry">

                    <div class="timeline-entry-inner">

                        <div class="timeline-icon bg-secondary">
                            <i class="entypo-feather"></i>
                        </div>
                        <div class="timeline-label">

                        </div>
                    </div>

                </article>

        @endif




    </div>


</div>
@section('js_lib')
@parent
<script src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.10.4/dist/typeahead.bundle.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/bootstrap-tokenfield.min.js" type="text/javascript"></script>
    <script type="text/javascript">
    $(document).ready(function(){
    $('#Afac').hide();

    $('#fac').change(function(){
    var fac = $(this).val();

    var cls = $('#'+fac);
    cls.removeClass('.dep_select');
    $('.dep_select').hide();
    cls.addClass('dep_select');
    if ( !cls.is(':visible') ) cls.fadeIn(500);

    });
    });


    var $tagList = $('#tag_list');
    var engine = new Bloodhound({
        remote: {
            url: '/get_token?query=%QUERY'
        },
        datumTokenizer: function (d) {
            return Bloodhound.tokenizers.whitespace(d.value);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace
    });

    engine.initialize();

    $tagList.tokenfield({
        typeahead: [
            {hint: true,
                highlight: true,
                minLength: 1},
            {displayKey: 'skill', source: engine.ttAdapter()}
        ]
    });



    function toggleDiv(divId) {
        var my_div = $("#"+divId);
        if(my_div.is(":visible")){
            my_div.slideToggle("fast", function() {
                $('#'+divId+"> .timeline-entry-inner > .und_list").empty();
            });
        }else{
            var job_id = divId.split('_');
            $.get("{{{action('IndSearchController@search')}}}", //send AJAX request with job_id
                    {job_id : job_id[1] , page : 1},
                    function(json){
                        $('#'+divId+"> .timeline-entry-inner > .und_list").html(json);
                        my_div.slideToggle();
                    }
            );
        }

    }


    function toggleForm(divId) {
        $("#"+divId).slideToggle();
    }


    </script>



@stop