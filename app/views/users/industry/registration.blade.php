<?php
$meta_description="University of Moratuwa - Career Guidance Unit Web Based Management System";
$page_title = "Registration";
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <!--prevent browser caching for developing purpose-->
    <meta http-equiv='cache-control' content='no-cache'>
    <meta http-equiv='expires' content='0'>
    <meta http-equiv='pragma' content='no-cache'>

    <link rel="shortcut icon" href="{{URL::to('/')}}/favicon.ico" type="image/vnd.microsoft.icon" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>{{$page_title}}</title>

    <meta name="description" content="{{$meta_description}}">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    @section('topincludes')
        <script src="//cdn.ckeditor.com/4.4.7/standard/ckeditor.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.4/css/bootstrap.css" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/css/bootstrap2/bootstrap-switch.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
        <link href="{{URL::to('/')}}/src/css/stdregistration.css" rel="stylesheet">
        <link rel="stylesheet" href="{{URL::to('/')}}/src/css/main.css">
        <script src="{{URL::to('/')}}/src/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>

    @show


</head>
<body>

<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->


@section('content')
    <div>

    </div>
    {{ Form::open(array('url'=>action('IndProfileBuildController@buildProfile'),'method'=>'POST','novalidate', 'files'=>true,'id'=>'msform')) }}

    <!-- progressbar -->
    <ul id="progressbar">
        <li class="active">Hello</li>
        <li>Brush Up Your Profile</li>
        <li>Collecting Details</li>
        <li>Social Links</li>
        <li>Thank You</li>
    </ul>
    @if(isset($error))
        {{$error}}fghfg
        @endif
                <!-- fieldsets -->
        <fieldset>
            <h1 class="fs-title">Hi There</h1>
            <h3 class="fs-subtitle">This is Career Plus</h3>
            <h3 class="fs-subtitle">You are about to register into Career Plus</h3>
            <h3 class="fs-subtitle">Browse through amazing pool of candidates for you job appertunities </h3>
            <h3 class="fs-subtitle">Be the first to reach out to a leading government university </h3>
            <h3 class="fs-subtitle">Good Luck </h3>
            <h3 class="fs-subtitle">Click Next to proceed </h3>
            <h3 class="fs-subtitle">Read <a>Privacy policy</a> </h3>
            <input type="button" id="reg_btn" name="next" class="next" />
        </fieldset>
        <fieldset>
            <h2 class="fs-title">Brush Up Your Profile</h2>
            <h3 class="fs-subtitle">Add the company logo</h3>
            <div class="fs-input">
                <div class="image-editor">

                    <input type="file" id="files" class="cropit-image-input hidden">

                    <label for="files" id="overlay">
                        <span class="fa fa-camera fa-2x"></span><br/>
                        Browse Images
                    </label>
                    <div class="cropit-image-preview" style="background:url('{{URL::to('/').Auth::user()->profile_pic}}') no-repeat center">
                    </div>
                    <input type="range" class="cropit-image-zoom-input">


                    <input type="hidden" name="image-data" class="hidden-image-data" />
                </div>


            </div>
            {{--            <h3 class="fs-subtitle">Add a nice cover photo</h3>
                        <div class="fs-input">
                            <div class="image-editor">

                                <input type="file" id="cover-files" class="cropit-image-input hidden">

                                <label for="cover-files" id="overlay">
                                    <span class="fa fa-camera fa-2x"></span><br/>
                                    Browse Images
                                </label>
                                <div class="cover-input" style="background:url(()'{{URL::to('/').Auth::user()->profile_pic}}') no-repeat center">
                                </div>



                                <input type="hidden" name="cover-image-data" class="hidden-image-data" />
                            </div>
                        </div>--}}
            <input type="button" id="2-1" name="previous" class="previous" />
            <input type="button" id="2-3" name="next" class="next" />
        </fieldset>
        <fieldset>
            <h2 class="fs-title">Collecting Details</h2>
            <div class="fs-input">
                <h4>About your company :</h4><br/>

                <textarea name="cmp-details"></textarea>
                <script>
                    CKEDITOR.replace( 'cmp-details' );
                </script>
                <br/>
            </div>
            <input type="button" id="reg_btn" name="previous" class="previous" />
            <input type="button" id="reg_btn" name="next" class="next" />
        </fieldset>

        <fieldset>
            <div>
                <h2 class="fs-title">Social Links</h2>

                <h3 class="fs-subtitle">Twitter</h3><br />
                <i class="fa fa-twitter-square" style="font-size: 30px"></i>
                <input id="social-links" name="twitter" type="url"> </input>
                <h3 class="fs-subtitle">Linkedin</h3><br />
                <i class="fa fa-linkedin-square" style="font-size: 30px"></i>
                <input id="social-links" name="linkedin" type="url"> </input>

            </div>

            <input type="button" id="reg_btn" name="previous" class="previous"/>
            <input type="button" id="reg_btn1" name="next" class="next"/>

        </fieldset>
        <fieldset>
            <h2 class="fs-title">Thank You</h2>
            <h3 class="fs-subtitle">You are almost done</h3>
            <h3 class="fs-subtitle">You can add and customize your Profile</h3>
            <input type="submit" name="next" class="btn btn-success" value="Save and exit" style="font-size: medium"/>

            <input type="button" id="reg_btn" name="previous" class="previous"/>

        </fieldset>
        </form>
@show

@section('footer')
    <!--<div id="push"></div>
</div>


<div id="footer">
<div class="container">
<br>
    <div class="col-md-6 text-left">
        <strong><a href="{{URL::to('/')}}/about">About</a> <a href="{{URL::to('/')}}/contact"> | Contact Us</a> <a href="http://mrt.ac.lk"> | Main Site</a></strong>
    </div>
    <div class="col-md-6 text-right">
            <p>Copyright &copy; 2014 University of Moratuwa All right reserved.</p>
    </div>
</div>
</div>-->
@show

@section('bottom_includes')


    <script src="//cdn.ckeditor.com/4.4.7/standard/ckeditor.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.4/js/bootstrap.js"></script>
    <script src="{{URL::to('/')}}/src/js/jquery.easing.min.js"></script>
    <script src="{{URL::to('/')}}/src/js/jquery.cropit.min.js"></script>
    <script src="{{URL::to('/')}}/src/js/registration.js"></script>
    <script>
        $(function() {
            $('.image-editor').cropit();

            $('#reg_btn1').click(function() {
                // Move cropped image data to hidden input
                var imageData = $('.image-editor').cropit('export');
                $('.hidden-image-data').val(imageData);

            });
        });
    </script>
@show

<script>
    (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
    ga('create','UA-XXXXX-X');ga('send','pageview');
</script>

</body>
</html>
