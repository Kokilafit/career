<style type="text/css">
#cover{
    background-image:url("{{URL::to('/')}}/img/IFSFlags.jpg") ;
    background-size: cover;
    height: 250px;
}
#C_logo{
    max-height: 200px;
    max-width: 150px;
    background-size: cover;
}

#C_Name{
    text-align: right;
    font-weight: 600;
    font-family: 'Open Sans' , sans-serif;
    font-size: xx-large;
}

</style>


<div class="container-fluid">
<div class="row info" style="text-align:center;">

</div>

<div class="resume">
    <header class="page-header">
    <h3 class="page-title">CareerPlus Industry Profile</h3>
    <small> <i class="fa fa-clock-o"></i> Last Updated on: <time>Sunday, October 05, 2014</time></small>
  </header>


    <div class="row">
  <div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-1 col-lg-10">
    <div class="panel panel-default">
      <div class="panel-heading resume-heading">
        <div class="row">
          <div class="col-lg-12" id="cover">
            <div class="col-xs-12 col-sm-12">


                <!--<div class="row pull-right">
                    <div class="col-xs-12 social-btns">

                        <div class="col-xs-3 col-md-1 col-lg-1 social-btn-holder">
                            <a href="#" class="btn btn-social btn-block btn-google">
                                <i class="fa fa-google"></i> </a>
                        </div>


                        <div class="col-xs-3 col-md-1 col-lg-1 social-btn-holder">
                            <a href="#" class="btn btn-social btn-block btn-linkedin">
                                <i class="fa fa-linkedin"></i> </a>
                        </div>

                    </div>
                </div>


            </div>

            <div class="col-xs-12 col-sm-8">

              <ul class="list-group">
                <li class="list-group-item">Name Example</li>
                <li class="list-group-item">Faculty</li>
                <li class="list-group-item">Department</li>
                  <li class="list-group-item"><i class="fa fa-suitcase"></i> Job Status</li>
                <li class="list-group-item"><i class="fa fa-phone"></i><a href="#" id="pno" data-type="text" data-placement="right" data-title="Enter username"> +94716330371</a> </li>
                <li class="list-group-item"><i class="fa fa-envelope"></i> <a href="#" id="email" data-type="text" data-placement="right" data-title="Enter username"> koki@gmail.com</a></li>
              </ul>-->
<table style="margin-top:3% ">
		<tr>
			<td  style="width: 27%;">
			<figure>
                 <img width="100%" class="img-thumbnail img-responsive" id="C_logo" alt="profile picture not loaded" src="{{URL::to('/')}}/img/ifs_logo.jpg">
            </figure>
			</td>

		</tr>
    <tr>
        <td>
            <h3 id="C_Name">IFS Sri Lanka</h3>
        </td>
    </tr>
</table>
</div>
</div>
</div>
</div>
      <div class="bs-callout bs-callout-danger">
        <h4>Comapny Details</h4>
        <p>
            IFS, the global enterprise applications company, provides software solutions that enable organizations
            to become more agile. Founded in 1983, IFS pioneered component-based enterprise resources planning (ERP)
            software with IFS Applications, now in its seventh generation. IFS' component architecture provides solutions
            that are easier to implement, run, and upgrade, and that give companies the flexibility to respond quickly to market changes,
            new business opportunities, and new technologies. IFS provides its single component-based software solution to targeted industries;
            our industry-focused solutions are optimized for ERP, EAM and MRO.
        </p>
        <p>


        </p>
      </div>
      <div class="bs-callout bs-callout-danger">
        <h4>News And Events</h4>
          <h4>IFS RECEIVES ORDER FROM GLOBAL TELECOMMUNICATIONS COMPANY</h4>
       <p>
           IFS, the global enterprise applications company, announces that Ericsson,
           the world’s leading provider of communications technology and services,
           has chosen IFS Metrix Service Management to support business-critical processes in its Hardware Services business unit.
           The contract includes services, software licenses and maintenance valued at approximately US$ 2 million.<br/>

          The IFS solution will support the organization’s global, end-to-end, hardware-related services,
           including both forward and reverse logistics. The contract, which pertains to the first part of the implementation project,
           also includes the deployment of IFS Metrix Service Management Mobile for Android (MMA).
        </p>

      </div>

      <div class="bs-callout bs-callout-danger ">
        <h4>Events Posted</h4>
        <ul class="list-group">
          <a class="list-group-item inactive-link" href="#">
            <h4 class="list-group-item-heading">
              Software Engineer at Twitter
            </h4>
            <p class="list-group-item-text">
              Lorem ipsum dolor sit amet, ea vel prima adhuc, scripta liberavisse ea quo, te vel vidit mollis complectitur. Quis verear mel ne. Munere vituperata vis cu,
         te pri duis timeam scaevola, nam postea diceret ne. Cum ex quod aliquip mediocritatem, mei habemus persecuti mediocritatem ei.
            </p>
          </a>
          <a class="list-group-item inactive-link" href="#">
            <h4 class="list-group-item-heading">Software Engineer at LinkedIn</h4>
            <p class="list-group-item-text">
              Lorem ipsum dolor sit amet, ea vel prima adhuc, scripta liberavisse ea quo, te vel vidit mollis complectitur.
              Quis verear mel ne. Munere vituperata vis cu, te pri duis timeam scaevola,
              nam postea diceret ne. Cum ex quod aliquip mediocritatem, mei habemus persecuti mediocritatem ei.
                </p><ul>
                  <li>
                 Lorem ipsum dolor sit amet, ea vel prima adhuc, scripta liberavisse ea quo, te vel vidit mollis complectitur.
              Quis verear mel ne. Munere vituperata vis cu, te pri duis timeam scaevola,
              nam postea diceret ne. Cum ex quod aliquip mediocritatem, mei habemus persecuti mediocritatem ei.
                  </li>
                  <li>
                 Lorem ipsum dolor sit amet, ea vel prima adhuc, scripta liberavisse ea quo, te vel vidit mollis complectitur.
              Quis verear mel ne. Munere vituperata vis cu, te pri duis timeam scaevola,
              nam postea diceret ne. Cum ex quod aliquip mediocritatem, mei habemus persecuti mediocritatem ei.
                  </li>
                </ul>
            <p></p>
          </a>
        </ul>
      </div>


          </a>

        </ul>
      </div>

    </div>

  </div>
</div>

</div>

</div>

