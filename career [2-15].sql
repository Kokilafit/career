-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 15, 2015 at 05:36 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `career`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `users_id` int(10) unsigned NOT NULL,
  `name` varchar(300) DEFAULT NULL,
  `telephone` varchar(15) DEFAULT NULL,
  `contact_email` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`users_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `industry`
--

CREATE TABLE IF NOT EXISTS `industry` (
  `comp_name` varchar(45) DEFAULT NULL,
  `website` varchar(45) DEFAULT NULL,
  `phone` varchar(45) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `phone_UNIQUE` (`phone`),
  UNIQUE KEY `website_UNIQUE` (`website`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `industry`
--

INSERT INTO `industry` (`comp_name`, `website`, `phone`, `user_id`) VALUES
('SLFIT', 'http://google.com', '943453453451', 49);

-- --------------------------------------------------------

--
-- Table structure for table `undergraduate`
--

CREATE TABLE IF NOT EXISTS `undergraduate` (
  `users_id` int(10) unsigned NOT NULL,
  `name` varchar(300) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `n_email` varchar(100) DEFAULT NULL,
  `entrance_year` int(11) DEFAULT NULL,
  `faculty` varchar(50) DEFAULT NULL,
  `department` varchar(100) DEFAULT NULL,
  `Linkedin` varchar(45) DEFAULT NULL,
  `googleplus` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`users_id`),
  KEY `fk_undergraduate_users1_idx` (`users_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `undergraduate`
--

INSERT INTO `undergraduate` (`users_id`, `name`, `dob`, `n_email`, `entrance_year`, `faculty`, `department`, `Linkedin`, `googleplus`) VALUES
(46, 'Kokila Alupotha', '1990-05-18', 'kokila.alu@gmail.com', 2015, 'ITfac', NULL, 'http://asdasd/asdasd/asda/sda/sd', 'http://agoogleasd.dfgh');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `p_email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `profile_pic` varchar(32) COLLATE utf8_unicode_ci DEFAULT '/img/profilepic.png',
  `confirmed` tinyint(1) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `confirmation_token` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`),
  UNIQUE KEY `users_p_email_unique` (`p_email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=50 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `p_email`, `role`, `profile_pic`, `confirmed`, `remember_token`, `confirmation_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', '$2y$10$oeVYcPSZG6/aAEJ2g9SB7Orli0WFTLn57MNzgmTxi9z3GVoQH8HHa', 'admin@uom.lk', 'adm', '/img/profilepic.png', 1, 'WfI4dWwx2NH7KD0YbZIr84XQCgppK9Ky6C5K4Uaou14Mmt86s5upUnPzIMcg', '', '2014-12-15 17:27:49', '2015-02-05 09:48:51'),
(19, '124137c', '$2y$10$dcq29Et5qKyPxsr5FzTOCeHp3h3jGQmgsbgIZOh2y11QrJLkceixe', '124137c@uom.lk', 'std', '/img/profilepic.png', 0, NULL, 'VRLAFsWl6vCtLSuVBaZGW0cigJJmV2lsGWKOiOGuTPPu77eIe1XQvlwD8XLK', '2014-12-15 17:17:46', '2014-12-15 17:17:46'),
(22, 'ifs', '$2y$10$oeVYcPSZG6/aAEJ2g9SB7Orli0WFTLn57MNzgmTxi9z3GVoQH8HHa', 'ifs@gmail.asd', 'ind', '/img/profilepic.png', 1, 'ZQiXXFam3gzi3VVhUPzYra9tkO3FlNHK8EiNie8SCZcjr95WPsmJPpGPT1GQ', '', '2014-12-15 17:27:49', '2015-02-05 09:48:51'),
(27, '123123s', '$2y$10$f/wgJOIcCRZhq0e3048Am.bg7tD/hAp.h/vTR3wS8TeqhQSbyZUG6', '123123s@uom.lk', 'std', '/img/profilepic.png', 0, NULL, 'OpRz8KzcVkn4M3H1ZtBm1iG9y8yei8pzr6xPGkYpoHaCZ8kmF1fmHDlCMxNL', '2015-01-12 15:23:46', '2015-01-12 15:23:46'),
(28, '123456s', '$2y$10$qk62mV7vV/kDuQKo7caX6ORvM7y0aUB/WHjaAUJuf.h1tUMEVD95.', '123456s@uom.lk', 'std', '/img/profilepic.png', 0, NULL, 'fuT6F9nNV6eAROMa9HFt787lupPmsnU99Ah6tMgcEoe4vGpPifVLEyqi8fAI', '2015-01-12 15:25:04', '2015-01-12 15:25:04'),
(29, '125sdfs', '$2y$10$EvfB2hL0D/x/Cd4evEM9v.YN2x/NWbSu8wESO4lxByJTPIzZe3sP.', '125sdfs@uom.lk', 'std', '/img/profilepic.png', 0, NULL, 'V0jXgCFQyqUTotZ93g0HKHzA6g3TkvEHvKd5zgHP9JTmu1l5ILvuWelgr0HY', '2015-01-21 00:42:45', '2015-01-21 00:42:45'),
(30, '124130a', '$2y$10$9QoV1bHg5fnV2ouh2IvlTuC0/AuGZCZd4DqSk3/acySXfal6t3E1i', '124130a@uom.lk', 'std', '/img/profilepic.png', 1, NULL, '', '2015-01-25 06:19:02', '2015-02-05 09:48:51'),
(46, '124008h', '$2y$10$4bl7c4.KxGoC/nqJBZvCgOUZb4uBeQsdKH.KTOtYJ84iWQEocbNuy', '124008h@uom.lk', 'std', '/uploads/sa0YJgou7uyK2.png', 1, 'TrSCuXICT4nOlfoXJyd5c96bgTSyMoAkPHwPOdAQfzArEKqGaqvVYtTSRDCf', 'ghjghjgh', '2015-02-05 22:19:56', '2015-02-09 06:16:11'),
(48, '124037h', '$2y$10$c7pPhz8ZAiRPEoHUvyiU8ev3TUL/N5TXT6l3DdZV8yX8ighAsMJWe', '124037h@uom.lk', 'std', '/img/profilepic.png', 0, NULL, '', '2015-02-05 22:34:47', '2015-02-05 22:34:47'),
(49, 'kokisoft', '$2y$10$ZBuCCMFOcJApPp3rqNImueEsJRCZAvGTpG16PhrXUsOjaHIjudv42', 'kokila.alu@gmail.com', 'ind', '/img/profilepic.png', 1, 'MHAdIkuyVhZmtWro3W7jEvcAm7gyM0JdeUgdsk4ilCeyV35KhfJkkuoyn7E8', '', '2015-02-06 01:10:54', '2015-02-09 06:10:35');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin`
--
ALTER TABLE `admin`
  ADD CONSTRAINT `fk_table1_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `industry`
--
ALTER TABLE `industry`
  ADD CONSTRAINT `fk_industry_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `undergraduate`
--
ALTER TABLE `undergraduate`
  ADD CONSTRAINT `fk_undergraduate_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
