-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 09, 2015 at 02:59 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `career_new`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `users_id` int(10) unsigned NOT NULL,
  `name` varchar(300) DEFAULT NULL,
  `telephone` varchar(15) DEFAULT NULL,
  `contact_email` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`users_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `backup`
--
CREATE TABLE IF NOT EXISTS `backup` (
`profile_pic` varchar(32)
,`user_id` int(10) unsigned
,`name` varchar(300)
,`dob` date
,`n_email` varchar(100)
,`entrance_year` int(4)
,`faculty` varchar(6)
,`department` varchar(6)
,`Linkedin` varchar(250)
,`googleplus` varchar(250)
,`result_id` int(11)
,`semester` int(11)
,`gpa` float
,`proj_id` int(11)
,`proj_title` varchar(75)
,`proj_descr` varchar(512)
,`members` varchar(150)
,`more_id` int(11)
,`summary` varchar(512)
,`marital_status` enum('S','M','D')
,`employe` enum('Y','N')
,`ex_id` int(11)
,`title` varchar(75)
,`descr` varchar(512)
,`skills_pool_id` int(11)
,`percentage` int(2)
);
-- --------------------------------------------------------

--
-- Table structure for table `experience`
--

CREATE TABLE IF NOT EXISTS `experience` (
  `ex_id` int(11) NOT NULL AUTO_INCREMENT,
  `undergraduate_id` int(10) unsigned NOT NULL,
  `title` varchar(75) DEFAULT NULL,
  `descr` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`ex_id`,`undergraduate_id`),
  KEY `fk_experience_undergraduate1` (`undergraduate_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `experience`
--

INSERT INTO `experience` (`ex_id`, `undergraduate_id`, `title`, `descr`) VALUES
(1, 50, 'BOOTSTRAP', 'THIS IS A JOKE');

-- --------------------------------------------------------

--
-- Table structure for table `industry`
--

CREATE TABLE IF NOT EXISTS `industry` (
  `users_id` int(10) unsigned NOT NULL,
  `comp_name` varchar(45) DEFAULT NULL,
  `website` varchar(45) DEFAULT NULL,
  `phone` varchar(45) NOT NULL,
  PRIMARY KEY (`users_id`),
  UNIQUE KEY `phone_UNIQUE` (`phone`),
  UNIQUE KEY `website_UNIQUE` (`website`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE IF NOT EXISTS `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_txt` varchar(160) DEFAULT NULL,
  `datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `sender_id` int(10) unsigned NOT NULL,
  `reciver_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`,`sender_id`,`reciver_id`),
  KEY `fk_message_user1_idx` (`sender_id`),
  KEY `fk_message_user2_idx` (`reciver_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `more_detail`
--

CREATE TABLE IF NOT EXISTS `more_detail` (
  `more_id` int(11) NOT NULL AUTO_INCREMENT,
  `undergraduate_id` int(10) unsigned NOT NULL,
  `summary` varchar(512) DEFAULT NULL,
  `marital_status` enum('S','M','D') DEFAULT NULL,
  `employe` enum('Y','N') DEFAULT NULL,
  PRIMARY KEY (`more_id`,`undergraduate_id`),
  KEY `fk_more_details_undergraduate1` (`undergraduate_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `more_detail`
--

INSERT INTO `more_detail` (`more_id`, `undergraduate_id`, `summary`, `marital_status`, `employe`) VALUES
(1, 50, 'ASD ASDASDA ASDASD ASD ASD ASDASD ', 'S', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE IF NOT EXISTS `project` (
  `proj_id` int(11) NOT NULL AUTO_INCREMENT,
  `undergraduate_id` int(10) unsigned NOT NULL,
  `proj_title` varchar(75) DEFAULT NULL,
  `proj_descr` varchar(512) DEFAULT NULL,
  `members` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`proj_id`,`undergraduate_id`),
  KEY `fk_project_undergraduate1_idx` (`undergraduate_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `result`
--

CREATE TABLE IF NOT EXISTS `result` (
  `result_id` int(11) NOT NULL AUTO_INCREMENT,
  `undergraduate_id` int(10) NOT NULL,
  `semester` int(11) NOT NULL,
  `gpa` float DEFAULT NULL,
  PRIMARY KEY (`result_id`,`undergraduate_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `result`
--

INSERT INTO `result` (`result_id`, `undergraduate_id`, `semester`, `gpa`) VALUES
(1, 50, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `skills`
--

CREATE TABLE IF NOT EXISTS `skills` (
  `undergraduate_id` int(10) unsigned NOT NULL,
  `skills_pool_id` int(11) NOT NULL,
  `percentage` int(2) DEFAULT '25',
  PRIMARY KEY (`undergraduate_id`,`skills_pool_id`),
  KEY `fk_skills_skills_pool1_idx` (`skills_pool_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `skills`
--

INSERT INTO `skills` (`undergraduate_id`, `skills_pool_id`, `percentage`) VALUES
(50, 1, 25);

-- --------------------------------------------------------

--
-- Table structure for table `skills_pool`
--

CREATE TABLE IF NOT EXISTS `skills_pool` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `skill` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `skills_pool`
--

INSERT INTO `skills_pool` (`id`, `skill`) VALUES
(1, 'PHP');

-- --------------------------------------------------------

--
-- Stand-in structure for view `students`
--
CREATE TABLE IF NOT EXISTS `students` (
`profile_pic` varchar(32)
,`user_id` int(10) unsigned
,`name` varchar(300)
,`dob` date
,`n_email` varchar(100)
,`entrance_year` int(4)
,`faculty` varchar(6)
,`department` varchar(6)
,`Linkedin` varchar(250)
,`googleplus` varchar(250)
,`result_id` int(11)
,`semester` int(11)
,`gpa` float
,`proj_id` int(11)
,`proj_title` varchar(75)
,`proj_descr` varchar(512)
,`members` varchar(150)
,`more_id` int(11)
,`summary` varchar(512)
,`marital_status` enum('S','M','D')
,`employe` enum('Y','N')
,`ex_id` int(11)
,`title` varchar(75)
,`descr` varchar(512)
,`skills_pool_id` int(11)
,`percentage` int(2)
);
-- --------------------------------------------------------

--
-- Table structure for table `undergraduate`
--

CREATE TABLE IF NOT EXISTS `undergraduate` (
  `user_id` int(10) unsigned NOT NULL,
  `name` varchar(300) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `n_email` varchar(100) DEFAULT NULL,
  `entrance_year` int(4) DEFAULT NULL,
  `faculty` varchar(6) DEFAULT NULL,
  `department` varchar(6) DEFAULT NULL,
  `Linkedin` varchar(250) DEFAULT NULL,
  `googleplus` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  KEY `fk_undergraduate_users1_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `undergraduate`
--

INSERT INTO `undergraduate` (`user_id`, `name`, `dob`, `n_email`, `entrance_year`, `faculty`, `department`, `Linkedin`, `googleplus`) VALUES
(50, 'kokila alupotha', '1990-05-18', 'kokila.alu@gmail.com', 2015, 'ITfac', NULL, 'http://upx.sourceforge.net/', 'http://upx.asdge.net/'),
(51, 'kokila alupotha', '1990-05-18', 'kokila@gmail.com', 2015, 'ITfac', NULL, 'http://upx.sourceforge.net/', 'http://upx.asdge.net/');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `p_email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `profile_pic` varchar(32) COLLATE utf8_unicode_ci DEFAULT '/img/profilepic.png',
  `confirmed` tinyint(1) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `confirmation_token` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`),
  UNIQUE KEY `users_p_email_unique` (`p_email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=54 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `p_email`, `role`, `profile_pic`, `confirmed`, `remember_token`, `confirmation_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', '$2y$10$oeVYcPSZG6/aAEJ2g9SB7Orli0WFTLn57MNzgmTxi9z3GVoQH8HHa', 'admin@uom.asd', 'adm', '/uploads/profilepic.png', 1, NULL, '', '2015-03-08 03:26:22', '2015-03-08 03:26:22'),
(50, '124008h', '$2y$10$oeVYcPSZG6/aAEJ2g9SB7Orli0WFTLn57MNzgmTxi9z3GVoQH8HHa', '124008h@uom.lk', 'std', '/uploads/sa0YJgou7uyK2.png', 1, NULL, '', '2015-03-08 03:26:22', '2015-03-08 03:26:22'),
(51, '124009h', '$2y$10$oeVYcPSZG6/aAEJ2g9SB7Orli0WFTLn57MNzgmTxi9z3GVoQH8HHa', '124009h@uom.lk', 'std', '/uploads/profilepic.png', 1, NULL, '', '2015-03-08 03:26:22', '2015-03-08 03:26:22'),
(52, '124137c', '$2y$10$oeVYcPSZG6/aAEJ2g9SB7Orli0WFTLn57MNzgmTxi9z3GVoQH8HHa', '124137c@uom.lk', 'std', '/uploads/profilepic.png', 1, NULL, '', '2015-03-08 03:26:22', '2015-03-08 03:26:22'),
(53, '124130z', '$2y$10$oeVYcPSZG6/aAEJ2g9SB7Orli0WFTLn57MNzgmTxi9z3GVoQH8HHa', '124130z@uom.lk', 'std', '/uploads/profilepic.png', 0, NULL, '', '2015-03-08 03:26:22', '2015-03-08 03:26:22');

-- --------------------------------------------------------

--
-- Structure for view `backup`
--
DROP TABLE IF EXISTS `backup`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `backup` AS select `u`.`profile_pic` AS `profile_pic`,`und`.`user_id` AS `user_id`,`und`.`name` AS `name`,`und`.`dob` AS `dob`,`und`.`n_email` AS `n_email`,`und`.`entrance_year` AS `entrance_year`,`und`.`faculty` AS `faculty`,`und`.`department` AS `department`,`und`.`Linkedin` AS `Linkedin`,`und`.`googleplus` AS `googleplus`,`result`.`result_id` AS `result_id`,`result`.`semester` AS `semester`,`result`.`gpa` AS `gpa`,`project`.`proj_id` AS `proj_id`,`project`.`proj_title` AS `proj_title`,`project`.`proj_descr` AS `proj_descr`,`project`.`members` AS `members`,`more`.`more_id` AS `more_id`,`more`.`summary` AS `summary`,`more`.`marital_status` AS `marital_status`,`more`.`employe` AS `employe`,`exper`.`ex_id` AS `ex_id`,`exper`.`title` AS `title`,`exper`.`descr` AS `descr`,`skill`.`skills_pool_id` AS `skills_pool_id`,`skill`.`percentage` AS `percentage` from ((((((`user` `u` join `undergraduate` `und` on((`u`.`id` = `und`.`user_id`))) join `result` on((`und`.`user_id` = `result`.`undergraduate_id`))) join `more_detail` `more` on((`und`.`user_id` = `more`.`undergraduate_id`))) join `project` on((`und`.`user_id` = `project`.`undergraduate_id`))) join `experience` `exper` on((`und`.`user_id` = `exper`.`undergraduate_id`))) join `skills` `skill` on((`und`.`user_id` = `skill`.`undergraduate_id`))) where ((`u`.`role` = 'std') and (`u`.`confirmed` = 1));

-- --------------------------------------------------------

--
-- Structure for view `students`
--
DROP TABLE IF EXISTS `students`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `students` AS select `u`.`profile_pic` AS `profile_pic`,`und`.`user_id` AS `user_id`,`und`.`name` AS `name`,`und`.`dob` AS `dob`,`und`.`n_email` AS `n_email`,`und`.`entrance_year` AS `entrance_year`,`und`.`faculty` AS `faculty`,`und`.`department` AS `department`,`und`.`Linkedin` AS `Linkedin`,`und`.`googleplus` AS `googleplus`,`result`.`result_id` AS `result_id`,`result`.`semester` AS `semester`,`result`.`gpa` AS `gpa`,`project`.`proj_id` AS `proj_id`,`project`.`proj_title` AS `proj_title`,`project`.`proj_descr` AS `proj_descr`,`project`.`members` AS `members`,`more`.`more_id` AS `more_id`,`more`.`summary` AS `summary`,`more`.`marital_status` AS `marital_status`,`more`.`employe` AS `employe`,`exper`.`ex_id` AS `ex_id`,`exper`.`title` AS `title`,`exper`.`descr` AS `descr`,`skill`.`skills_pool_id` AS `skills_pool_id`,`skill`.`percentage` AS `percentage` from ((((((`user` `u` left join `undergraduate` `und` on((`u`.`id` = `und`.`user_id`))) left join `result` on((`u`.`id` = `result`.`undergraduate_id`))) left join `more_detail` `more` on((`u`.`id` = `more`.`undergraduate_id`))) left join `project` on((`u`.`id` = `project`.`undergraduate_id`))) left join `experience` `exper` on((`u`.`id` = `exper`.`undergraduate_id`))) left join `skills` `skill` on((`u`.`id` = `skill`.`undergraduate_id`))) where ((`u`.`role` = 'std') and (`u`.`confirmed` = 1));

--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin`
--
ALTER TABLE `admin`
  ADD CONSTRAINT `fk_table1_users1` FOREIGN KEY (`users_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `experience`
--
ALTER TABLE `experience`
  ADD CONSTRAINT `fk_experience_undergraduate1` FOREIGN KEY (`undergraduate_id`) REFERENCES `undergraduate` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `industry`
--
ALTER TABLE `industry`
  ADD CONSTRAINT `fk_industry_users1` FOREIGN KEY (`users_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `fk_message_user1` FOREIGN KEY (`sender_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_message_user2` FOREIGN KEY (`reciver_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `more_detail`
--
ALTER TABLE `more_detail`
  ADD CONSTRAINT `fk_more_details_undergraduate1` FOREIGN KEY (`undergraduate_id`) REFERENCES `undergraduate` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `project`
--
ALTER TABLE `project`
  ADD CONSTRAINT `fk_project_undergraduate1` FOREIGN KEY (`undergraduate_id`) REFERENCES `undergraduate` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `skills`
--
ALTER TABLE `skills`
  ADD CONSTRAINT `fk_skills_skills_pool1` FOREIGN KEY (`skills_pool_id`) REFERENCES `skills_pool` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_skills_undergraduate1` FOREIGN KEY (`undergraduate_id`) REFERENCES `undergraduate` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `undergraduate`
--
ALTER TABLE `undergraduate`
  ADD CONSTRAINT `fk_undergraduate_users1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
