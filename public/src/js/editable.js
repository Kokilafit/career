/**
 * Created by alu on 12/13/2014.
 */

$(document).ready(function() {
    //toggle `popup` / `inline` mode
    $.fn.editable.defaults.disabled = false;


    $.fn.editable.defaults.mode = 'inline';
    $.fn.editable.defaults.url = '../../profile_update';
    $.fn.editable.defaults.success = function(response, newValue) {
            if(!response) {
                return null;
            }
            if(response.success === false) {
                return response.msg;
            }
        }
    $.fn.editable.defaults.send = 'always';
    $('#name').editable({type:'text' });

    $fac_name = $('#faculty');
    $fac_name.editable({
        source: [
            {value:'Efac' , text: 'Faculty of Engineering'},
            {value:'ITfac' , text: 'Faculty of Information Technology'},
            {value:'Afac' , text: 'Faculty of Architecture'}
        ]
    });






    $('#department').editable({
        source: function() {
            console.log($fac_name.text());
            if($fac_name.text()==='Faculty of Architecture') {
                return [
                    {value:'DArc' , text: 'Department of Architecture'},
                    {value:'DID' , text: 'Department of Integrated Design'},
                    {value:'DBE' , text: 'Department of Building Economics'},
                    {value:'DTCP' , text: 'Department of Town & Country Planning'}
                ]
            }
            else if($fac_name.text()=='Faculty of Engineering') {
                return [
                    {value:'DCPE' , text: 'Department of Chemical & Process Engineering'},
                    {value:'DCE' , text: 'Civil Engineering'},
                    {value:'DCSE' , text: 'Department of computer Science & Engineering'},
                    {value:'DER' , text: 'Earth Resource Engineering'},
                    {value:'DTEE' , text: 'Electrical Engineering'},
                    {value:'DENTC' , text: 'Electronic & Telecommunication Engineering'},
                    {value:'DMSE' , text: 'Material Science & Engineering'},
                    {value:'DMech' , text: 'Mechanical Engineering'},
                    {value:'DCT' , text: 'Textile & Clothing Technology'},
                    {value:'DTLM' , text: 'Transport & Logistics Management'}
                ]
            }
            else {
                return [
                    {value:'' , text: 'none'}
                ]
            }
        }
    });


    $('#mobile_no').editable({type:'text'});
    $('#n_email').editable({type:'text' });
    $('#summary').editable({
        type:'textarea',
        showbuttons: 'bottom',
        rows: 5
    });


    //$('#title').editable({type:'text'});
    $('.descr').editable({
        type:'textarea',
        showbuttons: 'bottom',
        rows: 5
    });

    $('.title').editable({
        type:'textarea',
        showbuttons: 'bottom',
        rows: 1
    });


    $('.descr_add').editable({
        send: false,
        type:'textarea',
        showbuttons: false,
        rows: 5
    });

    $('.title_add').editable({
        type:'textarea',
        showbuttons: false,
        rows: 1
    });




    $('#Linkedin').editable({type:'text' });
    $('#googleplus').editable({type:'text' });
    $('#titleNo').editable({type:'text' });



    $('#dob').editable({
        format: 'yyyy-mm-dd',
        viewformat: 'dd/mm/yyyy',
        datepicker: {
            weekStart: 1
        }
    });
    $('#marital_status').editable({
        value:'S',
        source: [
            {value: 'S', text: 'Single'},
            {value: 'M', text: 'Married '},
            {value: 'D', text: 'Divorced'}
        ]
    });


    $('#status').editable({
        value: 1,
        source: [
            {value: 1, text: 'Faculty of Information Technology'},
            {value: 2, text: 'Blocked'},
            {value: 3, text: 'Deleted'}
        ]
    });

    var as = $("#editbut").click(function() {

        $('.editable').editable('toggleDisabled');
    });
});

