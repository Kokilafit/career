 $(document).ready(function() {
        $('#std_form').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                usernamesss: {
                    validators: {
                        notEmpty: {
                            message: 'The index no is required and cannot be empty'
                        },
                        remote: {
                            message: 'The index no is already registered one',
                            url: '../../checkemail/index'
                        }
                    }
                },
                password: {
                    validators: {
                        notEmpty: {
                            message: 'The password is required and cannot be empty'
                        },
                        identical: {
                            field: 'conf_password',
                            message: 'The password and its confirm are not the same'
                        },
                        stringLength: {
                            min: 6,
                            max: 30,
                            message: 'The password must be more than 6 and less than 30 characters long'
                    }
                    }
                },
                password_confirmation: {
                    validators: {
                        notEmpty: {
                            message: 'The password is required and cannot be empty'
                        },
                        identical: {
                            field: 'password',
                            message: 'The password and its confirm are not the same'
                        }
                    }
                },
                'agreement[]': {
                    validators: {
                        choice: {
                            min: 1,
                            max: 1,
                            message: 'you should agree to Career-Plus terms for registering'
                        }
                    }
                }

            }
        });
    });

