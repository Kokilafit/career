-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: May 25, 2015 at 02:49 PM
-- Server version: 5.6.19-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `career_new`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `GetGPA`(IN asd float)
BEGIN
 SELECT * 
 FROM students
 WHERE gpa >= asd;
 END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `users_id` int(10) unsigned NOT NULL,
  `name` varchar(300) DEFAULT NULL,
  `telephone` varchar(15) DEFAULT NULL,
  `contact_email` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`users_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`users_id`, `name`, `telephone`, `contact_email`) VALUES
(1, 'Careerplus Admin', '0712232332', 'careerplus@uom.lk');

-- --------------------------------------------------------

--
-- Table structure for table `alumni`
--

CREATE TABLE IF NOT EXISTS `alumni` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(100) NOT NULL,
  `faculty` varchar(5) NOT NULL,
  `dep` varchar(7) DEFAULT NULL,
  `g_year` year(4) NOT NULL,
  `phone_no` int(11) DEFAULT NULL,
  `linkedin` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `alumni`
--

INSERT INTO `alumni` (`id`, `name`, `faculty`, `dep`, `g_year`, `phone_no`, `linkedin`) VALUES
(79, 'Janath Thilakasiri', 'ITfac', NULL, 2015, 716334545, 'linkedin.com'),
(85, 'Sri qwe qwe', 'ITfac', NULL, 2015, 716334545, 'linkedin.com/asd');

-- --------------------------------------------------------

--
-- Stand-in structure for view `backup`
--
CREATE TABLE IF NOT EXISTS `backup` (
`profile_pic` varchar(100)
,`user_id` int(10) unsigned
,`name` varchar(300)
,`dob` date
,`n_email` varchar(100)
,`entrance_year` int(4)
,`faculty` varchar(6)
,`department` varchar(6)
,`Linkedin` varchar(250)
,`googleplus` varchar(250)
,`result_id` int(11)
,`semester` int(11)
,`gpa` float
,`proj_id` int(11)
,`proj_title` varchar(75)
,`proj_descr` varchar(512)
,`members` varchar(150)
,`more_id` int(11)
,`summary` varchar(512)
,`marital_status` enum('S','M','D')
,`employe` enum('Y','N')
,`ex_id` int(11)
,`title` varchar(75)
,`descr` varchar(512)
,`skills_pool_id` int(11)
,`percentage` int(2)
);
-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE IF NOT EXISTS `employee` (
  `id` int(10) unsigned NOT NULL,
  `workplace` varchar(150) DEFAULT NULL,
  `join_year` year(4) DEFAULT NULL,
  `designation` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id`, `workplace`, `join_year`, `designation`) VALUES
(79, 'Workplace test', 2009, 'Designatio test');

-- --------------------------------------------------------

--
-- Table structure for table `experience`
--

CREATE TABLE IF NOT EXISTS `experience` (
  `ex_id` int(11) NOT NULL AUTO_INCREMENT,
  `undergraduate_id` int(10) unsigned NOT NULL,
  `title` varchar(75) DEFAULT NULL,
  `descr` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`ex_id`,`undergraduate_id`),
  KEY `fk_experience_undergraduate1` (`undergraduate_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `experience`
--

INSERT INTO `experience` (`ex_id`, `undergraduate_id`, `title`, `descr`) VALUES
(1, 50, 'BOOTSTRAP', 'THIS IS A JOKE'),
(8, 65, 'BOOTSTRAP', 'THIS IS A JOKE'),
(9, 66, 'BOOTSTRAP', 'THIS IS A JOKE'),
(10, 67, 'BOOTSTRAP', 'THIS IS A JOKE'),
(11, 68, 'BOOTSTRAP', 'THIS IS A JOKE'),
(12, 69, 'BOOTSTRAP', 'THIS IS A JOKE'),
(13, 70, 'BOOTSTRAP', 'THIS IS A JOKE'),
(14, 71, 'BOOTSTRAP', 'THIS IS A JOKE'),
(15, 72, 'BOOTSTRAP', 'THIS IS A JOKE'),
(16, 73, 'BOOTSTRAP', 'THIS IS A JOKE'),
(17, 74, 'BOOTSTRAP', 'THIS IS A JOKE'),
(18, 75, 'BOOTSTRAP', 'THIS IS A JOKE'),
(19, 76, 'BOOTSTRAP', 'THIS IS A JOKE'),
(20, 77, 'BOOTSTRAP', 'THIS IS A JOKE'),
(21, 78, 'BOOTSTRAP', 'THIS IS A JOKE'),
(23, 78, 'asdasd', 'asdasssssssssssssssssss'),
(24, 58, 'test title 122 sdfsdfsdf', 'Elephants are large mammals of the family Elephantidae and the order Proboscidea. Two species are traditionally recognised, the African elephant (Loxodonta africana) and the Asian elephant (Elephas maximus), although some evidence suggests that African bush '),
(25, 58, 'test title 1', 'desc 1'),
(26, 58, 'test title 1', 'desc 1'),
(27, 58, 'test title 122', NULL),
(28, 58, 'test title 111', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `friends`
--

CREATE TABLE IF NOT EXISTS `friends` (
  `requester` int(10) unsigned NOT NULL,
  `acceptor` int(10) unsigned NOT NULL,
  `state` tinyint(1) NOT NULL,
  PRIMARY KEY (`requester`,`acceptor`),
  KEY `acceptor` (`acceptor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `friends`
--

INSERT INTO `friends` (`requester`, `acceptor`, `state`) VALUES
(58, 50, 0),
(58, 51, 0),
(58, 58, 0),
(58, 60, 0),
(58, 61, 0),
(58, 62, 1),
(58, 63, 1),
(58, 64, 0),
(58, 65, 1),
(58, 66, 0),
(58, 70, 0),
(58, 71, 0),
(58, 73, 0),
(58, 74, 1),
(58, 75, 0),
(58, 77, 1),
(58, 78, 1);

-- --------------------------------------------------------

--
-- Table structure for table `industry`
--

CREATE TABLE IF NOT EXISTS `industry` (
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `comp_name` varchar(45) DEFAULT NULL,
  `website` varchar(45) DEFAULT NULL,
  `phone` varchar(45) NOT NULL,
  `about` varchar(500) DEFAULT NULL,
  `linkedin` varchar(200) DEFAULT NULL,
  `twitter` varchar(200) DEFAULT NULL,
  `cover_pic` varchar(200) NOT NULL DEFAULT 'cover.png',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `phone_UNIQUE` (`phone`),
  UNIQUE KEY `website_UNIQUE` (`website`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `industry`
--

INSERT INTO `industry` (`user_id`, `comp_name`, `website`, `phone`, `about`, `linkedin`, `twitter`, `cover_pic`) VALUES
(84, 'CodeJen pvt ltd', 'asdqwe.com', '1234567890', '<p>this is <strong>test asd</strong> asd</p>\r\n', 'linkedin.com/asda', 'asdasdTwitter', 'cover.png');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `sender` int(11) NOT NULL,
  `reciver` int(11) NOT NULL,
  `message` varchar(200) NOT NULL,
  `m_id` int(11) NOT NULL AUTO_INCREMENT,
  `time_stamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`m_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`sender`, `reciver`, `message`, `m_id`, `time_stamp`) VALUES
(58, 60, 'ggvctfccdrctvtvy', 1, '2015-05-24 10:50:54'),
(58, 60, 'vfvgf', 2, '2015-05-24 11:18:50'),
(58, 50, 'fjhfghfhg ghfghf', 3, '2015-05-24 15:37:17'),
(58, 58, 'asdasdasd', 4, '2015-05-25 13:53:25'),
(50, 58, 'This is test msg 2', 5, '2015-05-25 14:09:02');

-- --------------------------------------------------------

--
-- Table structure for table `more_detail`
--

CREATE TABLE IF NOT EXISTS `more_detail` (
  `more_id` int(11) NOT NULL AUTO_INCREMENT,
  `undergraduate_id` int(10) unsigned NOT NULL,
  `summary` varchar(512) DEFAULT NULL,
  `marital_status` enum('S','M','D') DEFAULT NULL,
  `employe` enum('Y','N') DEFAULT NULL,
  PRIMARY KEY (`more_id`,`undergraduate_id`),
  KEY `fk_more_details_undergraduate1` (`undergraduate_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `more_detail`
--

INSERT INTO `more_detail` (`more_id`, `undergraduate_id`, `summary`, `marital_status`, `employe`) VALUES
(1, 50, 'ASD ASDASDA ASDASD ASD ASD ASDASD ', 'S', 'N'),
(3, 60, 'ASD ASDASDA ASDASD ASD ASD ASDASD ', 'S', 'N'),
(4, 61, 'ASD ASDASDA ASDASD ASD ASD ASDASD ', 'S', 'N'),
(5, 62, 'ASD ASDASDA ASDASD ASD ASD ASDASD ', 'S', 'N'),
(6, 63, 'ASD ASDASDA ASDASD ASD ASD ASDASD ', 'S', 'N'),
(7, 64, 'ASD ASDASDA ASDASD ASD ASD ASDASD ', 'S', 'N'),
(8, 65, 'ASD ASDASDA ASDASD ASD ASD ASDASD ', 'S', 'N'),
(9, 66, 'ASD ASDASDA ASDASD ASD ASD ASDASD ', 'S', 'N'),
(10, 67, 'ASD ASDASDA ASDASD ASD ASD ASDASD ', 'S', 'N'),
(11, 68, 'ASD ASDASDA ASDASD ASD ASD ASDASD ', 'S', 'N'),
(12, 69, 'ASD ASDASDA ASDASD ASD ASD ASDASD ', 'S', 'N'),
(13, 70, 'ASD ASDASDA ASDASD ASD ASD ASDASD ', 'S', 'N'),
(14, 71, 'ASD ASDASDA ASDASD ASD ASD ASDASD ', 'S', 'N'),
(15, 72, 'ASD ASDASDA ASDASD ASD ASD ASDASD ', 'S', 'N'),
(16, 73, 'ASD ASDASDA ASDASD ASD ASD ASDASD ', 'S', 'N'),
(17, 74, 'ASD ASDASDA ASDASD ASD ASD ASDASD ', 'S', 'N'),
(18, 75, 'ASD ASDASDA ASDASD ASD ASD ASDASD ', 'S', 'N'),
(19, 76, 'ASD ASDASDA ASDASD ASD ASD ASDASD ', 'S', 'N'),
(20, 77, 'ASD ASDASDA ASDASD ASD ASD ASDASD ', 'S', 'N'),
(21, 78, 'ASD ASDASDA ASDASD ASD ASD ASDASD ', 'S', 'N'),
(22, 58, 'asd asdasd asdas dasd asd sdasdasdasd', 'S', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE IF NOT EXISTS `post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(10) unsigned NOT NULL,
  `title` varchar(75) NOT NULL,
  `desc` varchar(500) NOT NULL,
  `img` varchar(100) DEFAULT NULL,
  `cat` varchar(10) DEFAULT NULL,
  `posted_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id`, `uid`, `title`, `desc`, `img`, `cat`, `posted_time`) VALUES
(7, 79, 'IFS partners with University of Moratuwa to spark innovation and entreprene', 'IFS announced its partnership with the University of Moratuwa to establish an IFS Endowed Chair for Innovation and Entrepreneurship at the University of Moratuwa, at a press briefing held in Colombo. An agreement establishing this was signed between IFS and the University of Moratuwa in the presence of dignitaries and members of the media.', '7955611cf666e759.16168088.png', 'alm-post', '2015-05-24 06:06:06'),
(8, 79, 'dfghfgh', 'fghfghfg fghfghfgh', NULL, 'alm-post', '2015-05-24 06:35:27'),
(9, 79, 'dfghfgh', 'fghfghfg fghfghfgh', NULL, 'alm-post', '2015-05-24 06:35:47'),
(10, 79, 'jkljkhh gkhkghkghjk', ' ghkgjkgjkg hk gjk gjhkg hjkg', NULL, 'alm-post', '2015-05-24 06:36:05'),
(11, 79, 'asd asdasdasd', 'IFS announced its partnership with the University of Moratuwa to establish an IFS Endowed Chair for Innovation and Entrepreneurship at the University of Moratuwa, at a press briefing held in Colombo. An agreement establishing thi', NULL, 'alm-post', '2015-05-24 06:36:50'),
(12, 79, 'asd asdasdasd', 'IFS announced its partnership with the University of Moratuwa to establish an IFS Endowed Chair for Innovation and Entrepreneurship at the University of Moratuwa, at a press briefing held in Colombo. An agreement establishing thi', NULL, 'alm-post', '2015-05-24 06:38:37'),
(13, 79, 'this is new test post', 'nership with the University of Moratuwa to establish an IFS Endowed Chair for Innovation and Entrepreneurship at the University of Moratuwa, at a press briefing held in Colombo. An agreement establishing thi\r\n', '79556124ceedc5e6.21123872.png', 'alm-post', '2015-05-24 06:39:34'),
(14, 79, 'sdfsdf', 'sdfsdfsdf sdfsdfs dfsdf', NULL, 'alm-post', '2015-05-24 06:57:24'),
(15, 1, 'asdasdasd', 'asdasd asda sd asdasdasdassd', NULL, 'adm-event', '2015-05-24 07:41:42'),
(16, 1, 'asdasdasd', 'asdasd asda sd asdasdasdassd', NULL, 'adm-event', '2015-05-24 07:42:41'),
(17, 1, 'asdasdasd', 'asdasd asda sd asdasdasdassd', NULL, 'adm-event', '2015-05-24 07:46:47'),
(18, 1, 'test with img', 'this is test event post with image ', '1556134b5c73a92.40538131.png', 'adm-event', '2015-05-24 07:47:25'),
(19, 79, 'We are going to conduct asda ', 'ip with the University of Moratuwa to establish an IFS Endowed Chair for Innovation and Entrepreneurship at the University of Moratuwa, at a press briefing held in Colombo. An agreement establishing this was signed between IFS and th', '79556143b2c42a01.28195564.png', 'alm-post', '2015-05-24 08:51:22'),
(20, 1, 'This is test event', 'This is not asdasd asdasd asdtestg', '15562b6763d7874.51567081.png', 'adm-event', '2015-05-25 11:13:18'),
(21, 84, 'this is test job for SE', 'more tests follow ubuntu documentation on master mode. The setup is divided ... Please make sure there are no trailing white space and the end of the line! Hostapd is very ... Then start the hostapd service using the following command,.', '845562e47e7bacc8.16081135.png', 'ind-job', '2015-05-25 14:29:42');

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE IF NOT EXISTS `project` (
  `proj_id` int(11) NOT NULL AUTO_INCREMENT,
  `undergraduate_id` int(10) unsigned NOT NULL,
  `proj_title` varchar(75) DEFAULT NULL,
  `proj_descr` varchar(512) DEFAULT NULL,
  `members` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`proj_id`,`undergraduate_id`),
  KEY `fk_project_undergraduate1_idx` (`undergraduate_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`proj_id`, `undergraduate_id`, `proj_title`, `proj_descr`, `members`) VALUES
(1, 50, 'google glas', 'this is a project description', '50,60,59'),
(3, 60, 'google glas', 'this is a project description', '50,60,59'),
(4, 61, 'google glas', 'this is a project description', '50,60,59'),
(5, 62, 'google glas', 'this is a project description', '50,60,59'),
(6, 63, 'google glas', 'this is a project description', '50,60,59'),
(7, 64, 'google glas', 'this is a project description', '50,60,59'),
(8, 65, 'google glas', 'this is a project description', '50,60,59'),
(9, 66, 'google glas', 'this is a project description', '50,60,59'),
(10, 67, 'google glas', 'this is a project description', '50,60,59'),
(11, 68, 'google glas', 'this is a project description', '50,60,59'),
(12, 69, 'google glas', 'this is a project description', '50,60,59'),
(13, 70, 'google glas', 'this is a project description', '50,60,59'),
(14, 71, 'google glas', 'this is a project description', '50,60,59'),
(15, 72, 'google glas', 'this is a project description', '50,60,59'),
(16, 73, 'google glas', 'this is a project description', '50,60,59'),
(17, 74, 'google glas', 'this is a project description', '50,60,59'),
(18, 75, 'google glas', 'this is a project description', '50,60,59'),
(19, 76, 'google glas', 'this is a project description', '50,60,59'),
(20, 77, 'google glas', 'this is a project description', '50,60,59'),
(21, 78, 'google glas', 'this is a project description', '50,60,59');

-- --------------------------------------------------------

--
-- Table structure for table `result`
--

CREATE TABLE IF NOT EXISTS `result` (
  `result_id` int(11) NOT NULL AUTO_INCREMENT,
  `undergraduate_id` int(10) NOT NULL,
  `semester` int(11) NOT NULL,
  `gpa` float DEFAULT NULL,
  PRIMARY KEY (`result_id`,`undergraduate_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `result`
--

INSERT INTO `result` (`result_id`, `undergraduate_id`, `semester`, `gpa`) VALUES
(1, 50, 1, 2.75),
(3, 60, 1, 2.5),
(4, 61, 1, 2.5),
(5, 62, 1, 2.5),
(6, 63, 1, 2.5),
(7, 64, 1, 3.7),
(8, 65, 1, 3.7),
(9, 66, 1, 3.7),
(10, 67, 1, 3.7),
(11, 68, 1, 3.7),
(11, 69, 1, 3.4),
(12, 70, 1, 3.4),
(13, 71, 1, 3.4),
(14, 72, 1, 3.4),
(15, 73, 1, 3),
(16, 74, 1, 3.2),
(17, 75, 1, 3.1),
(18, 76, 1, 3.2),
(19, 77, 1, 3.2),
(20, 78, 1, 3.3),
(21, 78, 2, 2.5),
(22, 78, 3, 2.4),
(23, 66, 2, 3.9),
(24, 50, 2, 3.5),
(25, 50, 3, 3.7);

--
-- Triggers `result`
--
DROP TRIGGER IF EXISTS `ogpa_calc`;
DELIMITER //
CREATE TRIGGER `ogpa_calc` AFTER UPDATE ON `result`
 FOR EACH ROW UPDATE undergraduate
SET ogpa=(select sum(gpa)/count(*) from result
where undergraduate_id=NEW.undergraduate_id GROUP BY undergraduate_id)
WHERE user_id=NEW.undergraduate_id
//
DELIMITER ;
DROP TRIGGER IF EXISTS `ogpa_calc2`;
DELIMITER //
CREATE TRIGGER `ogpa_calc2` AFTER INSERT ON `result`
 FOR EACH ROW UPDATE undergraduate
SET ogpa=(select sum(gpa)/count(*) from result
where undergraduate_id=NEW.undergraduate_id GROUP BY undergraduate_id)
WHERE user_id=NEW.undergraduate_id
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `skills`
--

CREATE TABLE IF NOT EXISTS `skills` (
  `undergraduate_id` int(10) unsigned NOT NULL,
  `skills_pool_id` int(11) NOT NULL,
  `percentage` int(2) DEFAULT '25',
  PRIMARY KEY (`undergraduate_id`,`skills_pool_id`),
  KEY `fk_skills_skills_pool1_idx` (`skills_pool_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `skills`
--

INSERT INTO `skills` (`undergraduate_id`, `skills_pool_id`, `percentage`) VALUES
(50, 1, 25),
(50, 3, 25),
(58, 1, 25),
(60, 1, 25),
(60, 2, 25),
(60, 3, 25),
(61, 2, 26),
(62, 3, 27),
(63, 4, 28),
(64, 5, 29),
(65, 6, 30),
(66, 7, 31),
(67, 8, 32),
(68, 9, 33),
(69, 1, 34),
(69, 3, 35),
(70, 2, 35),
(71, 3, 36),
(72, 4, 37),
(73, 5, 38),
(74, 6, 39),
(75, 7, 40),
(76, 8, 41),
(77, 9, 42),
(78, 2, 25),
(78, 3, 43);

-- --------------------------------------------------------

--
-- Table structure for table `skills_pool`
--

CREATE TABLE IF NOT EXISTS `skills_pool` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `skill` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `skills_pool`
--

INSERT INTO `skills_pool` (`id`, `skill`) VALUES
(1, 'PHP'),
(2, 'JAVA'),
(3, 'VB'),
(4, 'C#'),
(5, 'JS'),
(6, 'SQL'),
(7, 'HTML'),
(8, 'CSS'),
(9, 'SINGING');

-- --------------------------------------------------------

--
-- Stand-in structure for view `students`
--
CREATE TABLE IF NOT EXISTS `students` (
`profile_pic` varchar(100)
,`user_id` int(10) unsigned
,`name` varchar(300)
,`dob` date
,`n_email` varchar(100)
,`entrance_year` int(4)
,`faculty` varchar(6)
,`department` varchar(6)
,`ogpa` float
,`Linkedin` varchar(250)
,`googleplus` varchar(250)
,`result_id` int(11)
,`semester` int(11)
,`gpa` float
,`proj_id` int(11)
,`proj_title` varchar(75)
,`proj_descr` varchar(512)
,`members` varchar(150)
,`more_id` int(11)
,`summary` varchar(512)
,`marital_status` enum('S','M','D')
,`employe` enum('Y','N')
,`ex_id` int(11)
,`title` varchar(75)
,`descr` varchar(512)
,`skill_uid` int(10) unsigned
,`skills_pool_id` int(11)
,`percentage` int(2)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `studentsnew`
--
CREATE TABLE IF NOT EXISTS `studentsnew` (
`profile_pic` varchar(100)
,`user_id` int(10) unsigned
,`name` varchar(300)
,`dob` date
,`n_email` varchar(100)
,`entrance_year` int(4)
,`faculty` varchar(6)
,`department` varchar(6)
,`ogpa` float
,`Linkedin` varchar(250)
,`googleplus` varchar(250)
,`result_uid` int(10)
,`result_id` int(11)
,`semester` int(11)
,`gpa` float
,`proj_uid` int(10) unsigned
,`proj_id` int(11)
,`proj_title` varchar(75)
,`proj_descr` varchar(512)
,`members` varchar(150)
,`more_uid` int(10) unsigned
,`more_id` int(11)
,`summary` varchar(512)
,`marital_status` enum('S','M','D')
,`employe` enum('Y','N')
,`exp_uid` int(10) unsigned
,`ex_id` int(11)
,`title` varchar(75)
,`descr` varchar(512)
,`skill_uid` int(10) unsigned
,`skills_pool_id` int(11)
,`percentage` int(2)
);
-- --------------------------------------------------------

--
-- Table structure for table `undergraduate`
--

CREATE TABLE IF NOT EXISTS `undergraduate` (
  `user_id` int(10) unsigned NOT NULL,
  `name` varchar(300) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `n_email` varchar(100) DEFAULT NULL,
  `entrance_year` int(4) DEFAULT NULL,
  `faculty` varchar(6) DEFAULT NULL,
  `department` varchar(6) DEFAULT NULL,
  `ogpa` float NOT NULL,
  `Linkedin` varchar(250) DEFAULT NULL,
  `googleplus` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  KEY `fk_undergraduate_users1_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `undergraduate`
--

INSERT INTO `undergraduate` (`user_id`, `name`, `dob`, `n_email`, `entrance_year`, `faculty`, `department`, `ogpa`, `Linkedin`, `googleplus`) VALUES
(50, 'kokila alupotha', '1990-05-18', 'kokila.alu@gmail.com', 2015, 'ITfac', NULL, 3.31667, 'http://upx.sourceforge.net/', 'http://upx.asdge.net/'),
(51, 'kokila alupotha', '1990-05-18', 'kokila@gmail.com', 2015, 'ITfac', NULL, 0, 'http://upx.sourceforge.net/', 'http://upx.asdge.net/'),
(58, 'John Doe', '1990-05-18', 'kokila.alu@gmail.com', 2015, 'ITfac', '', 0, 'http://lk.linkedin.com/pub/kokila-alupotha/72/292/696/', 'https://plus.google.com/kokilaalupotha/posts'),
(60, 'Anjula', '1990-05-10', 'kokila.Lorem10@gmail.com', 2015, 'ITfac', 'DCE', 2.5, 'http://lk.linkedin.com/pub/kokila-Lorem10/72/292/696/', 'https://plus.google.com/Lorem10/posts'),
(61, 'Ridma gimhani', '1990-05-11', 'kokila.Ipsum11@gmail.com', 2015, 'ITfac', 'DCE', 2.5, 'http://lk.linkedin.com/pub/kokila-Ipsum11/72/292/696/', 'https://plus.google.com/Ipsum11/posts'),
(62, 'Chamini kumarasinghe', '1990-05-12', 'kokila.Dolor12@gmail.com', 2015, 'ITfac', 'DCE', 2.5, 'http://lk.linkedin.com/pub/kokila-Dolor12/72/292/696/', 'https://plus.google.com/Dolor12/posts'),
(63, 'Samitha Jayaweera', '1990-05-13', 'kokila.Lorem13@gmail.com', 2015, 'ITfac', 'DCE', 2.5, 'http://lk.linkedin.com/pub/kokila-Lorem13/72/292/696/', 'https://plus.google.com/Lorem13/posts'),
(64, 'kokila Ipsum14', '1990-05-14', 'kokila.Ipsum14@gmail.com', 2015, 'Efac', 'DCE', 3.7, 'http://lk.linkedin.com/pub/kokila-Ipsum14/72/292/696/', 'https://plus.google.com/Ipsum14/posts'),
(65, 'kokila Dolor15', '1990-05-15', 'kokila.Dolor15@gmail.com', 2015, 'Afac', 'DCE', 3.7, 'http://lk.linkedin.com/pub/kokila-Dolor15/72/292/696/', 'https://plus.google.com/Dolor15/posts'),
(66, 'kokila Lorem16', '1990-05-16', 'kokila.Lorem16@gmail.com', 2015, 'ITfac', 'DCE', 3.8, 'http://lk.linkedin.com/pub/kokila-Lorem16/72/292/696/', 'https://plus.google.com/Lorem16/posts'),
(67, 'kokila Ipsum17', '1990-05-17', 'kokila.Ipsum17@gmail.com', 2015, 'Efac', 'DCE', 3.7, 'http://lk.linkedin.com/pub/kokila-Ipsum17/72/292/696/', 'https://plus.google.com/Ipsum17/posts'),
(68, 'kokila Dolor18', '1990-05-18', 'kokila.Dolor18@gmail.com', 2015, 'Afac', 'DCE', 3.7, 'http://lk.linkedin.com/pub/kokila-Dolor18/72/292/696/', 'https://plus.google.com/Dolor18/posts'),
(69, 'kokila Lorem19', '1990-05-19', 'kokila.Lorem19@gmail.com', 2015, 'ITfac', 'DCE', 3.4, 'http://lk.linkedin.com/pub/kokila-Lorem19/72/292/696/', 'https://plus.google.com/Lorem19/posts'),
(70, 'kokila Ipsum20', '1990-05-20', 'kokila.Ipsum20@gmail.com', 2015, 'Efac', 'DCE', 3.4, 'http://lk.linkedin.com/pub/kokila-Ipsum20/72/292/696/', 'https://plus.google.com/Ipsum20/posts'),
(71, 'kokila Dolor21', '1990-05-21', 'kokila.Dolor21@gmail.com', 2015, 'Afac', 'DCE', 3.4, 'http://lk.linkedin.com/pub/kokila-Dolor21/72/292/696/', 'https://plus.google.com/Dolor21/posts'),
(72, 'kokila Lorem22', '1990-05-22', 'kokila.Lorem22@gmail.com', 2015, 'ITfac', 'DCE', 3.4, 'http://lk.linkedin.com/pub/kokila-Lorem22/72/292/696/', 'https://plus.google.com/Lorem22/posts'),
(73, 'kokila Ipsum23', '1990-05-23', 'kokila.Ipsum23@gmail.com', 2015, 'Efac', 'DCE', 3, 'http://lk.linkedin.com/pub/kokila-Ipsum23/72/292/696/', 'https://plus.google.com/Ipsum23/posts'),
(74, 'kokila Dolor24', '1990-05-24', 'kokila.Dolor24@gmail.com', 2015, 'Afac', 'DCE', 3.2, 'http://lk.linkedin.com/pub/kokila-Dolor24/72/292/696/', 'https://plus.google.com/Dolor24/posts'),
(75, 'kokila Lorem25', '1990-05-25', 'kokila.Lorem25@gmail.com', 2015, 'ITfac', 'DCE', 3.1, 'http://lk.linkedin.com/pub/kokila-Lorem25/72/292/696/', 'https://plus.google.com/Lorem25/posts'),
(76, 'kokila Ipsum26', '1990-05-26', 'kokila.Ipsum26@gmail.com', 2015, 'Efac', 'DCE', 3.2, 'http://lk.linkedin.com/pub/kokila-Ipsum26/72/292/696/', 'https://plus.google.com/Ipsum26/posts'),
(77, 'kokila Dolor27', '1990-05-27', 'kokila.Dolor27@gmail.com', 2015, 'Afac', 'DCE', 3.2, 'http://lk.linkedin.com/pub/kokila-Dolor27/72/292/696/', 'https://plus.google.com/Dolor27/posts'),
(78, 'Chamini Kumarasinghe', '1990-05-28', 'kokila.Lorem28@gmail.com', 2015, 'ITfac', 'DCE', 2.73333, 'http://lk.linkedin.com/pub/kokila-Lorem28/72/292/696/', 'https://plus.google.com/Lorem28/posts');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `p_email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `profile_pic` varchar(100) COLLATE utf8_unicode_ci DEFAULT '/img/profilepic.png',
  `confirmed` tinyint(1) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `confirmation_token` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`),
  UNIQUE KEY `users_p_email_unique` (`p_email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=86 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `p_email`, `role`, `profile_pic`, `confirmed`, `remember_token`, `confirmation_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', '$2y$10$oeVYcPSZG6/aAEJ2g9SB7Orli0WFTLn57MNzgmTxi9z3GVoQH8HHa', 'admin@uom.asd', 'adm', '/uploads/profilepic.png', 1, '95sp4qdPjy6xytFKbq1q4KMpyC8UK7zhLVcsh5o0H7GSMQzSh5gkIBJGaNt4', '', '2015-03-08 03:26:22', '2015-05-25 02:53:00'),
(50, '124007h', '$2y$10$oeVYcPSZG6/aAEJ2g9SB7Orli0WFTLn57MNzgmTxi9z3GVoQH8HHa', '124018h@uom.lk', 'std', '/uploads/sa0YJgou7uyK2.png', 1, 'AsDJgBjADPsFTACmc3xIwpf1smFCdDjOM5FI0aXMybqYvpEV1oMwSTBgagPn', '', '2015-03-08 03:26:22', '2015-03-10 11:18:15'),
(51, '124009h', '$2y$10$oeVYcPSZG6/aAEJ2g9SB7Orli0WFTLn57MNzgmTxi9z3GVoQH8HHa', '124009h@uom.lk', 'std', '/uploads/profilepic.png', 1, NULL, '', '2015-03-08 03:26:22', '2015-03-08 03:26:22'),
(58, '124008h', '$2y$10$boxCut5dRA5nB0oCtYs3jOdQXzQSGjSbGB0bMcnf1sRJpVp2/GO82', '124008h@uom.lk', 'std', '/uploads/sa0YJgou7uyK2.png', 1, 'kSwbwC3qiL23OCEKzvvn8RLJi7bthb9e2IOnXrAuqeJBjaKeAfHgfs1i13VY', '', '2015-03-10 12:14:05', '2015-05-25 03:10:43'),
(59, '124037c', '$2y$10$boxCut5dRA5nB0oCtYs3jOdQXzQSGjSbGB0bMcnf1sRJpVp2/GO82', '124037c@uom.lk', 'std', '/img/profilepic.png', 0, NULL, '02uhpCxQ8BfsSCEyoZxzAdbLcIh9JXBkNcgN6wQdIm0ZB0tt4TVAXs2mnqjm', '2015-03-10 12:15:24', '2015-03-10 12:15:24'),
(60, '125008h', '$2y$10$boxCut5dRA5nB0oCtYs3jOdQXzQSGjSbGB0bMcnf1sRJpVp2/GO82', '125008h@uom.lk', 'std', '/uploads/3.png', 1, NULL, '', '2015-03-10 06:44:05', '2015-03-10 06:44:05'),
(61, '126008h', '$2y$10$boxCut5dRA5nB0oCtYs3jOdQXzQSGjSbGB0bMcnf1sRJpVp2/GO82', '126008h@uom.lk', 'std', '/uploads/4.png', 1, NULL, '', '2015-03-10 06:44:05', '2015-03-10 06:44:05'),
(62, '127008h', '$2y$10$boxCut5dRA5nB0oCtYs3jOdQXzQSGjSbGB0bMcnf1sRJpVp2/GO82', '127008h@uom.lk', 'std', '/uploads/5.png', 1, NULL, '', '2015-03-10 06:44:05', '2015-03-10 06:44:05'),
(63, '128008h', '$2y$10$boxCut5dRA5nB0oCtYs3jOdQXzQSGjSbGB0bMcnf1sRJpVp2/GO82', '128008h@uom.lk', 'std', '/uploads/6.png', 1, NULL, '', '2015-03-10 06:44:05', '2015-03-10 06:44:05'),
(64, '129008h', '$2y$10$boxCut5dRA5nB0oCtYs3jOdQXzQSGjSbGB0bMcnf1sRJpVp2/GO82', '129008h@uom.lk', 'std', '/uploads/7.png', 1, NULL, '', '2015-03-10 06:44:05', '2015-03-10 06:44:05'),
(65, '130008h', '$2y$10$boxCut5dRA5nB0oCtYs3jOdQXzQSGjSbGB0bMcnf1sRJpVp2/GO82', '130008h@uom.lk', 'std', '/uploads/8.png', 1, NULL, '', '2015-03-10 06:44:05', '2015-03-10 06:44:05'),
(66, '131008h', '$2y$10$boxCut5dRA5nB0oCtYs3jOdQXzQSGjSbGB0bMcnf1sRJpVp2/GO82', '131008h@uom.lk', 'std', '/uploads/sa0YJgou7uyK2.png', 1, NULL, '', '2015-03-10 06:44:05', '2015-03-10 06:44:05'),
(67, '132008h', '$2y$10$boxCut5dRA5nB0oCtYs3jOdQXzQSGjSbGB0bMcnf1sRJpVp2/GO82', '132008h@uom.lk', 'std', '/uploads/sa0YJgou7uyK2.png', 1, NULL, '', '2015-03-10 06:44:05', '2015-03-10 06:44:05'),
(68, '133008h', '$2y$10$boxCut5dRA5nB0oCtYs3jOdQXzQSGjSbGB0bMcnf1sRJpVp2/GO82', '133008h@uom.lk', 'std', '/uploads/sa0YJgou7uyK2.png', 1, NULL, '', '2015-03-10 06:44:05', '2015-03-10 06:44:05'),
(69, '134008h', '$2y$10$boxCut5dRA5nB0oCtYs3jOdQXzQSGjSbGB0bMcnf1sRJpVp2/GO82', '134008h@uom.lk', 'std', '/uploads/sa0YJgou7uyK2.png', 1, NULL, '', '2015-03-10 06:44:05', '2015-03-10 06:44:05'),
(70, '135008h', '$2y$10$boxCut5dRA5nB0oCtYs3jOdQXzQSGjSbGB0bMcnf1sRJpVp2/GO82', '135008h@uom.lk', 'std', '/uploads/sa0YJgou7uyK2.png', 1, NULL, '', '2015-03-10 06:44:05', '2015-03-10 06:44:05'),
(71, '136008h', '$2y$10$boxCut5dRA5nB0oCtYs3jOdQXzQSGjSbGB0bMcnf1sRJpVp2/GO82', '136008h@uom.lk', 'std', '/uploads/sa0YJgou7uyK2.png', 1, NULL, '', '2015-03-10 06:44:05', '2015-03-10 06:44:05'),
(72, '137008h', '$2y$10$boxCut5dRA5nB0oCtYs3jOdQXzQSGjSbGB0bMcnf1sRJpVp2/GO82', '137008h@uom.lk', 'std', '/uploads/sa0YJgou7uyK2.png', 1, NULL, '', '2015-03-10 06:44:05', '2015-03-10 06:44:05'),
(73, '138008h', '$2y$10$boxCut5dRA5nB0oCtYs3jOdQXzQSGjSbGB0bMcnf1sRJpVp2/GO82', '138008h@uom.lk', 'std', '/uploads/sa0YJgou7uyK2.png', 1, NULL, '', '2015-03-10 06:44:05', '2015-03-10 06:44:05'),
(74, '139008h', '$2y$10$boxCut5dRA5nB0oCtYs3jOdQXzQSGjSbGB0bMcnf1sRJpVp2/GO82', '139008h@uom.lk', 'std', '/uploads/sa0YJgou7uyK2.png', 1, NULL, '', '2015-03-10 06:44:05', '2015-03-10 06:44:05'),
(75, '140008h', '$2y$10$boxCut5dRA5nB0oCtYs3jOdQXzQSGjSbGB0bMcnf1sRJpVp2/GO82', '140008h@uom.lk', 'std', '/uploads/sa0YJgou7uyK2.png', 1, NULL, '', '2015-03-10 06:44:05', '2015-03-10 06:44:05'),
(76, '141008h', '$2y$10$boxCut5dRA5nB0oCtYs3jOdQXzQSGjSbGB0bMcnf1sRJpVp2/GO82', '141008h@uom.lk', 'std', '/uploads/sa0YJgou7uyK2.png', 1, NULL, '', '2015-03-10 06:44:05', '2015-03-10 06:44:05'),
(77, '142008h', '$2y$10$boxCut5dRA5nB0oCtYs3jOdQXzQSGjSbGB0bMcnf1sRJpVp2/GO82', '142008h@uom.lk', 'std', '/uploads/sa0YJgou7uyK2.png', 1, NULL, '', '2015-03-10 06:44:05', '2015-03-10 06:44:05'),
(78, '143008h', '$2y$10$boxCut5dRA5nB0oCtYs3jOdQXzQSGjSbGB0bMcnf1sRJpVp2/GO82', '143008h@uom.lk', 'std', '/uploads/5.png', 1, NULL, '', '2015-03-10 06:44:05', '2015-03-10 06:44:05'),
(79, '094007H', '$2y$10$oeVYcPSZG6/aAEJ2g9SB7Orli0WFTLn57MNzgmTxi9z3GVoQH8HHa', 'kokila.alu@gmail.com', 'alm', '/uploads/555fd5083bc565.45548182.png', 1, 'iZM387jnfBRodF6ad0uDVaKgd1RBI8hPMC1Ixs7UzchB60rtvjjg5j7PaEyI', '', '2015-05-22 06:41:36', '2015-05-24 04:12:55'),
(84, 'CodeJen', '$2y$10$v0RhgFzH2U5KW/gwA07GB.QomLx1DMpcCwxqGxUf1IF4qNzMMXbce', 'asdqweq@gmail.com', 'ind', '/uploads/5562a59b53831.png', 1, 'Snod044mNNDASlSQ0dj3B9hVetzsS8rLDnaQP58vHFgIIhRKqPs8XAf3vDc6', '', '2015-05-24 08:03:52', '2015-05-25 03:30:23'),
(85, 'ER VORIE', '$2y$10$v0RhgFzH2U5KW/gwA07GB.QomLx1DMpcCwxqGxUf1IF4qNzMMXbce', 'asdqsd@gmail.com', 'ind', '/uploads/5562a59b53831.png', 2, '5cPveNiOYcCkq0DGSKY8qdTNCIcr0clgioy9HVBKolOnslPzRnh4qiy3I8oS', '', '2015-05-24 08:03:52', '2015-05-25 00:45:33');

-- --------------------------------------------------------

--
-- Structure for view `backup`
--
DROP TABLE IF EXISTS `backup`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `backup` AS select `u`.`profile_pic` AS `profile_pic`,`und`.`user_id` AS `user_id`,`und`.`name` AS `name`,`und`.`dob` AS `dob`,`und`.`n_email` AS `n_email`,`und`.`entrance_year` AS `entrance_year`,`und`.`faculty` AS `faculty`,`und`.`department` AS `department`,`und`.`Linkedin` AS `Linkedin`,`und`.`googleplus` AS `googleplus`,`result`.`result_id` AS `result_id`,`result`.`semester` AS `semester`,`result`.`gpa` AS `gpa`,`project`.`proj_id` AS `proj_id`,`project`.`proj_title` AS `proj_title`,`project`.`proj_descr` AS `proj_descr`,`project`.`members` AS `members`,`more`.`more_id` AS `more_id`,`more`.`summary` AS `summary`,`more`.`marital_status` AS `marital_status`,`more`.`employe` AS `employe`,`exper`.`ex_id` AS `ex_id`,`exper`.`title` AS `title`,`exper`.`descr` AS `descr`,`skill`.`skills_pool_id` AS `skills_pool_id`,`skill`.`percentage` AS `percentage` from ((((((`user` `u` join `undergraduate` `und` on((`u`.`id` = `und`.`user_id`))) join `result` on((`und`.`user_id` = `result`.`undergraduate_id`))) join `more_detail` `more` on((`und`.`user_id` = `more`.`undergraduate_id`))) join `project` on((`und`.`user_id` = `project`.`undergraduate_id`))) join `experience` `exper` on((`und`.`user_id` = `exper`.`undergraduate_id`))) join `skills` `skill` on((`und`.`user_id` = `skill`.`undergraduate_id`))) where ((`u`.`role` = 'std') and (`u`.`confirmed` = 1));

-- --------------------------------------------------------

--
-- Structure for view `students`
--
DROP TABLE IF EXISTS `students`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `students` AS select `u`.`profile_pic` AS `profile_pic`,`und`.`user_id` AS `user_id`,`und`.`name` AS `name`,`und`.`dob` AS `dob`,`und`.`n_email` AS `n_email`,`und`.`entrance_year` AS `entrance_year`,`und`.`faculty` AS `faculty`,`und`.`department` AS `department`,`und`.`ogpa` AS `ogpa`,`und`.`Linkedin` AS `Linkedin`,`und`.`googleplus` AS `googleplus`,`result`.`result_id` AS `result_id`,`result`.`semester` AS `semester`,`result`.`gpa` AS `gpa`,`project`.`proj_id` AS `proj_id`,`project`.`proj_title` AS `proj_title`,`project`.`proj_descr` AS `proj_descr`,`project`.`members` AS `members`,`more`.`more_id` AS `more_id`,`more`.`summary` AS `summary`,`more`.`marital_status` AS `marital_status`,`more`.`employe` AS `employe`,`exper`.`ex_id` AS `ex_id`,`exper`.`title` AS `title`,`exper`.`descr` AS `descr`,`skill`.`undergraduate_id` AS `skill_uid`,`skill`.`skills_pool_id` AS `skills_pool_id`,`skill`.`percentage` AS `percentage` from ((((((`user` `u` join `undergraduate` `und` on((`u`.`id` = `und`.`user_id`))) left join `result` on((`u`.`id` = `result`.`undergraduate_id`))) left join `more_detail` `more` on((`u`.`id` = `more`.`undergraduate_id`))) left join `project` on((`u`.`id` = `project`.`undergraduate_id`))) left join `experience` `exper` on((`u`.`id` = `exper`.`undergraduate_id`))) left join `skills` `skill` on((`u`.`id` = `skill`.`undergraduate_id`))) where ((`u`.`role` = 'std') and (`u`.`confirmed` = 1));

-- --------------------------------------------------------

--
-- Structure for view `studentsnew`
--
DROP TABLE IF EXISTS `studentsnew`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `studentsnew` AS select `u`.`profile_pic` AS `profile_pic`,`und`.`user_id` AS `user_id`,`und`.`name` AS `name`,`und`.`dob` AS `dob`,`und`.`n_email` AS `n_email`,`und`.`entrance_year` AS `entrance_year`,`und`.`faculty` AS `faculty`,`und`.`department` AS `department`,`und`.`ogpa` AS `ogpa`,`und`.`Linkedin` AS `Linkedin`,`und`.`googleplus` AS `googleplus`,`result`.`undergraduate_id` AS `result_uid`,`result`.`result_id` AS `result_id`,`result`.`semester` AS `semester`,`result`.`gpa` AS `gpa`,`project`.`undergraduate_id` AS `proj_uid`,`project`.`proj_id` AS `proj_id`,`project`.`proj_title` AS `proj_title`,`project`.`proj_descr` AS `proj_descr`,`project`.`members` AS `members`,`more`.`undergraduate_id` AS `more_uid`,`more`.`more_id` AS `more_id`,`more`.`summary` AS `summary`,`more`.`marital_status` AS `marital_status`,`more`.`employe` AS `employe`,`exper`.`undergraduate_id` AS `exp_uid`,`exper`.`ex_id` AS `ex_id`,`exper`.`title` AS `title`,`exper`.`descr` AS `descr`,`skill`.`undergraduate_id` AS `skill_uid`,`skill`.`skills_pool_id` AS `skills_pool_id`,`skill`.`percentage` AS `percentage` from ((((((`user` `u` left join `undergraduate` `und` on((`u`.`id` = `und`.`user_id`))) left join `result` on((`u`.`id` = `result`.`undergraduate_id`))) left join `more_detail` `more` on((`u`.`id` = `more`.`undergraduate_id`))) left join `project` on((`u`.`id` = `project`.`undergraduate_id`))) left join `experience` `exper` on((`u`.`id` = `exper`.`undergraduate_id`))) left join `skills` `skill` on((`u`.`id` = `skill`.`undergraduate_id`))) where ((`u`.`role` = 'std') and (`u`.`confirmed` = 1));

--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin`
--
ALTER TABLE `admin`
  ADD CONSTRAINT `fk_table1_users1` FOREIGN KEY (`users_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `alumni`
--
ALTER TABLE `alumni`
  ADD CONSTRAINT `alumni_ibfk_1` FOREIGN KEY (`id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `employee`
--
ALTER TABLE `employee`
  ADD CONSTRAINT `employee_ibfk_1` FOREIGN KEY (`id`) REFERENCES `alumni` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `experience`
--
ALTER TABLE `experience`
  ADD CONSTRAINT `fk_experience_undergraduate1` FOREIGN KEY (`undergraduate_id`) REFERENCES `undergraduate` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `friends`
--
ALTER TABLE `friends`
  ADD CONSTRAINT `friends_ibfk_1` FOREIGN KEY (`requester`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `friends_ibfk_2` FOREIGN KEY (`acceptor`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `industry`
--
ALTER TABLE `industry`
  ADD CONSTRAINT `fk_industry_users1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `more_detail`
--
ALTER TABLE `more_detail`
  ADD CONSTRAINT `fk_more_details_undergraduate1` FOREIGN KEY (`undergraduate_id`) REFERENCES `undergraduate` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `post`
--
ALTER TABLE `post`
  ADD CONSTRAINT `post_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `project`
--
ALTER TABLE `project`
  ADD CONSTRAINT `fk_project_undergraduate1` FOREIGN KEY (`undergraduate_id`) REFERENCES `undergraduate` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `skills`
--
ALTER TABLE `skills`
  ADD CONSTRAINT `fk_skills_skills_pool1` FOREIGN KEY (`skills_pool_id`) REFERENCES `skills_pool` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_skills_undergraduate1` FOREIGN KEY (`undergraduate_id`) REFERENCES `undergraduate` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `undergraduate`
--
ALTER TABLE `undergraduate`
  ADD CONSTRAINT `fk_undergraduate_users1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
